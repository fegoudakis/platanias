<?php
/**
 * The template for displaying dashboard
 *
 * Template Name: Apofasis Search
 *
 */

	get_header();
	
		
		
		//GET YEARS OF DATES TO QUERY BETWEEN!
		$args_dates = array(
			'posts_per_page'      => -1,
			'post_type' => 'apofasis'
			);
		$the_dates = query_posts( $args_dates );	
		$valid_dates = array();
		foreach ($the_dates as $dates) {
			$the_year = get_field( 'year', $dates->ID );
			
			$lenght = strlen($the_year);
			
			if ($lenght == 4) {
				array_push($valid_dates, (string)$the_year);
			}
			 

		}	
		$valid_dates = array_unique($valid_dates);
		
		//Sort Array DESC
		rsort($valid_dates);
		//print_r($valid_dates);
		
		//GET ALL TAXONOMIES FOR GET CONDITION PURPOSES
		$terms = get_terms( array(
				'taxonomy' => 'epitropi',
				'hide_empty' => false,
			) );
		$check_gets = array();
		foreach($terms as $term) {
			$check_gets[] = $term->slug;
		}
		array_push($check_gets, 'keyword');
		array_push($check_gets, 'from_date');
		array_push($check_gets, 'to_date');
		
		//GET THE $_GET KEYS TO INTERSECT WITH CHECK_GETS
		$the_gets = array();
		$the_vals = array();	
		foreach ($_GET as $gkey => $gvals) {
			$the_gets[] = $gkey;
			if ($gvals != '') {
				$the_vals[] = $gvals;
			}
			
		}
		//var_dump($the_vals);
		
		//CHECK IF $_GET CONTAINS VALID PARAMS
		$get_result = array_intersect($the_gets, $check_gets);
//AND ($_GET['keyword'] != '' AND $_GET['from_date'] != '' AND $_GET['to_date'] != '' AND $_GET['arithmos_sinedriasis'] != '' AND $_GET['arithmos_apofasis'] != '')

//print_r();
		if ($get_result AND $the_vals) {
			$terms = get_terms( array(
				'taxonomy' => 'epitropi',
				'hide_empty' => false,
			) );
			$taxQuery = array(
				'relation' => 'OR',
			);
			$metaQuery = array(
				'relation' => 'OR',
			);
			//if(  ||  !empty($_GET['from_date']) ){
				
			$start = $_GET['from_date'];
			$end = $_GET['to_date'];
			if ($start != "" && $end != "") {
				
				$thedates = array($start, $end);
				$compare = 'BETWEEN';
				//echo 'BETWEEN';
				
			} elseif ($start == "" && $end != "") {
				
				$thedates = $end;
				$compare = 'IN';
				//echo 'END';
				
			} elseif ($start != "" && $end == "") {
				
				$thedates = $start;
				$compare = 'IN';
				//echo 'START';
			}
			
			
			
			if ($start != "" || $end != "") {
				$miniQuery = array(
			        'key' => 'year',
			        'value' => $thedates,
			        'compare' => $compare,
			        //'type' => 'DATE'
			    );
				array_push($metaQuery, $miniQuery);
			}	
			
			if(isset($_GET['number'])){
			  $showposts = esc_sql($_GET['number']);
			}else{
			  $showposts = 10;
			}	
			
						
			foreach($terms as $term) {
				//materials
			//if($GET_['sillogikwn']){
				if ( $_GET['epitropes'] == $term->term_id) {
					$miniQuery =   array(
		                'taxonomy' => 'epitropi',
		                'field' => 'term_id',
		                //'terms' => $term->term_id,
		                'terms' => $_GET['epitropes'],
		                'include_children' => true,
		                'operator' => 'IN'
		            );
					array_push($taxQuery, $miniQuery);
				}
			//}
			}
			//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'showposts'=> $showposts,
				'posts_per_page' => $posts_per_page,
				//'posts_per_page'      => 20,
				'post_type'  => 'apofasis',
				's' => $_GET['keyword'],
				'paged'	 => $paged,		
				'meta_query'  => $metaQuery,
	            'tax_query'  => array(
		            'relation' => 'OR',
		            $taxQuery
			    )
	            
			);

			
		} else {
			//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'showposts'=> $showposts,
				'posts_per_page' => $posts_per_page,
				'post_type'  => 'apofasis',
				'paged'	 => $paged
			);			
		}

		$apofasis = new WP_Query( $args );
		$temp_query = $wp_query;
		$wp_query   = NULL;
		$wp_query   = $apofasis;
		

		//echo $paged;
		/*echo('<pre>');
		print_r ($apofasis);
		echo('</pre>');*/
		
	/*
 echo('<pre>');
 print_r ($args);
 echo('</pre>');*/
 
	//$query = ! empty( $params['keyword'] ) ? esc_attr( $params['keyword'] ) : '';
/*
echo ("<pre>");
print_r ($search);
echo ("</pre>");*/
?>
   

<div style="border: solid 1px; margin-bottom: 50px; padding: 15px; margin-top:45px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35)!important; border-radius: 5px;" class="search_bar_wrap" style="margin-top:50px; margin-bottom:50px;">
	<p style="text-align:center; font-size:25px;"><span><strong>Κριτήρια Αναζήτησης</strong></span></p>
        <form id="formid1" method="get"  class="horizontal-form" action="<?php echo esc_attr( site_url() ); ?>/search-apofasis/" >
			<div class="col-md-12">
                <p class="search_field">
					<span> <strong>Λέξεις κλειδιά</strong> </span>
                    <input id="first-key" placeholder="πχ. Παραίτηση από ένδικα μέσα – Λήψη απόφασης" type="text" name="keyword" value="<?php echo $_GET['keyword']; ?>" style="display:inline-block; margin-top: 9px;">
                </p>
			</div>
				
				<div class="col-md-4">	
					<ul class="the_serchss">
						<span> <strong>Συλλογικά Όργανα</strong> </span>
						<li style="margin-bottom:9px;">
						
						<select data-default=" " id="myselect" sstyle="display:inline-block; height: 53px; width:90%; margin-top: 9px;" class="" name="epitropes">
						  <option name="first" value="">Αποφάσεις Συλλογικών Οργάνων</option>
						  <option value="147"<?php if ($_GET['epitropes'] == 147) {echo "selected";} ?>>Δημοτικό Συμβούλιο</option>
						  <option value="143"<?php if ($_GET['epitropes'] == 143) {echo "selected";} ?>>Αποφάσεις Δημάρχου</option>
						  <option value="148"<?php if ($_GET['epitropes'] == 148) {echo "selected";} ?>>Οικονομική Επιτροπή</option>
						  <option value="142"<?php if ($_GET['epitropes'] == 142) {echo "selected";} ?>>Επιτροπή Ποιότητας Ζωής</option>
						  <option value="145"<?php if ($_GET['epitropes'] == 145) {echo "selected";} ?>>Δημοτική Κοινότητα Πλατανιά</option>
						  <option value="146"<?php if ($_GET['epitropes'] == 146) {echo "selected";} ?>>Δημοτική Κοινότητα Γερανίου</option>
						</select>
						
						
						<!--<select name="" style="display:inline-block; height: 53px; width:90%; margin-top: 9px;">-->

								
					<?php 
						$terms = get_terms( array(
						    'taxonomy' => 'epitropi',
						    'hide_empty' => false,
						) );
	
						
						foreach($terms as $term) {
							
						/*style="border: solid 1px; padding: 15px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35); border-radius: 5px;"echo '<pre>';
						print_r($term);
						echo '</pre>';*/
						
					?>
			<!--<li><label><input <?php echo ($_GET[$term->slug]==$term->term_id) ? 'checked="checked"' : '' ; ?> name="<?php echo $term->slug ?>" type="checkbox" value="<?php echo $term->term_id ?>"><?php echo $term->name ?></label></li>-->
			
			
					<!--	<option name="<?php echo $term->slug; ?>" value="<?php echo $term->term_id;  ?>" <?php if ($_GET[$term->slug]==$term->term_id) {echo "selected";} ?>><?php echo $term->name; ?></option>-->

						<!--<option name="<?php echo $term->slug ?>" value="<?php echo $term->term_id ?>"><?php echo $term->name ?></option>-->
						<!--<li><label><input <?php echo ($_GET[$term->slug]==$term->term_id) ? 'checked="checked"' : '' ; ?> name="<?php echo $term->slug ?>" type="checkbox" value="<?php echo $term->term_id ?>"><?php echo $term->name ?></label></li>-->
						<?php } ?>
						
					<!--	</select>-->
						</li>
						
					<!--	<span> <strong>Αριθμός στοιχείων ανά σελίδα στον πίνακα</br> Αποφάσεις Δήμου</strong> </span>
						
						<li>
						<select  class="field" name="number">
							<option value="5"<?php if ($_GET['number'] == 5) {echo "selected";} ?>>5</option>
							<option value="10"<?php if ($_GET['number'] == 10) {echo "selected";} ?>>10</option>
							<option value="15"<?php if ($_GET['number'] == 15) {echo "selected";} ?>>15</option>
							<option value="20"<?php if ($_GET['number'] == 20) {echo "selected";} ?>>20</option>
						</select> 	
						</li>-->
					</ul>
				</div>
				
				<div class="col-md-4">	
					<ul class="the_serchss">
						<span> <strong>Έτος</strong> </span>
	                    <li>
	                    <select id="from_date" name="from_date" style="display:inline-block; height: 53px; width:90%;">
						<?php if ($_GET['from_date'] || $_GET['from_date'] == "") {
							
							echo '<option value="" selected > Έτος από </option>';
							
						} ?>
						<?php foreach ($valid_dates as $date) { ?>
							<option value="<?php echo $date; ?>" <?php if ($_GET['from_date'] == $date) {echo "selected";} ?>><?php echo $date; ?></option>
						<?php } ?>
						</select></li>
		
						<li>
						<select id="to_date" name="to_date" style="display:inline-block; height: 53px; width:90%;">
							<?php if ($_GET['to_date'] || $_GET['to_date'] == "") {
								
								echo '<option value="" selected > Έτος μέχρι </option>';
								
							} ?>
							<?php foreach ($valid_dates as $date) { ?>
								<option value="<?php echo $date; ?>" <?php if ($_GET['to_date'] == $date) {echo "selected";} ?> ><?php echo $date; ?></option>
							<?php } ?>
						</select></li>
					</ul>
				</div>
				
				<div class="col-md-4">									
					<ul class="the_serchss">
						<span> <strong>Αριθμός συνεδρίασης</strong> </span>
	                    <li>
	                    <!--<span> <strong>Αριθμός Συνεδρίασης</strong> </span><br>-->
	                    <input id="ari"class="inputadvapo"  placeholder="Εισάγετε των αριθμό συνεδρίασης" type="text" name="arithmos_sinedriasis" value="<?php echo $_GET['arithmos_sinedriasis']; ?>" style="display:inline-block;">
						</li>
						<span> <strong>Αριθμός απόφασης</strong> </span>
						<li>
						<!--<span> <strong>Αριθμός Απόφασης</strong> </span><br>-->
						<input id="arit" class="inputadvapo"  placeholder="Εισάγετε των αριθμό απόφασης" type="text" name="arithmos_apofasis" value="<?php echo $_GET['arithmos_apofasis']; ?>" style="display:inline-block;">
						</li>
						
					</ul>
				</div>	
				
				
			<div class="col-md-12">
                <p style="text-align:center;">
                    <button style="margin-top: 15px;" type="submit" class="cmsmasters_theme_icon_search">   Αναζήτηση</button>
					<a id="reset"><button>Καθαρισμός Φίλτρων</button></a>
                </p>
			</div>
			
        <!--</form>-->

	</div>

<!--    <div class="content">-->


    <?php

    //if( isset($_GET['keyword']) && ($_GET['keyword'] != '' ) ):

        if ( $apofasis->have_posts()  ) : ?>
	<div class="cmsmasters_row_outer_parent">
		<div class="cmsmasters_row_outer">
			<div class="cmsmasters_row_inner">
				<div class="cmsmasters_row_margin cmsmasters_11">
					<div id="cmsmasters_column_e772dfe2f7" class="cmsmasters_column one_first">
						<div class="cmsmasters_column_inner">
							<table class="cmsmasters_table">
								<caption style="font-size:25px;">Αποφάσεις Δήμου
							<div style="float:right;">
								<div style="display: inline-block;">
								<select style="" class="field" name="number">
									  <option value="5"<?php if ($_GET['number'] == 5) {echo "selected";} ?>>5</option>
									  <option value="10"<?php if ($_GET['number'] == 10) {echo "selected";} ?>>10</option>
									  <option value="15"<?php if ($_GET['number'] == 15) {echo "selected";} ?>>15</option>
									  <option value="20"<?php if ($_GET['number'] == 20) {echo "selected";} ?>>20</option>
								</select>
								</div>
							<div style="display: inline-block;">
								 <p>
									<button style="margin-top: 15px;" type="submit" class="cmsmasters_theme_icon_search"></button>
								</p>
							</div>
							</div>
							</form>
								</caption>
							
							
								<!--<span><strong>Αριθμός στοιχείων ανά σελίδα</strong> </span>-->
								<!--<li>-->
									
								<!--</li>-->

							
							
								<thead>
									<tr class="cmsmasters_table_row_header">
										<th class="cmsmasters_table_cell_aligncenter">A/A</th>
										<th class="cmsmasters_table_cell_aligncenter">Τίτλος</th>
										<th class="cmsmasters_table_cell_aligncenter">ΑΔΑ</th>
										<th class="cmsmasters_table_cell_aligncenter">Αρ. Απόφασης</th>
										<th class="cmsmasters_table_cell_aligncenter">Αρ. Συνεδρίασης</th>
										<!--<th class="cmsmasters_table_cell_aligncenter">Στοιχεία</th>-->
										<th class="cmsmasters_table_cell_aligncenter">Επιτρόπη</th>
										<th class="cmsmasters_table_cell_aligncenter">Έτος</th>
										<th class="cmsmasters_table_cell_aligncenter">Ενέργεια</th>
									</tr>
								</thead>
								<tbody>
       <?php     while ( $apofasis->have_posts() ) : $apofasis->the_post();
				$the_epitropous = wp_get_post_terms($post->ID, 'epitropi', array("fields" => "names"));
    ?>			<!--Προβολή PDF Α/Α, Τίτλος, ΑΔΑ, Αρ. Απόφασης, Αρ. Συνεδρίασης, Επιτροπή, Έτος, Ενέργεια-->
									<tr>
										<td class="cmsmasters_table_cell_aligncenter"><?php echo get_field( 'aa' ); ?></td>
										
										<td class="cmsmasters_table_cell_aligncenter"><!--<a href="<?php echo $post->guid; ?>">--><?php echo get_the_title( ); ?><!--</a>--></td>
										<td class="cmsmasters_table_cell_aligncenter"><?php ?></td>
										<td class="cmsmasters_table_cell_aligncenter"><?php echo get_field( 'stoixeia' ); ?></td>
										<td class="cmsmasters_table_cell_aligncenter"><?php  ?></td>
										<td class="cmsmasters_table_cell_aligncenter"><?php echo $the_epitropous[0];  ?></td>
										<td class="cmsmasters_table_cell_aligncenter"><?php echo get_field( 'year' ); ?></td>
										<td class="cmsmasters_table_cell_aligncenter">
											<a target="_blank" href="http://46.4.136.58/platanias/<?php echo get_field( 'arxeio' ); ?>"><i style="font-size: 18px;" class="fa fa-file-pdf-o" aria-hidden="true"></i></a> <br>
											<!--<a href="<?php echo $post->guid; ?>"><i style="font-size: 18px;" class="fa fa-eye" aria-hidden="true"></i></a>-->
											<?php if (get_field( 'ada' )): ?>
												<a href='https://et.diavgeia.gov.gr/search?advanced=true&query=ada:"<?php echo get_field( 'ada' ); ?>"&page=0'><!--ΑΔΑ--><i style="font-size: 18px;" class="fa fa-eye" aria-hidden="true"></i></a>
											<?php endif; ?>
										</td>
										
									</tr>

            <?php endwhile; 
	           // wp_reset_postdata();
            ?>
            					</tbody>
								<tfoot>
									<tr class="cmsmasters_table_row_footer">
										<td class="cmsmasters_table_cell_aligncenter">A/A</td>
										<td class="cmsmasters_table_cell_aligncenter">Τίτλος</td>
										<td class="cmsmasters_table_cell_aligncenter">ΑΔΑ</td>
										<td class="cmsmasters_table_cell_aligncenter">Αρ. Απόφασης</td>
										<td class="cmsmasters_table_cell_aligncenter">Αρ. Συνεδρίασης</td>
										<!--<td class="cmsmasters_table_cell_aligncenter">Στοιχεία</td>-->
										<td class="cmsmasters_table_cell_aligncenter">Επιτρόπη</td>
										<td class="cmsmasters_table_cell_aligncenter">Έτος</td>
										<td class="cmsmasters_table_cell_aligncenter">Ενέργεια</td>
									</tr>
								</tfoot>
							</table>
						</div>
					</div>		
				</div>
			</div>
		</div>
	</div>
            <!-- pagination -->
            <div style="clear:both"></div>

	          <?php  else: ?>
	<div>
		
		<p>
		<b> Δεν βρέθηκαν αποτελέσματα από την αναζήτηση σας.</b>  Δοκιμάστε να κάνετε μια καινούργια αναζήτηση, επιστρέφοντας στην <a href="<?php echo get_site_url().'/search-apofasis/'; ?>"> σελίδα αναζήτησης αποφάσεων</a>.
				
		
	</div> 
		          
		     <?php     endif;
	        wp_reset_postdata();
	        

            ?>

	<div class="navigation1">
<?php 	pagination($apofasis->max_num_pages); ?>
	</div>      
            

<!-- </div> -->

<!--    </div>-->

<script>




$("#reset").on("click", function () {
	$('#myselect').val(' ').change();
	$('#from_date').val(' ').change();
	$('#to_date').val(' ').change();
	$('#first-key').val(' ').change();
	$('#ari').val(' ').change();
	$('#arit').val(' ').change();
});    
    

	//document.getElementById("#myselect").selectedIndex = "0";
	//$('option[name=first]').attr('selected');
	//$('name="first[]"] option[value="-1"]').attr('selected', 'selected');
	//console.log('fsdfasdf');
	//$('select').val('');
   // $('#myselect ').prop('selectedIndex',0);


/*
$("#reset").on("click", function () {
        var $select = $('#myselect');
        $select.val($select.data('default'));
    });*/
	
/*	
$("#reset").on("click", function () {
    $('#myselect option').prop('selected', function() {
        return this.defaultSelected;
    });
});*/
</script>

<?php get_footer(); ?>
