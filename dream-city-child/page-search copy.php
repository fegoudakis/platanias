<?php
/**
 * The template for displaying dashboard
 *
 * Template Name: Template Simple Search
 *
 */

	get_header();


if( isset($_GET['keyword']) && ($_GET['keyword'] != '' ) ):

		$args = array(
			'posts_per_page' => -1,	
			'post_type' => array('project','page','post',),
			'orderby' => 'type',
			's' => $_GET['keyword'],
		);
		
		$search = new WP_Query( $args );

endif;
/*
echo ("<pre>");
print_r ($search);
echo ("</pre>");
*/
?>
   


    <div class="search_bar_wrap" style="margin-top:50px; margin-bottom:50px;">

        <form method="get" class="horizontal-form" action="<?php echo esc_attr( site_url() ); ?>/anazitisi/" >

                <p class="search_field">
                    <input  placeholder="Search by Keywords: e.g. word1, word2" type="text" name="keyword" value="<?php echo $_GET['keyword']; ?>" style="display:block">
                </p>

                <p class="search_button">
                    <button type="submit" class="cmsmasters_theme_icon_search"></button>
                </p>

        </form>

    </div>

    <?php

    if( isset($_GET['keyword']) && ($_GET['keyword'] != '' ) ):

        if ( $search->have_posts() && $post->post_title != 'Home' ) :

            while ( $search->have_posts() ) : $search->the_post();
			
               if ($post->post_title != 'Αρχική') :
    ?>

					<article style="padding: 30px 0 0;" id="post-<?php echo $post->ID;?>" class="cmsmasters_archive_type post-<?php echo $post->ID;?> post type-post status-publish format-standard has-post-thumbnail hentry category-agrotika-themata category-thema">
							<div class="cmsmasters_archive_item_img_wrap" style="width:14%;">
							<!--	<?php if ( has_post_thumbnail($post->ID) ) { ?>
								
								<figure class="cmsmasters_img_wrap"><a href="http://beta.platanias.gr/georgikes-proidopiisis-gia-abel/" class="cmsmasters_img_link preloader fs">
									 <img width="580" height="490" src="<?php the_post_thumbnail_url();?>" class="full-width wp-post-image" sizes="(max-width: 580px) 100vw, 580px"></a>
								</figure>
								<?php } ?>-->
							</div>	

							<div class="cmsmasters_archive_item_cont_wrap">
								<div class="cmsmasters_archive_item_type">
										<?php  $the_date = get_the_date( $format, $post->ID );  echo $the_date; 
										if($post->post_type == 'project' ){
											echo ', Υπηρεσία';
										}
										if($post->post_type == 'page' ){
											echo ', Σελίδα';
										}
										
										$categories = get_the_category( $post->ID );
										//$par = wp_get_post_parent_id( $post->ID );
										//$cats = get_terms('category',$post->ID);
										if ($categories[0]->name){
											//echo ($categories[0]->name);
											$vat = ($categories[0]->category_parent);
																						
											if( $vat == 101 ){
												echo  ', '.($categories[2]->name);
											}else{
												echo  ', '.($categories[0]->name);
											}												
										}
										
										?>
								</div>
								<header class="cmsmasters_archive_item_header entry-header">
									<h3><a href="<?php echo get_the_permalink($post);?>"><?php echo $post->post_title; ?></a></h3>
								</header>
								<div class="cmsmasters_archive_item_content entry-content">
									<p><?php the_excerpt();	?></p>
								</div>
								<footer class="cmsmasters_archive_item_info entry-meta"></footer>		
							</div>
					</article>
			
					            
			 <?php 	endif; endwhile; 
			
		else: ?>
				<p style="text-align:center;"><b> Δεν βρέθηκαν αποτελέσματα από την αναζήτηση σας.</b>  Δοκιμάστε να κάνετε μια καινούργια αναζήτηση, επιστρέφοντας στην <a href="<?php echo get_site_url(); ?>">αρχική σελίδα</a>.</p> 
	<?php 
		endif;  
	endif;
	

 get_footer(); ?>
