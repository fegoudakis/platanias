 <?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Website Footer Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_option = dream_city_get_global_options();
?>


		</div>
	</div>
</div>
<!-- _________________________ Finish Middle _________________________ -->
<?php 

get_sidebar('bottom');

?>
<a href="javascript:void(0);" id="slide_top" class="cmsmasters_theme_icon_slide_top_button"><span></span></a>
</div>
<!-- _________________________ Finish Main _________________________ -->

<!-- _________________________ Start Footer _________________________ -->
<footer id="footer" class="footer <?php echo 'cmsmasters_color_scheme_' . $cmsmasters_option['dream-city' . '_footer_scheme'] . ($cmsmasters_option['dream-city' . '_footer_type'] == 'default' ? ' cmsmasters_footer_default' : ' cmsmasters_footer_small'); ?>">
	<?php 
	//get_template_part('theme-framework/template/footer');
	?>
	<div class="footer_inner">
	
		<div class="social_wrap">

			<div class="">
				<ul>
					<li>
						<a href="https://www.facebook.com/dimosplatania" class="cmsmasters_social_icon cmsmasters_social_icon_1 cmsmasters-icon-facebook-1" title="Facebook" target="_blank"></a>
					</li>
					<li>
						<a href="https://twitter.com/dimosplatania" class="cmsmasters_social_icon cmsmasters_social_icon_2 cmsmasters-icon-twitter" title="Twitter" target="_blank"></a>
					</li>
					<li>
						<a href="https://www.youtube.com/channel/UCfOd155pai-QIeAH5DDeh2A" class="cmsmasters_social_icon cmsmasters_social_icon_3 cmsmasters-icon-youtube-play" title="YouTube" target="_blank"></a>
					</li>
					<li>
						<a href="#" class="cmsmasters_social_icon cmsmasters_social_icon_4 cmsmasters-icon-instagram" title="Instagram"></a>
					</li>
				</ul>
			</div>
		</div>	
		
		<span id="copyright">Δήμος Πλατανιά © 2017 / All Rights Reserved</span>
		<span id="footer-links"> <a href="<?php echo get_site_url(); ?>/project/%CF%85%CF%80%CE%B7%CF%81%CE%B5%CF%83%CE%AF%CE%B5%CF%82/">Υπηρεσίες </a> | <a href="<?php echo get_site_url(); ?>/map/">Χάρτες </a> | <a href="<?php echo get_site_url(); ?>/forma-lipsis-enimerosis/">Εγγραφή για λήψη ενημέρωσης </a> </span>
		
		
	</div>
</footer>
<!-- _________________________ Finish Footer _________________________ -->

<?php do_action('cmsmasters_after_page', $cmsmasters_option); ?>
</div>
<!-- _________________________ Finish Page _________________________ -->

<?php do_action('cmsmasters_after_body', $cmsmasters_option); ?>
<?php wp_footer(); ?>

</body>
</html>
