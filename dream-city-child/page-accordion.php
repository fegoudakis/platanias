<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Template Name: Template Accordion
 * 
 * 
 */


get_header();


list($cmsmasters_layout) = dream_city_theme_page_layout_scheme();


echo '<!--_________________________ Start Content _________________________ -->' . "\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo '<div class="content entry">' . "\n\t";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo '<div class="content entry fr">' . "\n\t";
} else {
	echo '<div class="middle_content entry" style="margin-bottom:15px;">'; //style for space up from footer
}


if (have_posts()) : the_post();
	$content_start = substr(get_post_field('post_content', get_the_ID()), 0, 15);
	



	if ($cmsmasters_layout == 'fullwidth' && $content_start === '[cmsmasters_row') {
		echo '</div>' . 
		'</div>';
	}
	
	
	the_content();
	
	echo '<div class="cl"></div>';
	/***ACCORDION****/
	
	global $post;
    $page_title = $post->post_name;
	$page_id = $post->ID;
	//echo $page_id;
	
	$args = array('posts_per_page' => -1,
				'post_type' => 'page',
				'orderby' => 'menu_order',
				'order'   => 'ASC',
				'post_parent'    => $post->ID,
			);
	$query = new WP_Query( $args );
	
	//echo '<pre>';
	//var_dump($query);
	//echo '</pre>';
	
	if ( $query->have_posts() ) {
			$i = 0;
			$b = 0;
		while ( $query->have_posts() ) {
				$query->the_post();
				$child_page_id = get_the_ID();
		?>
			<button style="padding: 5px; margin: 5px 0;" class="accordion"><?php echo  get_the_title();?><i class="fa fa-angle-down" aria-hidden="true"></i></button>		
			<div  class="panel" style="padding: 10px; border-radius: 5px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35);">
				<div id="print-<?php echo $i; ?>">
					<p><?php echo  the_content(); ?></p>
					
					<?php $rows = get_field('file_repeater');
						
						if($rows) { ?>
							<h4 style="color:#044a85; margin-top: 10px;">Αρχεία</h4>
							<ul id="ul_accordion">
						<?php foreach($rows as $row) { ?>
								<li>	
									<p class="accordion-file"><a target="_blank" href="<?php echo $row['add_file']; ?>"><i class="fa fa-arrow-circle-o-down fa-accordion-file" aria-hidden="true"></i><?php echo $row['file_name']; ?></a></p>	
								</li>
						<?php } ?>
							</ul>
						<?php } ?>
				</div>
		<?php	
			$params = array('posts_per_page' => -1,
			'post_type' => 'page',
			'orderby' => 'menu_order',
			'order'   => 'ASC',
			'post_parent'    => $child_page_id,
			);
			$subquery = new WP_Query( $params );
			
			if ( $subquery->have_posts() ) {
						
				while ( $subquery->have_posts() ) {
					$subquery->the_post();
					++$b;
		?>
					<button style="padding: 5px; margin: 5px 0;" class="accordion2"><?php echo  get_the_title();?><i class="fa fa-angle-down" aria-hidden="true"></i></button>
					<div class="panel2" style="padding: 10px; border-radius: 5px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35);">
						<div id="insideprint-<?php echo $b; ?>">
							<p><?php echo  the_content(); ?> </p>
							<?php $rows = get_field('file_repeater');
								if($rows) {
							?>
									<h4 style="color:#044a85;">Αρχεία</h4>
									<ul id="ul_accordion">
							<?php	foreach($rows as $row) { ?>
									<li>	
										<p class="accordion-file"><a target="_blank" href="<?php echo $row['add_file']; ?>"><i class="fa fa-arrow-circle-o-down fa-accordion-file" aria-hidden="true"></i><?php echo $row['file_name']; ?></a></p>
									</li>
							<?php	} ?>
									</ul>
						<?php	} ?>
						</div>
						<button type="button" onclick="printContent('insideprint-<?php echo $b;?>')" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i> Εκτύπωση</button>
					</div>
		<?php 	} 
			}
		?>
		</div>
	<?php } wp_reset_postdata(); 	
		
	}

	if ($cmsmasters_layout == 'fullwidth' && $content_start === '[cmsmasters_row') {
		echo '<div class="content_wrap ' . $cmsmasters_layout . 
		((is_singular('project')) ? ' project_page' : '') . 
		((is_singular('profile')) ? ' profile_page' : '') . 
		'">' . "\n\n" . 
			'<div class="middle_content entry">';
	}
	
	
	wp_link_pages(array( 
		'before' => '<div class="subpage_nav">' . '<strong>' . esc_html__('Pages', 'dream-city') . ':</strong>', 
		'after' => '</div>' . "\n", 
		'link_before' => ' [ ', 
		'link_after' => ' ] ' 
	));
	
	
	comments_template();
endif;


echo '</div>' . "\n" . 
'<!-- _________________________ Finish Content _________________________ -->' . "\n\n";


if ($cmsmasters_layout == 'r_sidebar') {
	echo "\n" . '<!-- _________________________ Start Sidebar _________________________ -->' . "\n" . 
	'<div class="sidebar">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- _________________________ Finish Sidebar _________________________ -->' . "\n";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo "\n" . '<!-- _________________________ Start Sidebar _________________________ -->' . "\n" . 
	'<div class="sidebar fl">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- _________________________ Finish Sidebar _________________________ -->' . "\n";
}


get_footer();
?>
<script type="text/javascript">


var acc = document.getElementsByClassName("accordion");
var panel = document.getElementsByClassName('panel');

for (var i = 0; i < acc.length; i++) {
    acc[i].onclick = function() {
    	var setClasses = !this.classList.contains('active');
        setClass(acc, 'active', 'remove');
        setClass(panel, 'show', 'remove');
        
       	if (setClasses) {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
}

function setClass(els, className, fnName) {
    for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
    }
}

var acc2 = document.getElementsByClassName("accordion2");
var panel2 = document.getElementsByClassName('panel2');

for (var i = 0; i < acc2.length; i++) {
    acc2[i].onclick = function() {
    	var setClasses2 = !this.classList.contains('active');
        setClass2(acc2, 'active', 'remove');
        setClass2(panel2, 'show', 'remove');
        
       	if (setClasses2) {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
}

function setClass2(els, className, fnName) {
    for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
    }
}


function printContent(el){
	var divContents = $("#" + el).html();
            var printWindow = window.open('', '', 'height=400,width=800');
            //printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
			

}


</script>

