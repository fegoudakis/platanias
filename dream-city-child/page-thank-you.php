<?php
/**
 * Template name: Thank you Form
 *
 * 
 */
get_header();
?>
    <div style="margin: 80px; text-align: center;">
        <h3>
			<?php
			$new_post_id = $_GET["thankid"];
			
			$name = get_field('issue_number', $new_post_id );
			//echo $name;
		
			?>
            Έχουμε λάβει το αίτημά σας. Ο κωδικός αριθμός του αιτήματός σας είναι <?php echo '<b>'; echo $name; echo '</b>';?>. Μπορείτε να δείτε την πορεία του εδώ <a target="_blank" href="<?php echo get_site_url();?>/anazitisi-etiseon/?issue_number=<?php echo $name; ?>">http://beta.platanias.gr/anazitisi-etiseon/?issue_number=<?php echo $name; ?></a>. Σας ευχαριστούμε!	

        </h3>
    </div>
	


<?php get_footer(); ?>