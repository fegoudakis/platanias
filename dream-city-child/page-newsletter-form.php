<?php
/**
 * Template name: Newsletter Form
 *
 * 
 */

acf_form_head();
get_header();
?>

<?php if( isset($_GET['updated']) && ($_GET['updated'] == 'true' ) ): ?>
	

    <div style="margin: 80px; text-align: center;">
        <h3>
            Μεταβείτε στην διεύθυνση του ηλεκτρονικού σας ταχυδρομείου <br> για να επιβεβαιώσετε την εγγραφή σας στη λίστα ενημέρωσης. <br> Σας ευχαριστούμε.
        </h3>
    </div>
	
<?php else: ?>

    <div style="margin: 80px;  text-align: center;">
        <h3>
            Εγγραφείτε στη λίστα ενημέρωσης του Δήμου Πλατανία <br>συμπληρώνοντας την παρακάτω φόρμα.
        </h3>
    </div>

<?php

    $options = array(

        /* (string) Unique identifier for the form. Defaults to 'acf-form' */
        'id' => 'newsletter',

        /* (int|string) The post ID to load data from and save data to. Defaults to the current post ID. 
        Can also be set to 'new_post' to create a new post on submit */
        'post_id' => 'new_post',

        /* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
        The above 'post_id' setting must contain a value of 'new_post' */
        'new_post'		=> array(
                        'post_type'		=> 'newsletters',
                        'post_status'	=> 'draft',
                    ),

        /* (array) An array of field group IDs/keys to override the fields displayed in this form */
        'fields' => array('field_59b8ebd08735e','field_59b7b866e2ac3','field_59b7b886e2ac4','field_59b7b897e2ac5','field_59b7b897e2ac','field_59b7b8a8e2ac6'),

        /* (boolean) Whether or not to show the post title text field. Defaults to false */
       // 'post_title' => array('newsletter'),
        'post_title' => false,

        /* (boolean) Whether or not to show the post content editor field. Defaults to false */
        'post_content' => false,

        /* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
        A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
        'return' => '?updated=true',

        /* (string) The text displayed on the submit button */
        'submit_value' =>  __("Εγγραφή", 'acf'),

        /* (string) A message displayed above the form after being redirected. Can also be set to false for no message */
        'updated_message' =>  __("Σας ευχαριστούμε, έχουμε λάβει το αίτημα σας και το επεξεργαζόμαστε.", 'acf'),

        /* (string) Whether to use the WP uploader or a basic input for image and file fields. Defaults to 'wp' 
        Choices of 'wp' or 'basic'. Added in v5.2.4 */
        'uploader' => 'wp',
		
		'html_before_fields' => '<input type="text" id="isnewsletterform" name="isnewsletterform" value="yes" style="display:none;">',
		//'html_after_fields' => '<input type="text" id="isrequestform" name="isrequestform" value="yes" style="display:none;">',
        );



    acf_form($options); 
?>



<?php endif; ?>

<?php get_footer(); ?>