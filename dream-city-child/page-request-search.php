<?php
/**
 * The template for displaying dashboard
 *
 * Template Name: Aitiseis Search
 *
 */

	get_header();
?>	
<?php
			$taxQuery = array(
				'relation' => 'AND',
			);
			$metaQuery = array(
				'relation' => 'AND',
			);
			
			$miniQuery = array(
				'key' => 'issue_number',
				'value' => $_GET['issue_number'],
				'compare' => '=',
			);
			array_push($metaQuery, $miniQuery);
			
			
			$args = array(
				'posts_per_page' => -1,
				'post_type'  => 'request',
				'post_status'=>'any',
				'meta_query'  => $metaQuery,

				);
						
			
	$apotelesmata = new WP_Query( $args );
	
		$temp_query = $wp_query;
		$wp_query   = NULL;
		$wp_query   = $apotelesmata;
		
		/*echo('<pre>');
		print_r ($apotelesmata);
		echo('</pre>');*/
		
		
?>   

<div class="search_bar_wrap" style="margin-top:50px;">

	<form id="formid-request" method="get"  class="horizontal-form" action="<?php echo esc_attr( site_url() ); ?>/anazitisi-etiseon/" >
		
			<p class="search_field">
				<span> <strong>Κωδικός Αιτήματος</strong> </span>
				<input  placeholder="Κωδικός Αιτήματος" type="text" name="issue_number" value="<?php echo $_GET['issue_number']; ?>" style="display:inline-block; margin-top: 9px;">
			</p>

			<p style="text-align:center;">
				<button style="margin-top: 15px;" type="submit" class="cmsmasters_theme_icon_search">   Κατάσταση</button>
			</p>
			
	</form>

</div>

<?php
		
		
	if ( $apotelesmata->have_posts() ) {
		while ( $apotelesmata->have_posts() ) : $apotelesmata->the_post();
		
			//echo get_the_title();
			//echo'<br>';
			$id = get_the_id();
			//echo get_the_id();
			//echo'<br>';
			$terms = wp_get_post_terms( $id, 'katastasi');
			$term_ipiresia = wp_get_post_terms( $id, 'armodia_ipiresia');
			//echo $term_ipiresia[0]->name ;	
			//var_dump($term_ipiresia);	
		?>
		
		<div style="text-align:center; margin-bottom:150px;">
			<h3>
			<?php if (($term_ipiresia) || ($terms)) { ?>
				Το αίτημά σας <?php //echo get_the_title(); ?>με κωδικό: <?php echo '<b>'; echo $_GET['issue_number']; echo '</b>';?>  το χειρίζεται η υπηρεσία : <?php echo $term_ipiresia[0]->name; ?> και είναι σε κατάσταση: <?php echo $terms[0]->name; ?>				
				
			<!--	Το αίτημά σας <?php //echo get_the_title(); ?>με κωδικό: <?php echo '<b>'; echo $_GET['issue_number']; echo '</b>';?>   είναι σε κατάσταση: <?php echo $terms[0]->name; ?>-->
			<?php } else { ?>
				Το αίτημά σας <?php //echo get_the_title();?>με κωδικό: <?php echo '<b>'; echo $_GET['issue_number']; echo '</b>';?>  δεν έχει προτεραιοποιηθεί.	
			<?php } ?>
			</h3>
		</div>
		
		<?php	
		endwhile;
				 
	} 

?>
   
<?php get_footer(); ?>
