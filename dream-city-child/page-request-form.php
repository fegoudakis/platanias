<?php
/**
 * Template name: Request Form
 *
 * 
 */

acf_form_head();
get_header();
?>


<?php
/*
$katastasi = get_the_terms(71895, 'armodia_' );
$armodia = get_the_terms(71895, 'armodia_ipiresia' );
var_dump($armodia);*/
/*
$blogusers = get_users( 'role=administrator' );
// Array of WP_User objects.
foreach ( $blogusers as $user ) {
	echo '<span>' . esc_html( $user->user_email ) . '</span><br>';
}
*/
if( isset($_GET['updated']) && ($_GET['updated'] == 'true' ) ): ?>
	


    <div style="margin: 80px; text-align: center;">
        <h3>
            Έχουμε λάβει το αίτημα σας και το επεξεργαζόμαστε.<br>
            Σας ευχαριστούμε.
			<?php 
		/*	$pageid = $_POST['issubmitform'];
			
			$new_post_id = $_GET["updated"];
			echo $pageid;*/
			?>
        </h3>
    </div>
	
<?php else: ?>
 
    <div style="margin-top: 80px;  text-align: center;">
        <h3>
            Καλώς ορίσατε στο κέντρο ηλεκτρονικής διακυβέρνησης και εξυπηρέτησης του Δήμου.<br>
			Σε αυτη την φάση υποστηρίζεται η υποβολή αιτημάτων, προβλημάτων στις υπηρεσίες του Δήμου καθώς και η αναζήτηση της κατάστασης ενός αιτήματος.
        </h3>
		
    </div>
	
	<div style="margin-bottom:50px; margin-top:50px;" >
	  <div class="row">
		<div style="text-align: center;" class="col-md-6 kataxwrisi">
		  <img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/kataxwrise_eswt.png" alt="">
		</div>
		<div style="text-align: center;" class="col-md-6 anazitisi">
		  <a target="_blank" href="<?php echo get_site_url(); ?>/anazitisi-etiseon/" ><img src="<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/anazhthse_eswt.png" alt=""></a>
		</div>
	  </div>
	</div>
	

<?php

    $options = array(

        /* (string) Unique identifier for the form. Defaults to 'acf-form' */
        'id' => 'request-form',

        /* (int|string) The post ID to load data from and save data to. Defaults to the current post ID. 
        Can also be set to 'new_post' to create a new post on submit */
        'post_id' => 'new_post',

        /* (array) An array of post data used to create a post. See wp_insert_post for available parameters.
        The above 'post_id' setting must contain a value of 'new_post' */
        'new_post'		=> array(
                        'post_type'		=> 'request',
                        'post_status'	=> 'draft',
                    ),

        /* (array) An array of field group IDs/keys to override the fields displayed in this form */
        'fields' => array('field_5968876f89b30',
						'field_59e4d14a52fbc',
						'field_5968882f89b31',
						'field_5968885d89b32',						
						'field_596883ea165eb',						
						'field_596885518bed8',
						'field_596885ae8bed9',
						'field_596885bc8beda',
						'field_596885d98bedb',
						'field_596886c789b2e',						
						'field_596886ed89b2f',
						'field_596885f78bedd',
						'field_596886118bede',
						'field_596885e78bedc',
						'field_5968862f8bedf',
						'field_5968864889b2a',
						'field_5968866389b2b',						
						'field_5968869989b2d',
						'field_5968867589b2c',
						'field_596884f98bed7',						
						'field_59ba3cc106ec7',

					),

        /* (boolean) Whether or not to show the post title text field. Defaults to false */
        'post_title' => false,

        /* (boolean) Whether or not to show the post content editor field. Defaults to false */
        'post_content' => false,

        /* (string) The URL to be redirected to after the form is submit. Defaults to the current URL with a GET parameter '?updated=true'.
        A special placeholder '%post_url%' will be converted to post's permalink (handy if creating a new post) */
        'return' => '?updated=true',

        /* (string) The text displayed on the submit button */
        'submit_value' =>  __("Αποστολή", 'acf'),

        /* (string) A message displayed above the form after being redirected. Can also be set to false for no message */
        'updated_message' =>  __("Σας ευχαριστούμεdadsa, έχουμε λάβει το αίτημα σας και το επεξεργαζόμαστε.", 'acf'),

        /* (string) Whether to use the WP uploader or a basic input for image and file fields. Defaults to 'wp' 
        Choices of 'wp' or 'basic'. Added in v5.2.4 */
        'uploader' => 'wp',
		
		'html_before_fields' => '<input type="text" id="issubmitform" name="issubmitform" value="yes" style="display:none;">',
		'html_after_fields' => '<input type="text" id="isrequestform" name="isrequestform" value="yes" style="display:none;">',

        );



    acf_form($options); 
?>

<?php endif; ?>

<?php get_footer(); ?>

<script type="text/javascript">
$(document).ready(function(){
		
	$('.kataxwrisi').css('cursor', 'pointer');
	
	$('.kataxwrisi').hover(
		function(){
		$('.kataxwrisi img').attr("src" , "<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/kataxwrise_eswt_h.png");
		  },
		function(){
			$('.kataxwrisi img').attr("src" , "<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/kataxwrise_eswt.png");
		}
    );
	
	$('.anazitisi').hover(
		function(){
		$('.anazitisi img').attr("src" , "<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/anazhthse_eswt_h.png");
		  },
		function(){
			$('.anazitisi img').attr("src" , "<?php echo get_site_url(); ?>/wp-content/uploads/2017/09/anazhthse_eswt.png");
		}
    );

	$(".kataxwrisi").click(function() {
		$('html, body').animate({
			scrollTop: $("#request-form").offset().top -140
		}, 2000);
	
	}); 
	

});
</script>