<?php
/**
 * The template for displaying dashboard
 *
 * Template Name: Meletes Search
 *
 */

get_header();

		$args_dates = array(
            'posts_per_page'      => -1,
            'post_type' => 'meletes'
            );
        $etos_ekdosis = query_posts( $args_dates );    
        $valid_year = array();
        $mitrwa = array();
        foreach ($etos_ekdosis as $year) {
            $the_year = get_field( 'etos_ekdosis', $year->ID );
			//echo $the_year.'<br>';
			$ar_mitrwou = get_field( 'arithmos_mitrwou', $year->ID );
            
			$lenght = strlen($the_year);
            
            if ($lenght == 4) {
                array_push($valid_year, (string)$the_year);
            }
			
			if ($ar_mitrwou) {
                array_push($mitrwa, $ar_mitrwou);
            }
             

        }    
        $valid_years = array_unique($valid_year);
        rsort($valid_years);
        //var_dump($valid_years);
        //var_dump($mitrwa);
        
        $args_ipiresies = array(
            'posts_per_page'      => -1,
            'post_type' => 'meletes'
            );
        $ipiresia = query_posts( $args_ipiresies );    
        $valid_ipiresia = array();
        foreach ($ipiresia as $service) {
            $the_service = get_field( 'thematiki_enotita', $service->ID );
            
            $lenght = strlen($the_year);
            
            if ($lenght == 4) {
                array_push($valid_ipiresia, (string)$the_service);
            }
             

        }    
        $valid_ipiresies = array_unique($valid_ipiresia);
        rsort($valid_ipiresies);
		//var_dump($valid_ipiresies);
		
		
		
		if(isset($_GET['etos_ekdosis']) || isset($_GET['etos_ekdosis']) || isset($_GET['keyword']) ){ 
			
			$taxQuery = array(
				'relation' => 'AND',
			);
			$metaQuery = array(
				'relation' => 'AND',
			);	
			
			
			if( ($_GET['start_date'] !== '') && ($_GET['end_date'] !== '') ){
				
				$miniQuery = array(
					'relation' => 'AND',
					array(
						'key' => 'etos_ekdosis',
						'value' => $_GET['start_date'],
						'compare' => '>=',
					),
					array(
						'key' => 'etos_ekdosis',
						'value' => $_GET['end_date'],
						'compare' => '<=',
					)
				);
				array_push($metaQuery, $miniQuery);
				
			}
			else{
				
				if( $_GET['start_date'] !== '' ){
					$miniQuery = array(
						'key' => 'etos_ekdosis',
						'value' => $_GET['start_date'],
						'compare' => '>=',
					);
					array_push($metaQuery, $miniQuery);
				}
				
				if( $_GET['end_date'] !== '' ){
					$miniQuery = array(
						'key' => 'etos_ekdosis',
						'value' => $_GET['end_date'],
						'compare' => '<=',
					);
					array_push($metaQuery, $miniQuery);
				}
				
			}

			$miniQuery = array(
				'key' => 'thematiki_enotita',
				'value' => $_GET['thematiki_enotita'],
				'compare' => 'LIKE',
			);
			array_push($metaQuery, $miniQuery);
			
			$miniQuery = array(
				'value' => $_GET['keyword'],
				'compare' => 'LIKE',
			);
			array_push($metaQuery, $miniQuery);
			
			$miniQuery = array(
				'value' => $_GET['mitrwo'],
				'compare' => 'LIKE',
			);
			array_push($metaQuery, $miniQuery);
		
			$tag = get_term_by('name', esc_sql($_GET['keyword']), 'post_tag');
			$tagid = $tag->slug;
			echo esc_sql($tagid);
			
			$miniQuery = array(
				'taxonomy' => 'post_tag',
				'field' => 'slug',
				'terms' => esc_sql($tagid),
				'operator' => 'IN',
			);
			array_push($taxQuery, $miniQuery);
			
		if(isset($_GET['number'])){
		  $showposts = esc_sql($_GET['number']);
		}else{
		  $showposts = 10;
		}			

		//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
		$args = array(
			'showposts'=> $showposts,
			'posts_per_page' => $posts_per_page,
			'post_type'  => 'meletes',
			//'tag_slug_in' => esc_sql($tagid),
			'paged'	 => $paged,			
			'meta_query'  => $metaQuery,
			//'tax_query'  => array(
			//	'relation' => 'AND',
			//	$taxQuery
			//)
			
		);

			
		} else {
			//$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
			$args = array(
				'showposts'=> $showposts,
				'posts_per_page' => $posts_per_page,
				'post_type'  => 'meletes',
				'paged'	 => $paged,
				//'tag' => 'etiketa2',
			);			
		}

		$apofasis = new WP_Query( $args );
		$temp_query = $wp_query;
		$wp_query   = NULL;
		$wp_query   = $apofasis;
		
		// echo '<pre>';
		// var_dump($apofasis);
		// echo '</pre>';

?>
 
<?php
// $te = get_field('etos_ekdosis',71612);
// echo $te;

// $b = $_GET['start_date']; 
// if($te == $b){
	// echo 'OLE';
// }
//echo $_GET['end_date']; 
$tag = get_term_by('name', $_GET['keyword'], 'post_tag');
$tagid = $tag->term_id;	
//echo $tagid;

?>
<div id ="outer-form-meletes" class="search_bar_wrap" style="margin-top:50px; ">
<?php echo do_shortcode( '[stixia_epikoinwnias]'); ?>
	<form id="form-meletes" method="get"  class="horizontal-form" action="<?php echo esc_attr( site_url() ); ?>/anazitisi-meleton/" >
	
		<div class="row">
			<div class="col-md-12">
				<p class="search_field">
					<span> <strong>Αναζήτηση με λέξεις κλειδιά</strong> </span>
					<input  placeholder="Αναζήτηση με λέξεις κλειδιά" type="text" name="keyword" value="<?php echo $_GET['keyword']; ?>" style="display:inline-block; margin-top: 9px;">
				</p>
			</div>
		</div>
	
		<div class="row">
			<div class="col-md-3">	
				<ul class="the_serchss">
					<span> <strong>Αναζήτηση με βάση το έτος έκδοσης</strong> </span>
					<li>
					
					<span> <strong>Από</strong> </span>
					<input type="date"  name="start_date" value="<?php echo $_GET['start_date']; ?>">
					<span> <strong>Έως</strong> </span>
					<input type="date"  name="end_date" value="<?php echo $_GET['end_date']; ?>">

						<!--<select name="etos_ekdosis" style="display:inline-block; height: 53px; width:90%;">
							<option value="" selected > Ημερομηνία Έκδοσης </option>
							
							<?php foreach ($valid_years as $year) { ?>
								<option value="<?php echo $year; ?>" <?php if ($_GET['etos_ekdosis'] == $year) {echo "selected";} ?>><?php echo $year; ?></option>
							<?php } ?>	-->
						<!--	
							<option value="2013"<?php if ($_GET['etos_ekdosis'] == 2013) {echo "selected";} ?>>2013</option>
							<option value="2014"<?php if ($_GET['etos_ekdosis'] == 2014) {echo "selected";} ?>>2014</option>
							<option value="2015"<?php if ($_GET['etos_ekdosis'] == 2015) {echo "selected";} ?>>2015</option>
							<option value="2016"<?php if ($_GET['etos_ekdosis'] == 2016) {echo "selected";} ?>>2016</option>
						-->
				<!--		</select> -->
					</li>
					

				</ul>
			</div>
			
			<div class="col-md-3">									
				<ul class="the_serchss">
					<span> <strong>Αναζήτηση με βάσης την Θεματική Ενότητα</strong> </span>
					<li>
					<select name="thematiki_enotita" style="display:inline-block; height: 53px; width:90%;">
						<option value="" selected > Θεματική Ενότητα </option>
						
						<!--<?php foreach ($valid_ipiresies as $ipire) { ?>
							<option value="<?php echo $ipire; ?>" <?php if ($_GET['thematiki_enotita'] == $ipire) {echo "selected";} ?>><?php echo $ipire; ?></option>
						<?php } ?>
						-->
						
						<option value="oey"<?php if ($_GET['thematiki_enotita'] == 'oey') {echo "selected";} ?>>Διάρθρωση Υπηρεσιών Δήμου (ΟΕΥ)</option>
						<option value="kep"<?php if ($_GET['thematiki_enotita'] == 'kep') {echo "selected";} ?>>Κέντρα Εξυπηρέτησης του Πολίτη (ΚΕΠ)</option>
						<option value="nomika"<?php if ($_GET['thematiki_enotita'] == 'nomika') {echo "selected";} ?>>Νομικά Πρόσωπα & Επιχειρήσεις</option>
						
					</select>
					</li>	
				</ul>
			</div>			
			
			<div class="col-md-3">									
				<ul class="the_serchss">
					<span> <strong>Αναζήτηση με βάσης τον Αρ. Μητρώου</strong> </span>
						<li>						
							<input  placeholder="Αναζήτηση με Αρ. Μητρώου" type="text" name="mitrwo" value="<?php echo $_GET['mitrwo']; ?>" style="display:inline-block; margin-top: 9px;">	
						</li>				
				</ul>
			</div>

			<div class="col-md-3">									
				<ul class="the_serchss">
					<span> <strong>Αριθμός στοιχείων ανά σελίδα στον πίνακα Μελέτες</strong> </span><br>
						<li>
						
							<select  class="field" name="number">
								<option value="5"<?php if ($_GET['number'] == 5) {echo "selected";} ?>>5</option>
								<option value="10"<?php if ($_GET['number'] == 10) {echo "selected";} ?>>10</option>
								<option value="15"<?php if ($_GET['number'] == 15) {echo "selected";} ?>>15</option>
								<option value="20"<?php if ($_GET['number'] == 20) {echo "selected";} ?>>20</option>
							</select> 	
						</li>				
				</ul>
			</div>			
		</div>
		
		<div class="row">
			<div class="col-md-5">
			</div>
			<div class="col-md-2">
				<p style="text-align:center;">
					<button style="margin-top: 15px;" type="submit" class="cmsmasters_theme_icon_search">   Αναζήτηση</button>
				</p>
			</div>
			<div class="col-md-5">
			</div>
		</div>
	
	<!--
		<div class="row">	
			<div class="col-md-12">
				<p style="text-align:center;">
					<button style="margin-top: 15px;" type="submit" class="cmsmasters_theme_icon_search">   Αναζήτηση</button>
				</p>
			</div>
		</div>
	-->
		
	</form>

</div>

    <?php
	$count = 0;
        if ( $apofasis->have_posts()  ) : ?>

			<div class="cmsmasters_column_inner" style="margin-bottom: 100px;">
				<table class="cmsmasters_table">
					<caption style="font-size:25px;">Μελέτες Δήμου</caption>

					<thead>
						<tr class="cmsmasters_table_row_header">
							<th class="cmsmasters_table_cell_aligncenter">A/A</th>
							<th class="cmsmasters_table_cell_aligncenter">Τίτλος</th>
							<th class="cmsmasters_table_cell_aligncenter">Ημερομηνία Έκδοσης</th>
							<th class="cmsmasters_table_cell_aligncenter">Θεματική Ενότητα</th>
							<th class="cmsmasters_table_cell_aligncenter">Προβολή</th>
						</tr>
					</thead>
					
					<tbody>
<?php     while ( $apofasis->have_posts() ) : $apofasis->the_post();
		++$count;
	//$the_epitropous = wp_get_post_terms($post->ID, 'epitropi', array("fields" => "names"));
?>			
						<tr>
							<td class="cmsmasters_table_cell_aligncenter"><?php echo $count; ?></td>
							<td class="cmsmasters_table_cell_aligncenter"><?php echo get_the_title( ); ?><!--</a>--></td>										
							<td class="cmsmasters_table_cell_aligncenter"><?php echo get_field( 'etos_ekdosis' ); ?></td>
							<td class="cmsmasters_table_cell_aligncenter"><?php echo get_field( 'thematiki_enotita' ); ?></td>
							<td class="cmsmasters_table_cell_aligncenter">
							<?php if (get_field( 'episinaptomeno_arxeio' )) : ?>
								<a target="_blank" href="<?php echo get_field( 'episinaptomeno_arxeio' ); ?>"><i style="font-size: 18px;" class="fa fa-eye" aria-hidden="true"></i></a> <br>
							<?php endif; ?>
							</td>
							
						</tr>

<?php endwhile; 
?>
					</tbody>
					<tfoot>
						<tr class="cmsmasters_table_row_footer">
							<td class="cmsmasters_table_cell_aligncenter">A/A</td>
							<td class="cmsmasters_table_cell_aligncenter">Τίτλος</td>
							<td class="cmsmasters_table_cell_aligncenter">Ημερομηνία Έκδοσης</td>
							<td class="cmsmasters_table_cell_aligncenter">Θεματική Ενότητα</td>
							<td class="cmsmasters_table_cell_aligncenter">Προβολή</td>
						</tr>
					</tfoot>
				</table>
			</div>

            <!-- pagination -->
            <div style="clear:both"></div>

	          <?php  else: ?>
	<div>
		
		<p style="font-weight:bold;">Δεν βρέθηκαν αποτελέσματα από την αναζήτηση σας.<p>
		<p>Δοκιμάστε να κάνετε μια καινούργια αναζήτηση,  <a href="<?php echo get_site_url().'/anazitisi-meleton'; ?>">πηγαίνοντας εδώ.</a><p>
	
	</div> 
		          
		     <?php     endif;
	        wp_reset_postdata();
	        

            ?>

	<div class="navigation1">
<?php 	pagination($apofasis->max_num_pages); ?>
	</div> 
<?php get_footer(); ?>
