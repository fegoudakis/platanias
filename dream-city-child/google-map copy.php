<?php

function some_random_code(){
?>	

	<div class="acf-map row" style="margin-bottom:45px; position:relative;">			  
		<div id="map" class="acf-map" style="height:500px;">	

		</div>	
		<div id="buttons"  style="margin-bottom:50px; border:1px solid black; z-index:999; position:absolute; top:135px;">
		<label style="margin:2px 2px 0px 2px;"><input id ="second" type="checkbox" name="filter" value="Μουσείο" class='chk-btn' onchange="selectAllChecked();">Μουσείο</label>
		<br>
		<label style="margin:2px 2px 0px 2px;"><input id ="third" type="checkbox" name="filter" value="Πάρκο" class='chk-btn' onchange="selectAllChecked();">Πάρκο</label>

		</div>		
	</div>       
<?php
	$repeater = get_field('google_map');
	?>

 <script>

	var gmarkers1 = [];
	
    function initMap()	{
      var options = {
        zoom:8,
        center:{lat:42.3601,lng:-71.0589}
      }


    
	var locations = [

	<?php foreach($repeater as $locs): ?>
      ["<?php echo $locs["location"]["address"]; ?>", "<?php echo $locs["location"]["lat"]; ?>", "<?php echo $locs["location"]["lng"]; ?>", "<?php echo $locs["category"]; ?>", "<?php echo $locs["image"]; ?>","<?php echo $locs["title"]; ?>", "<?php echo $locs["info"]; ?>",],
     
    <?php endforeach; ?>
    ];
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(35.516702, 23.908920),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
	
    for (i = 0; i < locations.length; i++) { 
	
		if (locations[i][3] == "Μουσείο"){
			var category = locations[i][3];
			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			icon:'http://cpdev.crowdpolicy.com/platanias/wp-content/uploads/2017/07/rsz_1museum-icon.png',
			category : category,
			map: map
		  });
		}else{
			var category = locations[i][3];
			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			icon:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
			category : category,
			map: map
		  });
		  }
		
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
			if( (locations[i][4]) != ""){

          
          infowindow.setContent(locations[i][5]+'<br/><img  height="42" width="42" src="'+locations[i][4]+'"></br>'+locations[i][6]);
          
			}else {
				infowindow.setContent(locations[i][5]);
			}
          infowindow.open(map, marker);
        }
      })(marker, i));
	 gmarkers1.push(marker);
  
   }
		
		 selectAllChecked = function() {
		  var checkedPlace = [] 
		  var allCheckedElem = document.getElementsByName('filter');
		  for (var i = 0; i < allCheckedElem.length; i++) {
			if (allCheckedElem[i].checked == true) {
			  checkedPlace.push(allCheckedElem[i].value)
			}
		  }

		  filterChecker(checkedPlace) 
		
		
			 function filterChecker(category) {
			  //console.log(tip);
			  for (i = 0; i < locations.length; i++) {
				marker = gmarkers1[i];
				//console.log(marker);
				if (in_array(marker.category) != -1) {
				  marker.setVisible(true);
				} else {
				  marker.setVisible(false);
				}
			  }
			}
			
			function in_array(needle){
			 
			  for (var i = 0; i < checkedPlace.length; i++) {
				if (needle == checkedPlace[i] || needle == 'all') 	
					return i;
				console.log(needle);
				}
			  return -1;
			}
		
		}
}	

  </script>
  
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8HqS0Ers_lU2INlPls71aEMf1zAHwdgk&callback=initMap">
    </script>



<?php } ?>

