<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City Child
 * @version		1.0.0
 * 
 * Child Theme Functions File
 * Created by CMSMasters
 * 
 */


function dream_city_child_enqueue_styles() {
    wp_enqueue_style('dream-city-child-style', get_stylesheet_uri(), array('dream-city-style'), '1.0.0', 'screen, print');
}

add_action('wp_enqueue_scripts', 'dream_city_child_enqueue_styles', 9);
?>
<?php
/*
 * Alters event's archive titles
 */
function tribe_alter_event_archive_titles ( $original_recipe_title, $depth ) {
	// Modify the titles here
	// Some of these include %1$s and %2$s, these will be replaced with relevant dates
	$title_upcoming =   'Επόμενες Εκδηλώσεις'; // List View: Upcoming events
	$title_past =       'Προηγούμενες Εκδηλώσεις'; // List view: Past events
	$title_range =      'Εκδηλώσεις από %1$s μέχρι %2$s'; // List view: range of dates being viewed
	$title_month =      'Εκδηλώσεις %1$s'; // Month View, %1$s = the name of the month
	$title_day =        'Εκδηλώσεις για %1$s'; // Day View, %1$s = the day
	$title_all =        'Όλες οι Εκδηλώσεις για %s'; // showing all recurrences of an event, %s = event title
	$title_week =       'Εκδηλώσεις για τη βδομάδα %s'; // Week view
	// Don't modify anything below this unless you know what it does
	global $wp_query;
	$tribe_ecp = Tribe__Events__Main::instance();
	$date_format = apply_filters( 'tribe_events_pro_page_title_date_format', tribe_get_date_format( true ) );
	// Default Title
	$title = $title_upcoming;
	// If there's a date selected in the tribe bar, show the date range of the currently showing events
	if ( isset( $_REQUEST['tribe-bar-date'] ) && $wp_query->have_posts() ) {
		if ( $wp_query->get( 'paged' ) > 1 ) {
			// if we're on page 1, show the selected tribe-bar-date as the first date in the range
			$first_event_date = tribe_get_start_date( $wp_query->posts[0], false );
		} else {
			//otherwise show the start date of the first event in the results
			$first_event_date = tribe_event_format_date( $_REQUEST['tribe-bar-date'], false );
		}
		$last_event_date = tribe_get_end_date( $wp_query->posts[ count( $wp_query->posts ) - 1 ], false );
		$title = sprintf( $title_range, $first_event_date, $last_event_date );
	} elseif ( tribe_is_past() ) {
		$title = $title_past;
	}
	// Month view title
	if ( tribe_is_month() ) {
		$title = sprintf(
			$title_month,
			date_i18n( tribe_get_option( 'monthAndYearFormat', 'F Y' ), strtotime( tribe_get_month_view_date() ) )
		);
	}
	// Day view title
	if ( tribe_is_day() ) {
		$title = sprintf(
			$title_day,
			date_i18n( tribe_get_date_format( true ), strtotime( $wp_query->get( 'start_date' ) ) )
		);
	}
	// All recurrences of an event
	if ( function_exists('tribe_is_showing_all') && tribe_is_showing_all() ) {
		$title = sprintf( $title_all, get_the_title() );
	}
	// Week view title
	if ( function_exists('tribe_is_week') && tribe_is_week() ) {
		$title = sprintf(
			$title_week,
			date_i18n( $date_format, strtotime( tribe_get_first_week_day( $wp_query->get( 'start_date' ) ) ) )
		);
	}
	if ( is_tax( $tribe_ecp->get_event_taxonomy() ) && $depth ) {
		$cat = get_queried_object();
		$title = '<a href="' . esc_url( tribe_get_events_link() ) . '">' . $title . '</a>';
		$title .= ' &#8250; ' . $cat->name;
	}
	return $title;
}
add_filter( 'tribe_get_events_title', 'tribe_alter_event_archive_titles', 11, 2 );


/*Create Custom Post Type REQUEST*/
function create_custom_post_type() {

	$labels = array(
		'name'                  => _x( 'Αιτήματα', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Αίτημα', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Αιτήματα', 'text_domain' ),
		'name_admin_bar'        => __( 'Αιτήματα', 'text_domain' ),
		'parent_item_colon'     => __( 'Ανώτερο αίτημα:', 'text_domain' ),
		'all_items'             => __( 'Όλα τα αιτήματα', 'text_domain' ),
		'add_new_item'          => __( 'Δημιουργία αιτήματος', 'text_domain' ),
		'add_new'               => __( 'Δημιουργία αιτήματος', 'text_domain' ),
		'new_item'              => __( 'Δημιουργία αιτήματος', 'text_domain' ),
		'edit_item'             => __( 'Επεξεργασία αιτήματος', 'text_domain' ),
		'update_item'           => __( 'Ενημέρωση αιτήματος', 'text_domain' ),
		'view_item'             => __( 'Προβολή αιτήματος', 'text_domain' ),
		'search_items'          => __( 'Αναζήτηση αιτήματος', 'text_domain' ),
		'not_found'             => __( 'Δεν βρέθηκαν αιτήματα', 'text_domain' ),
		'not_found_in_trash'    => __( 'Δεν βρέθηκαν αιτήματα', 'text_domain' ),
		'items_list'            => __( 'Λίστα αιτημάτων', 'text_domain' ),
		'items_list_navigation' => __( 'Λίστα αιτημάτων', 'text_domain' ),
		'filter_items_list'     => __( 'Φιλτράρισμα αιτημάτων ', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Αίτημα', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'comments', 'author', 'custom-fields', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'request', $args );
	
	
	$labels = array(
		'name'                  => _x( 'Αποφάσεις', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Απόφαση', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Αποφάσεις', 'text_domain' ),
		'name_admin_bar'        => __( 'Αποφάσεις', 'text_domain' ),
		'parent_item_colon'     => __( 'Ανώτερη Απόφαση:', 'text_domain' ),
		'all_items'             => __( 'Όλες οι Αποφάσεις', 'text_domain' ),
		'add_new_item'          => __( 'Δημιουργία Απόφασης', 'text_domain' ),
		'add_new'               => __( 'Δημιουργία Απόφασης', 'text_domain' ),
		'new_item'              => __( 'Δημιουργία Απόφασης', 'text_domain' ),
		'edit_item'             => __( 'Επεξεργασία Απόφασης', 'text_domain' ),
		'update_item'           => __( 'Ενημέρωση Απόφασης', 'text_domain' ),
		'view_item'             => __( 'Προβολή Απόφασης', 'text_domain' ),
		'search_items'          => __( 'Αναζήτηση Απόφασης', 'text_domain' ),
		'not_found'             => __( 'Δεν βρέθηκαν Αποφάσεις', 'text_domain' ),
		'not_found_in_trash'    => __( 'Δεν βρέθηκαν Αποφάσεις', 'text_domain' ),
		'items_list'            => __( 'Λίστα Αποφάσεων', 'text_domain' ),
		'items_list_navigation' => __( 'Λίστα Αποφάσεων', 'text_domain' ),
		'filter_items_list'     => __( 'Φιλτράρισμα Αποφάσεων ', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Απόφαση', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'comments', 'author', 'custom-fields', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'apofasis', $args );
	
	$labels = array(
		'name'                  => _x( 'Newsletters', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Newsletter', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Newsletters', 'text_domain' ),
		'name_admin_bar'        => __( 'Newsletters', 'text_domain' ),
		'parent_item_colon'     => __( 'Ανώτερη Newsletter:', 'text_domain' ),
		'all_items'             => __( 'Όλες οι Newsletters', 'text_domain' ),
		'add_new_item'          => __( 'Δημιουργία Newsletter', 'text_domain' ),
		'add_new'               => __( 'Δημιουργία Newsletter', 'text_domain' ),
		'new_item'              => __( 'Δημιουργία Newsletter', 'text_domain' ),
		'edit_item'             => __( 'Επεξεργασία Newsletter', 'text_domain' ),
		'update_item'           => __( 'Ενημέρωση Newsletter', 'text_domain' ),
		'view_item'             => __( 'Προβολή Newsletter', 'text_domain' ),
		'search_items'          => __( 'Αναζήτηση Newsletter', 'text_domain' ),
		'not_found'             => __( 'Δεν βρέθηκαν Newsletters', 'text_domain' ),
		'not_found_in_trash'    => __( 'Δεν βρέθηκαν Newsletters', 'text_domain' ),
		'items_list'            => __( 'Λίστα Newsletters', 'text_domain' ),
		'items_list_navigation' => __( 'Λίστα Newsletters', 'text_domain' ),
		'filter_items_list'     => __( 'Φιλτράρισμα Newsletters ', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Newsletter', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'comments', 'author', 'custom-fields', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'newsletters', $args );
	
	$labels = array(
		'name'                  => _x( 'Αποθετήριο Μελετών', 'Post Type General Name', 'text_domain' ),
		'singular_name'         => _x( 'Μελέτη', 'Post Type Singular Name', 'text_domain' ),
		'menu_name'             => __( 'Αποθετήριο Μελετών', 'text_domain' ),
		'name_admin_bar'        => __( 'Μελέτες', 'text_domain' ),
		'parent_item_colon'     => __( 'Ανώτερη Newsletter:', 'text_domain' ),
		'all_items'             => __( 'Όλες οι Μελέτες', 'text_domain' ),
		'add_new_item'          => __( 'Δημιουργία Μελέτης', 'text_domain' ),
		'add_new'               => __( 'Δημιουργία Μελέτης', 'text_domain' ),
		'new_item'              => __( 'Δημιουργία Μελέτης', 'text_domain' ),
		'edit_item'             => __( 'Επεξεργασία Μελέτης', 'text_domain' ),
		'update_item'           => __( 'Ενημέρωση Μελέτης', 'text_domain' ),
		'view_item'             => __( 'Προβολή Μελέτης', 'text_domain' ),
		'search_items'          => __( 'Αναζήτηση Μελέτης', 'text_domain' ),
		'not_found'             => __( 'Δεν βρέθηκαν Μελέτες', 'text_domain' ),
		'not_found_in_trash'    => __( 'Δεν βρέθηκαν Μελέτες', 'text_domain' ),
		'items_list'            => __( 'Λίστα Μελέτών', 'text_domain' ),
		'items_list_navigation' => __( 'Λίστα Μελέτών', 'text_domain' ),
		'filter_items_list'     => __( 'Φιλτράρισμα Μελέτών ', 'text_domain' ),
	);
	$args = array(
		'label'                 => __( 'Μελέτη', 'text_domain' ),
		'description'           => __( 'Post Type Description', 'text_domain' ),
		'labels'                => $labels,
		'supports'              => array( 'title', 'editor', 'excerpt', 'comments', 'author', 'custom-fields', ),
		'taxonomies'            => array( 'category', 'post_tag' ),
		'hierarchical'          => false,
		'public'                => true,
		'show_ui'               => true,
		'show_in_menu'          => true,
		'menu_position'         => 5,
		'menu_icon'             => 'dashicons-edit',
		'show_in_admin_bar'     => true,
		'show_in_nav_menus'     => true,
		'can_export'            => true,
		'has_archive'           => true,		
		'exclude_from_search'   => false,
		'publicly_queryable'    => true,
		'capability_type'       => 'post',
	);
	register_post_type( 'meletes', $args );

}
add_action( 'init', 'create_custom_post_type', 0 );

add_action( 'init', 'create_taxonomies' );

function create_taxonomies() {
	register_taxonomy(
		'epitropi',
		'apofasis',
		array(
			'label' => __( 'Επιτρόπη' ),
			'rewrite' => array( 'slug' => 'epitropi' ),
			'hierarchical' => true,
		)
	);
	
	register_taxonomy(
		'katastasi',
		'request',
		array(
			'label' => __( 'Κατάσταση' ),
			'rewrite' => array( 'slug' => 'katastasi' ),
			'hierarchical' => true,
		)
	);	
	
	register_taxonomy(
		'armodia_ipiresia',
		'request',
		array(
			'label' => __( 'Αρμόδια Υπηρεσία' ),
			'rewrite' => array( 'slug' => 'armodia_ipiresia' ),
			'hierarchical' => true,
		)
	);

}

add_filter('manage_request_posts_columns', 'bs_request_table_head'); //dhmiourgia columns
function bs_request_table_head( $defaults ) {
    $defaults['katastasi']  = 'Κατάσταση';
    $defaults['armodia_ipiresia'] = 'Αρμόδια Υπηρεσία';
    return $defaults;
}

add_action( 'manage_request_posts_custom_column', 'bs_request_table_content', 10, 2 );  //emfanisi twn periexomenou sta columns
function bs_request_table_content( $column_name, $post_id ) {

    if ($column_name == 'katastasi') {
		$katastasi = get_the_terms($post_id, 'katastasi' );
			foreach ($katastasi as $kat){
				echo $kat->name;
			}
    }

    if ($column_name == 'armodia_ipiresia') {
		$armodia = get_the_terms($post_id, 'armodia_ipiresia' );
			foreach ($armodia as $arm){
				echo $arm->name;
			}
    }

}



function filter_requests_by_taxonomies( $post_type, $which ) { //Eisagwgi filtrwn me taxonomies

	// Apply this only on a specific post type
	if ( 'request' !== $post_type )
		return;

	// A list of taxonomy slugs to filter by
	$taxonomies = array( 'katastasi', 'armodia_ipiresia');

	foreach ( $taxonomies as $taxonomy_slug ) {

		// Retrieve taxonomy data
		$taxonomy_obj = get_taxonomy( $taxonomy_slug );
		$taxonomy_name = $taxonomy_obj->labels->name;

		// Retrieve taxonomy terms
		$terms = get_terms( $taxonomy_slug );

		// Display filter HTML
		echo "<select name='{$taxonomy_slug}' id='{$taxonomy_slug}' class='postform'>";
		echo '<option value="">' . sprintf( esc_html__( 'Show All %s', 'text_domain' ), $taxonomy_name ) . '</option>';
		foreach ( $terms as $term ) {
			printf(
				'<option value="%1$s" %2$s>%3$s (%4$s)</option>',
				$term->slug,
				( ( isset( $_GET[$taxonomy_slug] ) && ( $_GET[$taxonomy_slug] == $term->slug ) ) ? ' selected="selected"' : '' ),
				$term->name,
				$term->count
			);
		}
		echo '</select>';
	}

}
add_action( 'restrict_manage_posts', 'filter_requests_by_taxonomies' , 10, 2);


/*Exclude Uncategorized from Sidebar*/
function exclude_widget_categories($args)
  {
    $exclude = "1"; // The IDs of the excluding categories
    $args["exclude"] = $exclude;
    return $args;
  }
  add_filter("widget_categories_args","exclude_widget_categories");

function categories_to_project(){
    register_taxonomy_for_object_type( 'category', 'project' );
}
add_action( 'init', 'categories_to_project' );

/*
function change_input_labels($field) {

	if($field['name'] == '_post_title')  {
		$field['label'] = "Συνοπτική Περιγραφή";
	}

	return $field;

}
add_filter( 'acf/get_valid_field', 'change_input_labels');*/
/*
function change_input_labels_newsletter($field) {
	if($field['name'] == '_post_title')  {	
		if (is_page(71470)) {
			$field['label'] = "Email";			
		}
	}
	return $field;
}
add_filter( 'acf/get_valid_field', 'change_input_labels_newsletter');
*/

function my_save_post_mail($post_id){  //Make first field value as post title
  
  // Get the data from a field
  if ($_POST['isnewsletterform'] === "yes"){
	$new_title = get_field('new_mail', $post_id); 
	
	 // Set the post data
	$new_post = array(
      'ID'           => $post_id,
      'post_title'   => $new_title,
	);
	remove_action('acf/save_post', 'my_save_post_mail', 20);
	wp_update_post( $new_post );
	add_action('acf/save_post', 'my_save_post_mail', 20);
	
  } elseif ($_POST['isrequestform'] === "yes") {
	
	$epilogi_thema = get_field('field_5968876f89b30', $post_id);	
	$epilogi_other = get_field('field_59e4d14a52fbc', $post_id);
	$my_array = [];
	
	if($epilogi_thema){
		array_push($my_array,$epilogi_thema);
	}
	
	if($epilogi_other){
		array_push($my_array,$epilogi_other);
	}

	//$field_key = "field_59ba41dca722d"; //kwdikos aitimatos
	$value = uniqid();		
	
	array_push($my_array,$value);
	
	//update_field( $field_key, $value, $post_id );
	
	$tags = implode(', ', $my_array);
	
	$new_post_req = array(
      'ID'           => $post_id,
      'post_title'   => $tags,
	);
  
  // Remove the hook to avoid infinite loop. Please make sure that it has
  // the same priority (20)
  remove_action('acf/save_post', 'my_save_post_mail', 20);
  
  // Update the post
  wp_update_post( $new_post );
  wp_update_post($new_post_req); 
  
  $field_key = 'field_59b945b6f48ea'; //perigrafi
  $field_key2 = "field_59ba41dca722d"; //kwdikos aitimatos
  
  update_field( $field_key, $tags, $post_id );
  
  update_field( $field_key2, $value, $post_id );
  
  // Add the hook back
  add_action('acf/save_post', 'my_save_post_mail', 20);
  }
  
  
}
add_action('acf/save_post', 'my_save_post_mail', 20);


function my_acf_validate_value( $valid, $value, $field, $input ){ //Check if email exists

	// bail early if value is already invalid
	if( !$valid ) {		
		return $valid;		
	}	
	global $wpdb;

    $custom_post_type = 'newsletters'; // define your custom post type slug here

    // A sql query to return all post titles
    $results = $wpdb->get_results( $wpdb->prepare( "SELECT ID, post_title FROM {$wpdb->posts} WHERE post_type = %s and (post_status = 'publish' OR post_status = 'draft')", $custom_post_type ), ARRAY_A );
	
	$mails_array=[];
	$formId = "eyJpZCI6Im5ld3NsZXR0ZXIiLCJwb3N0X2lkIjoibmV3X3Bvc3QiLCJuZXdfcG9zdCI6eyJwb3N0X3R5cGUiOiJuZXdzbGV0dGVycyIsInBvc3Rfc3RhdHVzIjoiZHJhZnQifSwiZmllbGRfZ3JvdXBzIjpmYWxzZSwiZmllbGRzIjpbImZpZWxkXzU5YjhlYmQwODczNWUiLCJmaWVsZF81OWI3Yjg2NmUyYWMzIiwiZmllbGRfNTliN2I4ODZlMmFjNCIsImZpZWxkXzU5YjdiODk3ZTJhYzUiLCJmaWVsZF81OWI3Yjg5N2UyYWMiLCJmaWVsZF81OWI3YjhhOGUyYWM2Il0sInBvc3RfdGl0bGUiOmZhbHNlLCJwb3N0X2NvbnRlbnQiOmZhbHNlLCJmb3JtIjp0cnVlLCJmb3JtX2F0dHJpYnV0ZXMiOnsiaWQiOiJuZXdzbGV0dGVyIiwiY2xhc3MiOiJhY2YtZm9ybSIsImFjdGlvbiI6IiIsIm1ldGhvZCI6InBvc3QifSwicmV0dXJuIjoiP3VwZGF0ZWQ9dHJ1ZSIsImh0bWxfYmVmb3JlX2ZpZWxkcyI6IiIsImh0bWxfYWZ0ZXJfZmllbGRzIjoiIiwic3VibWl0X3ZhbHVlIjoiXHUwMzk1XHUwM2IzXHUwM2IzXHUwM2MxXHUwM2IxXHUwM2M2XHUwM2FlIiwidXBkYXRlZF9tZXNzYWdlIjoiXHUwM2EzXHUwM2IxXHUwM2MyIFx1MDNiNVx1MDNjNVx1MDNjN1x1MDNiMVx1MDNjMVx1MDNiOVx1MDNjM1x1MDNjNFx1MDNiZlx1MDNjZFx1MDNiY1x1MDNiNSwgXHUwM2FkXHUwM2M3XHUwM2JmXHUwM2M1XHUwM2JjXHUwM2I1IFx1MDNiYlx1MDNhY1x1MDNiMlx1MDNiNVx1MDNiOSBcdTAzYzRcdTAzYmYgXHUwM2IxXHUwM2FmXHUwM2M0XHUwM2I3XHUwM2JjXHUwM2IxIFx1MDNjM1x1MDNiMVx1MDNjMiBcdTAzYmFcdTAzYjFcdTAzYjkgXHUwM2M0XHUwM2JmIFx1MDNiNVx1MDNjMFx1MDNiNVx1MDNiZVx1MDNiNVx1MDNjMVx1MDNiM1x1MDNiMVx1MDNiNlx1MDNjY1x1MDNiY1x1MDNiMVx1MDNjM1x1MDNjNFx1MDNiNS4iLCJsYWJlbF9wbGFjZW1lbnQiOiJ0b3AiLCJpbnN0cnVjdGlvbl9wbGFjZW1lbnQiOiJsYWJlbCIsImZpZWxkX2VsIjoiZGl2IiwidXBsb2FkZXIiOiJ3cCIsImhvbmV5cG90Ijp0cnVlfQ==";
		

	//if($_POST['_acf_form'] == (string)$formId) {
	if ($_POST['isnewsletterform'] === "yes"){
		foreach( $results as $index => $post ) {
		  array_push($mails_array,$post['post_title']) ;
		}
		//var_dump($mails_array);
		
		$value_1 = $_POST['acf']['field_59b8ebd08735e'];
		
		if( in_array($value_1, $mails_array) ) {
			
			$valid = 'Email already exists. Please choose a different email.';
			
		}
	}
	return $valid;
	
}
add_filter('acf/validate_value/name=new_mail', 'my_acf_validate_value', 10, 4);  

function my_acf_save_post( $post_id, $post ) { 	 //apostoli email ston xristi meta tin simplirwsi formas Newsletter
				
		$field_key = "field_59b7cb360a73d";
        $value = $post_id;
		$valid_mail = get_the_title($post_id);
		//$mail = $post->post_title;	
       // remove_action('save_post', 'my_acf_save_post');
       
        update_field( $field_key, $value, $post_id );
		
        // re-hook this function  //. get_the_title($post_id) .
       // add_action('save_post', 'my_acf_save_post');		

		$mailurl = get_site_url().'/?varcode='.$valid_mail;
		$post_url = get_permalink( $post_id );
		$headers = 'From: Δήμος Πλατανιά <myname@example.com>' . "\r\n";
		$subject = 'Εγγραφή στο Newsletter	';
		$message = "Αγαπητέ χρήστη,\n\n Για να εγγραφείτε στην υπηρεσία του Newsletter, παρακαλώ όπως επιβεβαιώστε την πρόθεσή σας μέσω του παρακάτω συνδέσμου.\n\n".$mailurl ."\n\nΣε περίπτωση που δεν αιτηθήκατε εσείς την εγγραφή σας στην υπηρεσία, παρακαλώ όπως αγνοήσετε το παρόν μήνυμα.\n\nΜε εκτίμηση,\nΟ Δήμος Πλατανιά";
		wp_mail( $valid_mail, $subject, $message, $headers );
			

}

add_action('acf/save_post', 'my_acf_save_post', 20);

function verify_code($post_id) {	//redirect gia minima confirm email
	//var_dump($_POST);
	$thecode = ($_GET['varcode']);
	$tes = str_replace(" ","+",$thecode);
	//echo $tes;
	//echo (get_field('field_59b7caf10a73c',$theid->ID));
	//$theid = $_GET['verification_code'];
	
	$theid = (get_page_by_title( $tes, $output, 'newsletters' ));
	//var_dump($theid);
	$som = (get_field('field_59b7caf10a73c',$theid->ID));
	//print_r($som);
	
	//echo $thecode;
		//$url1 = 'http://beta.platanias.gr/mail-allready-confirmed/';
		//$url2 = 'http://beta.platanias.gr/confirm-email/';
		$vali = "novalid";
		
	if ($tes) {
		
		$url1 = 'http://beta.platanias.gr/mail-allready-confirmed/';
		$url2 = 'http://beta.platanias.gr/confirm-email/';
		if(get_field('field_59b7caf10a73c',$theid->ID) == (string)$vali){
			//echo 'OOOOO';
			$field_key = "field_59b7caf10a73c";
			$value = 'valid';
			//$value = rand(1,999999);
		
			//remove_action('save_post', 'my_acf_save_post');
       
			update_field( $field_key, $value, $theid->ID );
			// re-hook this function
			//add_action('save_post', 'my_acf_save_post');
			wp_redirect( $url1 );
			
		}else{
			wp_redirect( $url2 );
			
		}
		
	}
}
add_action( 'init', 'verify_code' );

function request_number_issue( $post_id, $post ) {   //dimiourgia uniq code gia to aitima kai apostolh email
    
	if ($_POST['isrequestform'] === "yes"){
		//$field_key = "field_59ba41dca722d";        
		//$value = uniqid();		
		//$valid_mail = get_the_title($post_id);      
        //update_field( $field_key, $value, $post_id );
		
		$value = get_field('field_59ba41dca722d',$post_id);
		
		$mail = get_field('field_5968867589b2c',$post_id);
		
		$mailurl = get_site_url().'/?issue_number='.$value;
		$visitlink = get_site_url().'/wp-admin/edit.php?post_type=request';
		$post_url = get_permalink( $post_id );
		$headers = 'From: Δήμος Πλατανιά <info@platanias.gr>' . "\r\n";
		$subject = 'Δημιουργία Αιτήματος';
		$message = "Αγαπητέ χρήστη,\n\n Ο κωδικός του αιτήματός σας είναι:\n\n".$value ."\n\nΕπισκεφθείτε την παρακάτω διεύθυνση για να δείτε την κατάσταση του αιτήματος.\n".$mailurl."\n\nΜε εκτίμηση,\nΟμάδα Ηλεκτρονικής Διακυβέρνησης Δήμου Πλατανιά";
		wp_mail( $mail, $subject, $message, $headers );
		
		$blogusers = get_users( 'role=request_role' );
		// Array of WP_User objects.
		foreach ( $blogusers as $user ) {
			
			$headers = 'From: Δήμος Πλατανιά <info@platanias.gr>' . "\r\n";
			$subject = 'Δημιουργία Αιτήματος';
			$message = "Αγαπητέ ". esc_html( $user->display_name ).",\n\n Προστέθηκε ένα e-αίτημα:\n\n\nΕπισκεφθείτε την παρακάτω διεύθυνση για να το προτεραιοποιήσεται.\n".$visitlink."\n\nΜε εκτίμηση,\nΟμάδα Ηλεκτρονικής Διακυβέρνησης Δήμου Πλατανιά";
			wp_mail( esc_html( $user->user_email ), $subject, $message, $headers );
		}
		
	}	
	if ($_POST['issubmitform'] === "yes"){
        wp_redirect( 'http://beta.platanias.gr/kodikos-etimatos/?thankid=' . $post_id ); exit;
    }
      
}

add_action('acf/save_post', 'request_number_issue', 20);

add_action( 'save_post', 'my_save_custompost_function', 10, 3 );

function my_save_custompost_function( $post_ID, $post, $update ) {  //Send mail to the user when request is updated.

$katastasi = get_the_terms($post_ID, 'katastasi' );
$armodia = get_the_terms($post_ID, 'armodia_ipiresia' );


 if(get_post_type($post_ID) == 'request' && $katastasi && $armodia ){
	$mail = get_field('field_5968867589b2c',$post_ID); 
	$url = get_field('field_59ba41dca722d',$post_ID);
	$link =	get_site_url().'/anazitisi-etiseon/?issue_number='.$url;
	
	$headers = 'From: Δήμος Πλατανιά <info@platanias.gr>' . "\r\n";
	$subject = 'Ενημέρωση Αιτήματος';
	$message = "Αγαπητέ χρήστη,\n\n το αίτημα σας προτεραιοποιήθηκε.Επισκεφθείτε την παρακάτω διεύθυνση για να δείτε την κατάσταση του αιτήματος.\n".$link."\n\nΜε εκτίμηση,\nΟμάδα Ηλεκτρονικής Διακυβέρνησης Δήμου Πλατανιά";
	wp_mail( $mail, $subject, $message, $headers );
 }
  
}

function wpdocs_five_posts_on_homepage( $query ) {
	if ( $query->is_search()  ) {
		$query->set( 'posts_per_page', -1 );
	}
}
add_action( 'pre_get_posts', 'wpdocs_five_posts_on_homepage' );


// numbered pagination
function pagination($pages = '', $range = 4)
{  
     $showitems = ($range * 2)+1;  
	//echo $showitems;
	
     global $paged;
	// echo $paged;
	// echo $pages;
     if(empty($paged)) $paged = 1;
 
     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   
 
     if(1 != $pages)
     {
         echo "<span>Σελιδά ".$paged." από ".$pages."</span>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."'>&laquo; Αρχή</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."'>&lsaquo; Προηγούμενο</a>";
 
         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<span class=\"current\">".$i."</span>":"<a href='".get_pagenum_link($i)."' class=\"inactive\">".$i."</a>";
             }
         }
 
         if ($paged < $pages && $showitems < $pages) echo "<a href=\"".get_pagenum_link($paged + 1)."\">Επόμενο &rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."'>Τέλος &raquo;</a>";
         echo "";
     }
     $wp_query = NULL;
	 $wp_query = $temp_query;
}

/*function title_filter($where, &$wp_query){
    global $wpdb;
	
    if($search_title = $wp_query->get( 'search_title' )){
        $search_title = $wpdb->esc_like($search_title);
        $search_title = ' \'%' . $search_title . '%\'';
        $where .= ' AND ' . $wpdb->posts . '.post_title LIKE '.$search_title;
    }

    return $where;
}
add_filter( 'posts_where', 'title_filter', 10, 2 );*/

/*
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'project'; // change to your post type
	$taxonomy  = 'pj-categs'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => false,
		));
	};
}*/

/*Filter by taxonomy name in dashboard*/
function pippin_add_taxonomy_filters() {
	global $typenow;
 
	// an array of all the taxonomyies you want to display. Use the taxonomy name or slug
	$taxonomies = array('taxonomy' => 'pj-categs');
 
	// must set this to the post type you want the filter(s) displayed on
	if( $typenow == 'project' ){
 
		foreach ($taxonomies as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			$tax_name = $tax_obj->labels->name;
			$terms = get_terms($tax_slug);
			if(count($terms) > 0) {
				echo "<select name='$tax_slug' id='$tax_slug' class='postform'>";
				echo "<option value=''>Show All $tax_name</option>";
				foreach ($terms as $term) { 
					echo '<option value='. $term->slug, $_GET[$tax_slug] == $term->slug ? ' selected="selected"' : '','>' . $term->name .' (' . $term->count .')</option>'; 
				}
				echo "</select>";
			}
		}
	}
}
add_action( 'restrict_manage_posts', 'pippin_add_taxonomy_filters' );


function my_acf_init() {
	
	acf_update_setting('google_api_key', 'AIzaSyAudGd4zIovIFmeKntsM7B0O2lFrKMY9GA');
}

add_action('acf/init', 'my_acf_init');

/*
function some_random_code(){
?>	

	<div class="acf-map row" style="margin-bottom:45px; position:relative;">			  
		<div id="map" class="acf-map" style="height:500px;">	

		</div>	
		<div id="buttons"  style="margin-bottom:50px; border:1px solid black; z-index:999; position:absolute; top:135px;">
		<label style="margin:2px 2px 0px 2px;"><input id ="second" type="checkbox" name="filter" value="Μουσείο" class='chk-btn' onchange="selectAllChecked();">Μουσείο</label>
		<br>
		<label style="margin:2px 2px 0px 2px;"><input id ="third" type="checkbox" name="filter" value="Πάρκο" class='chk-btn' onchange="selectAllChecked();">Πάρκο</label>

		</div>		
	</div>       
<?php
	$repeater = get_field('google_map');
	?>

 <script>

	var gmarkers1 = [];
	
    function initMap()	{
      var options = {
        zoom:8,
        center:{lat:42.3601,lng:-71.0589}
      }


    
	var locations = [

	<?php foreach($repeater as $locs): ?>
      ["<?php echo $locs["location"]["address"]; ?>", "<?php echo $locs["location"]["lat"]; ?>", "<?php echo $locs["location"]["lng"]; ?>", "<?php echo $locs["category"]; ?>", "<?php echo $locs["image"]; ?>","<?php echo $locs["title"]; ?>", "<?php echo $locs["info"]; ?>",],
     
    <?php endforeach; ?>
    ];
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 10,
      center: new google.maps.LatLng(35.516702, 23.908920),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
	
    for (i = 0; i < locations.length; i++) { 
	
		if (locations[i][3] == "Μουσείο"){
			var category = locations[i][3];
			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			icon:'http://cpdev.crowdpolicy.com/platanias/wp-content/uploads/2017/07/rsz_1museum-icon.png',
			category : category,
			map: map
		  });
		}else{
			var category = locations[i][3];
			marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			icon:'https://developers.google.com/maps/documentation/javascript/examples/full/images/beachflag.png',
			category : category,
			map: map
		  });
		  }
		
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
			if( (locations[i][4]) != ""){

          
          infowindow.setContent(locations[i][5]+'<br/><img  height="42" width="42" src="'+locations[i][4]+'"></br>'+locations[i][6]);
          
			}else {
				infowindow.setContent(locations[i][5]);
			}
          infowindow.open(map, marker);
        }
      })(marker, i));
	 gmarkers1.push(marker);
  
   }
		
		 selectAllChecked = function() {
		  var checkedPlace = [] 
		  var allCheckedElem = document.getElementsByName('filter');
		  for (var i = 0; i < allCheckedElem.length; i++) {
			if (allCheckedElem[i].checked == true) {
			  checkedPlace.push(allCheckedElem[i].value)
			}
		  }

		  filterChecker(checkedPlace) 

			 function filterChecker(category) {
			  //console.log(tip);
			  for (i = 0; i < locations.length; i++) {
				marker = gmarkers1[i];
				//console.log(marker);
				if (in_array(marker.category) != -1) {
				  marker.setVisible(true);
				} else {
				  marker.setVisible(false);
				}
			  }
			}
			
			function in_array(needle){
			 
			  for (var i = 0; i < checkedPlace.length; i++) {
				if (needle == checkedPlace[i] || needle == 'all') 	
					return i;
				console.log(needle);
				}
			  return -1;
			}
		
		}
}	

  </script>
  
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8HqS0Ers_lU2INlPls71aEMf1zAHwdgk&callback=initMap">
    </script>



<?php } 
	add_shortcode( 'dfgdfgdfgdfgdf', 'some_random_code' );

*/

function some_random_code(){
$field_key = "field_5982e8c4c016b";
$field = get_field_object($field_key);

	$themap = '<div class="acf-map row" style="position:relative;">			  
		<div id="map" class="acf-map" style="height:500px;">	

		</div>';
		
	if( $field ){ 
	
		$themap .= '<div id="buttons"  style="margin-left: 15px; background-color: rgba(255, 255, 255, 0.5); padding: 20px; margin-bottom:50px; border:1px solid white; z-index:999; position:absolute; top:135px; left: 15px;"><input checked id="allcheck" type="checkbox" " class="chk-btn" ><label style="margin:2px 2px 0px 2px; font-weight: 400; margin-left: 10px; font-size: 16px;">Επιλογή / Αποεπιλογή όλων</label><br>';
		
		foreach( $field['choices'] as $k => $v ) {
			
			if ($k == 'Αρχαιολογικός χώρος'){
				$imgsrc = wp_get_attachment_url( 72816 );				
			}elseif($k == 'Μουσείο'){
				$imgsrc = wp_get_attachment_url( 72820 );
			}elseif($k == 'Ναοί'){
				$imgsrc = wp_get_attachment_url( 72818 );
			}elseif($k == 'Παραλία'){
				$imgsrc = wp_get_attachment_url( 72817 );
			}elseif($k == 'Ποδηλατική διαδρομή'){
				$imgsrc = wp_get_attachment_url( 72819 );
			}elseif($k == 'Σπήλαιο'){
				$imgsrc = wp_get_attachment_url( 72814 );
			}elseif($k == 'Φεστιβάλ/ Εκδήλωση'){
				$imgsrc = wp_get_attachment_url( 72821 );
			}elseif($k == 'Φύση/ Μονοπάτι'){
				$imgsrc = wp_get_attachment_url( 72813 );
			}elseif($k == 'Χωριό τουριστικού ενδιαφέροντος'){
				$imgsrc = wp_get_attachment_url( 72815 );
			}
			
			$themap .= '<input checked id="second" type="checkbox" name="filter" value="'.$k.'" class="chk-btn" onchange="selectAllChecked();"><label style="margin:2px 2px 0px 2px; font-weight: 400; margin-left: 10px; font-size: 16px;">'.$k.'</label><span class="legend-icons"><img height="25px" width="25px" src="'.$imgsrc.'" ></span> <br/>';
		}
		$themap .= '</div> </div>';
	}
     
	add_action( "wp_footer", "printJSMap", 10, 4);

    return $themap;
 } 
	
add_shortcode( 'platanias_map', 'some_random_code' );

function printJSMap(){
	
	$repeater = get_field('google_map', 71075);
	
	?>
	
	<script type="text/javascript">
	
	    function createmap(){
	    
	    var locations = [ <?php $des_array = [];
		
				foreach($repeater as $locs): 
				
					array_push($des_array, $locs['keimeno']);
					$locs['keimeno'] = str_replace("\n", "", $locs['keimeno']);
					$locs['keimeno'] = str_replace("\r", "", $locs['keimeno']);
					$locs['keimeno'] = str_replace("\"", "“", $locs['keimeno']);
					$locs['keimeno']=trim($locs['keimeno']);

					$locs['title'] = str_replace("\n", "", $locs['title']);
					$locs['title'] = str_replace("\r", "", $locs['title']);
					$locs['title'] = str_replace("\"", "“", $locs['title']);
					$locs['title']=trim($locs['title']);
					
					echo '["'.
					$locs["location"]["address"].'", "'.
					$locs["location"]["lat"].'", "'.
					$locs["location"]["lng"].'", "'.
					$locs["category"].'", "'.
					$locs["image"].'","'.
					$locs["title"].'","'.
					$locs["pin"].'","'.
					htmlspecialchars($locs["keimeno"]).'"],';
				
				endforeach; ?> ];
	
	        var map = new google.maps.Map(document.getElementById("map"), {
	            zoom: 10,
	            center: new google.maps.LatLng(35.516702, 23.908920),
	            mapTypeId: google.maps.MapTypeId.ROADMAP
	        });
	
	        var infowindow = new google.maps.InfoWindow();
	
	        var marker, i;
	        var gmarkers1 = [];
	
	        for (i = 0; i < locations.length; i++) {
	
	            var category = locations[i][3];
	            var photopin = locations[i][6];
	            var pin = {
	                url: photopin,
	                scaledSize: new google.maps.Size(30, 30),
	            };
	            if (locations[i][6]) {
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	                    icon: pin,
	                    category: category,
	                    map: map
	                });
	            } else {
	                marker = new google.maps.Marker({
	                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
	                    category: category,
	                    map: map
	                });
	            }
	
	
	            var infowindow = new google.maps.InfoWindow({
	                maxWidth: 500
	            })
	
	            google.maps.event.addListener(marker, "click", (function(marker, i) {
	                return function() {
	
	                    var html = decodeHtml(locations[i][7]);
	                    infowindow.setContent(locations[i][5] + "<br/><img height=\"50px\" width=\"250px\" src=\"" + locations[i][4] + "\"></br>" + html);
	
	                    infowindow.open(map, marker);
	                }
	            })(marker, i));
	            gmarkers1.push(marker);
	
	        }
	
	        function decodeHtml(html) {
	            var txt = document.createElement("textarea");
	            txt.innerHTML = html;
	            return txt.value;
	        }
	
			jQuery('#allcheck').live('change', function(){
					if (jQuery(this).is(':checked')) {
						selectAll(true);
						jQuery("input[name='filter']").each( function () {
							jQuery(this).prop( "checked", true );
						});
					}else{
						selectAll(false);
						jQuery("input[name='filter']").each( function () {
							jQuery(this).prop( "checked", false );
						});
					}
				});
	
			selectAll = function(trigger) {
				
				for (i = 0; i < gmarkers1.length; i++) {
					marker = gmarkers1[i];		                    
					marker.setVisible(trigger);	                    
				}					
			}
	
			/*selectAll = function() {
				
				for (i = 0; i < gmarkers1.length; i++) {
	                    marker = gmarkers1[i];		                    
	                    marker.setVisible(true);	                    
				}					
			}*/
			
	        selectAllChecked = function() {
	            var checkedPlace = []
	            var allCheckedElem = document.getElementsByName("filter");
				
	            for (var i = 0; i < allCheckedElem.length; i++) {
	                if (allCheckedElem[i].checked == true) {
	                    checkedPlace.push(allCheckedElem[i].value)
	                }
	            }
	
	            filterChecker(checkedPlace)
	
	
	            function filterChecker(category) {
	                for (i = 0; i < locations.length; i++) {
	                    marker = gmarkers1[i];
	
	                    if (in_array(marker.category) != -1) {
	                        marker.setVisible(true);
	                    } else {
	                        marker.setVisible(false);
	                    }
	                }
	            }
	
	            function in_array(needle) {
	
	                for (var i = 0; i < checkedPlace.length; i++) {
	                    if (needle == checkedPlace[i] || needle == "all")
	                        return i;
	                }
	                return -1;
	            }
	        }
				
	    }
	    
	</script>
	
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC8HqS0Ers_lU2INlPls71aEMf1zAHwdgk&callback=createmap"></script>
	
	<?php
	
}

function print_button_shortcode( $atts ){
	return '<a class="print-link" href="javascript:window.print()">Print This Page</a>';
}
add_shortcode( 'print_button', 'print_button_shortcode' );


//Create new Role
add_role('request_role', __(
    'Διαχειριστής Αιτημάτων'),
    array(
        'read'              => true, // Allows a user to read
        'create_posts'      => true, // Allows user to create new posts
        'edit_posts'        => true, // Allows user to edit their own posts
        'edit_others_posts' => true, // Allows user to edit others posts too
        'publish_posts'     => true, // Allows the user to publish posts
        'manage_categories' => true, // Allows user to manage post categories
        )
);
//Remove menus from request role
function remove_menus(){
  if( current_user_can('request_role')) {
		remove_menu_page( 'admin.php?page=wpcf7'); 
		remove_menu_page( 'edit.php' ); //Remove posts
		remove_menu_page( 'edit.php?post_type=apofasis' ); //Remove apofaseis
		remove_menu_page( 'edit-comments.php' );           //Remove comments
		remove_menu_page( 'edit.php?post_type=project' ); //Remove ipiresies
		remove_menu_page( 'edit.php?post_type=profile' ); //Remove profiles
		remove_menu_page( 'edit.php?post_type=meletes' ); //Remove meletes
		remove_menu_page( 'edit.php?post_type=newsletters' ); //Remove newsletters
		
		
		
		remove_menu_page( 'tools.php' );   	 //Remove tools 
	
	echo '<style>#toplevel_page_wpcf7 {display:none;} </style>'; 
 }
  
}
add_action( 'admin_menu', 'remove_menus' );

//Removes wp logo from toolbar
function remove_wp_logo( $wp_admin_bar ) {
	$wp_admin_bar->remove_node( 'wp-logo' );
	
}
add_action( 'admin_bar_menu', 'remove_wp_logo', 999 );

//change the url from login page
function the_url( $url ) {
    return get_bloginfo( 'url' );
}
add_filter( 'login_headerurl', 'the_url' );

//change the logo from login page
function my_login_logo() { ?>
    <style type="text/css">
        #login h1 a, .login h1 a {
		background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/images/platanias-logo-02.png);
		height:70px;
		width:320px;
		background-size: 315px 100px;
		background-repeat: no-repeat;
		padding-bottom: 30px;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'my_login_logo' );

add_filter( 'get_user_option_admin_color', 'update_user_option_admin_color', 5 );

function update_user_option_admin_color( $color_scheme ) {
    $color_scheme = 'midnight';

    return $color_scheme;
}

//Display none from dashboar contact and form builder
function my_custom_fonts() {
  echo '<style>
    #toplevel_page_wpcf7 {display:none;}
	#toplevel_page_form-builder {display:none;}
  </style>';
}
add_action('admin_head', 'my_custom_fonts');

function code_for_tables(){

 $rows = get_field('stixia');

	if($rows) {
		foreach($rows as $row) {
			$thetable .= '<div class="row">
							 <div class="col-md-6">';
			if ($row['name']):
				$thetable .='<p><b>Ονοματεπώνυμο:</b> '.$row['name'].'</p>';
			endif;
			if ($row['idiotita']):
				$thetable .='<p><b>Ιδιότητα:</b> '.$row['idiotita'].'</p>';
			endif;
			if ($row['armodiotites']):
				$thetable .='<p><b>Αρμοδιότητες:</b> '.$row['armodiotites'].'</p>';
			endif;			
			
				$thetable .='</div>
								<div class="col-md-6">';
			if ($row['phone_number']):
				$thetable .='<p><i class="fa fa-phone fa-style" aria-hidden="true"></i><b>Τηλέφωνο:</b> '.$row['phone_number'].'</p>';
			endif;
			if ($row['fax_number']):
				$thetable .='<p><i class="fa fa-fax fa-style" aria-hidden="true"></i><b>Fax:</b> '.$row['fax_number'].'</p>';
			endif;
			if ($row['email']):
				$thetable .='<p><i class="fa fa-envelope fa-style" aria-hidden="true"></i><b>Email:</b> '.$row['email'].'</p>';
			endif;						
									
				$thetable .= '</div>
						   </div>
						<hr>';
		}
	}
    return $thetable;
 } 
	
add_shortcode( 'table_row', 'code_for_tables' );

function code_for_table_epikoinwnias(){

 $rows = get_field('stoixeia_epikoinwnias');

	if($rows) {
		foreach($rows as $row) {
			$thetable .= '<div class="row">
							 <div class="col-md-6">';
			if ($row['onoma_ipiresias']):
				$thetable .='<p><b>Όνομα Υπηρεσίας:</b><br>'.$row['onoma_ipiresias'].'</p>';
			endif;
						
				$thetable .='</div>
								<div class="col-md-6">';
			if ($row['address']):
				$thetable .='<p><i class="fa fa-map-marker fa-style" aria-hidden="true"></i><b>Διεύθυνση:</b> '.$row['address'].'</p>';
			endif;
			if ($row['tel_number']):
				$thetable .='<p><i class="fa fa-phone fa-style" aria-hidden="true"></i><b>Τηλέφωνο:</b> '.$row['tel_number'].'</p>';
			endif;
			if ($row['fax']):
				$thetable .='<p><i class="fa fa-fax fa-style" aria-hidden="true"></i><b>Fax:</b> '.$row['fax'].'</p>';
			endif;
			if ($row['email']):
				$thetable .='<p><i class="fa fa-envelope fa-style" aria-hidden="true"></i><b>Email:</b> '.$row['email'].'</p>';
			endif;						
									
				$thetable .= '</div>
						   </div>
						<hr>';
		}
	}
    return $thetable;
 } 
	
add_shortcode( 'stixia_epikoinwnias', 'code_for_table_epikoinwnias' );
