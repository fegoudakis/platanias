<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Comments Template
 * Created by CMSMasters
 * 
 */


if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME'])) {
    die(esc_html__('Please do not load this page directly. Thanks!', 'dream-city'));
}


if (post_password_required()) { 
	echo '<p class="nocomments">' . esc_html__('This post is password protected. Enter the password to view comments.', 'dream-city') . '</p>';
	
	
    return;
}


if (comments_open()) {
	if (have_comments()) {
		echo '<aside id="comments" class="post_comments">' . "\n" . 
			'<h4 class="post_comments_title">';
		
		
		comments_number(esc_attr__('No Comments', 'dream-city'), esc_attr__('Comment', 'dream-city') . ' (1)', esc_attr__('Comments', 'dream-city') . ' (%)');
		
		
		echo '</h4>' . "\n";
		
		
		if (get_previous_comments_link() || get_next_comments_link()) {
			echo '<aside class="comments_nav">';
				
				if (get_previous_comments_link()) {
					echo '<span class="comments_nav_prev cmsmasters_theme_icon_comments_nav_prev">';
						
						previous_comments_link(esc_attr__('Older Comments', 'dream-city'));
						
					echo '</span>';
				}
				
				
				if (get_next_comments_link()) {
					echo '<span class="comments_nav_next cmsmasters_theme_icon_comments_nav_next">';
						
						next_comments_link(esc_attr__('Newer Comments', 'dream-city'));
						
					echo '</span>';
				}
				
			echo '</aside>';
		}
		
		
		echo '<ol class="commentlist">' . "\n";
		
		
		wp_list_comments(array( 
			'type' => 'comment', 
			'callback' => 'dream_city_mytheme_comment' 
		));
		
		
		echo '</ol>' . "\n";
		
		
		if (get_previous_comments_link() || get_next_comments_link()) {
			echo '<aside class="comments_nav">';
				
				if (get_previous_comments_link()) {
					echo '<span class="comments_nav_prev cmsmasters_theme_icon_comments_nav_prev">';
						
						previous_comments_link(esc_attr__('Older Comments', 'dream-city'));
						
					echo '</span>';
				}
				
				
				if (get_next_comments_link()) {
					echo '<span class="comments_nav_next cmsmasters_theme_icon_comments_nav_next">';
						
						next_comments_link(esc_attr__('Newer Comments', 'dream-city'));
						
					echo '</span>';
				}
				
			echo '</aside>';
		}
		
		
		echo '</aside>';
	}
	
	
	$form_fields =  array( 
		/*'author' => '<p class="comment-form-author">' . "\n" .
			'<input type="text" id="author" name="author" value="' . esc_attr($commenter['comment_author']) . '" size="35"' . ((isset($aria_req)) ? $aria_req : '') . ' placeholder="' . esc_html__('Your name', 'dream-city') . (($req) ? ' *' : '') . '" />' . "\n" .
		'</p>' . "\n", 
		'email' => '<p class="comment-form-email">' . "\n" . 
			'<input type="text" id="email" name="email" value="' . esc_attr($commenter['comment_author_email']) . '" size="35"' . ((isset($aria_req)) ? $aria_req : '') . ' placeholder="' . esc_html__('Your email', 'dream-city') . (($req) ? ' *' : '') . '" />' . "\n" . 
		'</p>' . "\n" */

		'author' => '<p class="comment-form-author">' . "\n" .
		            '<input type="text" id="author" name="author" value="' . esc_attr($commenter['comment_author']) . '" size="35"' . ((isset($aria_req)) ? $aria_req : '') . ' placeholder="' . esc_html__('Όνομα', 'dream-city') . (($req) ? ' *' : '') . '" />' . "\n" .
		            '</p>' . "\n",
		'email' => '<p class="comment-form-email">' . "\n" .
		           '<input type="text" id="email" name="email" value="' . esc_attr($commenter['comment_author_email']) . '" size="35"' . ((isset($aria_req)) ? $aria_req : '') . ' placeholder="' . esc_html__('Email', 'dream-city') . (($req) ? ' *' : '') . '" />' . "\n" .
		           '</p>' . "\n"
	);
	
	
	echo "\n\n";
	
	
	comment_form(array( 
		'fields' => 			apply_filters('comment_form_default_fields', $form_fields), 
		'comment_field' => 		'<p class="comment-form-comment">' . 
									/*'<textarea name="comment" id="comment" cols="67" rows="2" placeholder="' . esc_html__('Comment', 'dream-city') . '"></textarea>' . */
									'<textarea name="comment" id="comment" cols="67" rows="2" placeholder="' . esc_html__('Περιεχόμενο', 'dream-city') . '"></textarea>' .
								'</p>',
		'must_log_in' => 		'<p class="must-log-in">' . 
									esc_html__('You must be', 'dream-city') . 
									' <a href="' . esc_url(wp_login_url(apply_filters('the_permalink', get_permalink()))) . '">' 
										. esc_html__('logged in', 'dream-city') . 
									'</a> ' 
									. esc_html__('to post a comment', 'dream-city') . 
								'.</p>' . "\n", 
		'logged_in_as' => 		'<p class="logged-in-as">' . 
									esc_html__('Logged in as', 'dream-city') . 
									' <a href="' . esc_url(admin_url('profile.php')) . '">' . 
										$user_identity . 
									'</a>. ' . 
									'<a class="all" href="' . esc_url(wp_logout_url(apply_filters('the_permalink', get_permalink()))) . '" title="' . esc_attr__('Log out of this account', 'dream-city') . '">' . 
										esc_html__('Log out?', 'dream-city') . 
									'</a>' . 
								'</p>' . "\n" .do_shortcode('[ratings]'), 
		/*'comment_notes_before' => 	'<p class="comment-notes">' .
										esc_html__('Your email address will not be published.', 'dream-city') . 
									'</p>' . "\n",*/
		'comment_notes_before' => 	'<p class="comment-notes">' .
		                             esc_html__('Το email σας δεν θα δημοσιευθεί.', 'dream-city') .
		                             '</p>' .  do_shortcode('[ratings]'),

		'comment_notes_after' => 	'', 
		'id_form' => 				'commentform', 
		'id_submit' => 				'submit', 
		/*'title_reply' => 			esc_attr__('Leave a Reply', 'dream-city'),
		'title_reply_to' => 		esc_attr__('Leave your comment to', 'dream-city'),
		'cancel_reply_link' => 		esc_attr__('Cancel Reply', 'dream-city'), 
		'label_submit' => 			esc_attr__('Add Comment', 'dream-city') */
		'title_reply' => 			esc_attr__('Σχολίασε την υπηρεσία - Πρότεινε ιδέες βελτίωσης', 'dream-city'),
		'title_reply_to' => 		esc_attr__('Leave your comment to', 'dream-city'),
		'cancel_reply_link' => 		esc_attr__('Cancel Reply', 'dream-city'),
		'label_submit' => 			esc_attr__('Yποβολή σχολίου', 'dream-city')
	));
}

