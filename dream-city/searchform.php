<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Search Form Template
 * Created by CMSMasters
 * 
 */
?>

<div class="search_bar_wrap">
    <form method="get" action="<?php echo esc_attr( site_url() ); ?>/search/" >
		<p class="search_field">
			<input name="keyword" placeholder="<?php esc_attr_e('εισάγετε λέξεις κλειδιά...', 'dream-city'); ?>" value="<?php echo $_GET['keyword']; ?>" type="search" />
		</p>
		<p class="search_button">
			<button type="submit" class="cmsmasters_theme_icon_search"></button>
		</p>
	</form>
</div>

