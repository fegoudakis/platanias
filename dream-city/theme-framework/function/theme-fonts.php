<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


function dream_city_theme_fonts() {
	$cmsmasters_option = dream_city_get_global_options();
	
	
	$custom_css = "/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Theme Fonts Rules
 * Created by CMSMasters
 * 
 */


/***************** Start Theme Font Styles ******************/

	/* Start Content Font */
	body,
	.mid_nav a .nav_subtitle,
	.bot_nav a .nav_subtitle,
	.cmsmasters_twitter_wrap .published, 
	#page .cmsmasters_comment_item .cmsmasters_comment_item_cont_info .comment-reply-link,
	#page .cmsmasters_comment_item .cmsmasters_comment_item_cont_info .cmsmasters_comment_item_date,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li p,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li .cmsmasters_comments_post_title small {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_content_font_google_font']) . $cmsmasters_option['dream-city' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_content_font_font_style'] . ";
	}
	
	.cmsmasters_footer_small .footer_copyright {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 1) . "px;
	}
	
	.about_author .about_author_cont, 
	.about_author .about_author_cont a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] + 1) . "px;
	}
	
	.widget_custom_twitter_entries .tweet_time, 
	#page .cmsmasters_comment_item .cmsmasters_comment_item_cont_info .cmsmasters_comment_item_date, 
	#page .cmsmasters_comment_item .cmsmasters_comment_item_cont_info .comment-reply-link, 
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info .cmsmasters_comment_item_date, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li p {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 2) . "px;
	}
	
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li .cmsmasters_comments_post_title small {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 3) . "px;
	}
	
	.mid_nav a .nav_subtitle,
	.bot_nav a .nav_subtitle {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_content_font_line_height'] - 4) . "px;
	}
	
	.mid_nav a .nav_subtitle,
	.bot_nav a .nav_subtitle {
		text-transform:none;
	}
	
	.cmsmasters_icon_list_items li:before {
		line-height:" . $cmsmasters_option['dream-city' . '_content_font_line_height'] . "px;
	}
	/* Finish Content Font */


	/* Start Link Font */
	a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_link_font_google_font']) . $cmsmasters_option['dream-city' . '_link_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_link_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_link_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_link_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_link_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_link_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_link_font_text_decoration'] . ";
	}
	
	.cmsmasters_wrap_pagination ul li .page-numbers,
	.cmsmasters_comment_item .cmsmasters_comment_item_avatar a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_link_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_link_font_line_height'] - 2) . "px;
	}
	
	a:hover {
		text-decoration:" . $cmsmasters_option['dream-city' . '_link_hover_decoration'] . ";
	}
	/* Finish Link Font */


	/* Start Navigation Title Font */
	.mid_nav a .nav_tag, 
	.bot_nav a .nav_tag, 
	.navigation > li > a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_nav_title_font_google_font']) . $cmsmasters_option['dream-city' . '_nav_title_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_nav_title_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_nav_title_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_nav_title_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_nav_title_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_nav_title_font_text_transform'] . ";
	}
	
	.mid_nav.mid_nav div.menu-item-mega-container > ul > li > a,
	.bot_nav div.menu-item-mega-container > ul > li > a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_nav_title_font_google_font']) . $cmsmasters_option['dream-city' . '_nav_title_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_nav_title_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_nav_title_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_nav_title_font_font_weight'] . ";
	}
	
	.header_top .meta_wrap * {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_nav_title_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_nav_title_font_line_height'] - 4) . "px;
	}
	
	.header_top .meta_wrap > *:before {
		font-size:" . $cmsmasters_option['dream-city' . '_nav_title_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_nav_title_font_line_height'] . "px;
	}
	
	@media only screen and (max-width: 1024px) {
		#header .navigation .cmsmasters_resp_nav_toggle {
			font-size:" . ((int) $cmsmasters_option['dream-city' . '_nav_title_font_font_size'] + 6) . "px;
		}
	}
	
	.mid_nav div.menu-item-mega-container > ul > li > a,
	.bot_nav div.menu-item-mega-container > ul > li > a {
		text-transform:" . $cmsmasters_option['dream-city' . '_nav_title_font_text_transform'] . ";
	}
	/* Finish Navigation Title Font */


	/* Start Navigation Dropdown Font */
	.navigation ul li a,
	.top_line_nav li > a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_nav_dropdown_font_google_font']) . $cmsmasters_option['dream-city' . '_nav_dropdown_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_nav_dropdown_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_nav_dropdown_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_nav_dropdown_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_nav_dropdown_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_nav_dropdown_font_text_transform'] . ";
	}
	
	.top_line_nav li > a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_nav_dropdown_font_font_size'] - 1) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_nav_dropdown_font_line_height'] - 1) . "px;
		font-weight:400;
	}
	/* Finish Navigation Dropdown Font */


	/* Start H1 Font */
	h1,
	h1 a,
	.logo .title,
	.cmsmasters_pricing_table .cmsmasters_price_wrap,
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter.counter_has_icon_or_image .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap, 
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h1_font_google_font']) . $cmsmasters_option['dream-city' . '_h1_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h1_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h1_font_text_decoration'] . ";
	}
	
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 24) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] + 16) . "px;
		font-weight:300;
	}
	
	.cmsmasters_dropcap {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h1_font_google_font']) . $cmsmasters_option['dream-city' . '_h1_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['dream-city' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h1_font_text_decoration'] . ";
	}
	
	.cmsmasters_icon_list_items.cmsmasters_icon_list_icon_type_number .cmsmasters_icon_list_item .cmsmasters_icon_list_icon:before,
	.cmsmasters_icon_box.box_icon_type_number:before,
	.cmsmasters_icon_box.cmsmasters_icon_heading_left.box_icon_type_number .icon_box_heading:before {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h1_font_google_font']) . $cmsmasters_option['dream-city' . '_h1_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['dream-city' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h1_font_font_style'] . ";
	}
	
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter.counter_has_icon_or_image .cmsmasters_counter_inner .cmsmasters_counter_counter_wrap,
	.cmsmasters_open_profile .cmsmasters_profile_header .cmsmasters_profile_title {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] - 10) . "px;
	}
	
	.cmsmasters_dropcap.type1 {
		font-size:44px; /* static */
	}
	
	.cmsmasters_dropcap.type2 {
		font-size:22px; /* static */
	}
	
	.headline_outer .headline_inner .headline_icon:before {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 5) . "px;
	}
	
	.headline_outer .headline_inner.align_center .headline_icon:before {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] + 15) . "px;
	}
	
	.headline_outer .headline_inner.align_left .headline_icon {
		padding-left:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 5) . "px;
	}
	
	.headline_outer .headline_inner.align_right .headline_icon {
		padding-right:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 5) . "px;
	}
	
	.headline_outer .headline_inner.align_center .headline_icon {
		padding-top:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] + 15) . "px;
	}
	
	.widget_tag_cloud a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h1_font_google_font']) . $cmsmasters_option['dream-city' . '_h1_font_system_font'] . ";
	}
	/* Finish H1 Font */


	/* Start H2 Font */
	h2,
	h2 a,
	.cmsmasters_profile_vertical .cmsmasters_profile_header_wrap h3 a, 
	.cmsmasters_project_grid .cmsmasters_project_header .cmsmasters_project_title, 
	.cmsmasters_project_grid .cmsmasters_project_header .cmsmasters_project_title a, 
	.cmsmasters_profile_horizontal .cmsmasters_profile_header h3 a, 
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap,
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h2_font_google_font']) . $cmsmasters_option['dream-city' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h2_font_text_decoration'] . ";
	}
	
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_line_height'] - 14) . "px;
	}
	
	.cmsmasters_profile_vertical .cmsmasters_profile_header_wrap h3 a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_font_size'] + 6) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_line_height'] + 6) . "px;	
	} 
	
	.cmsmasters_profile_horizontal .cmsmasters_profile_header h3 a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_line_height'] + 2) . "px;	
	}
	
	.cmsmasters_open_project .cmsmasters_project_title,
	.cmsmasters_open_post .cmsmasters_post_title, 
	.cmsmasters_post_default .cmsmasters_post_title,
	.cmsmasters_post_default .cmsmasters_post_title a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_font_size'] + 6) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_line_height'] + 2) . "px;
	}
	
	/* Finish H2 Font */


	/* Start H3 Font */
	h3,
	h3 a,
	.cmsmasters_open_profile .profile_details_title,
	.cmsmasters_open_profile .profile_features_title,
	.cmsmasters_open_profile .profile_contact_info_title,
	.cmsmasters_open_profile .profile_social_icons_title, 
	.cmsmasters_quotes_slider .cmsmasters_quote_title, 
	.cmsmasters_tabs .cmsmasters_tabs_list_item a, 
	.cmsmasters_search > .about_author .about_author_cont_title,
	.cmsmasters_archive > .about_author .about_author_cont_title {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h3_font_google_font']) . $cmsmasters_option['dream-city' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h3_font_text_decoration'] . ";
	}
	/* Finish H3 Font */


	/* Start H4 Font */
	h4, 
	h4 a,
	.widgettitle,
	.cmsmasters_pings_list .pingback,  
	.cmsmasters_profile_vertical .cmsmasters_profile_subtitle, 
	.cmsmasters_profile_horizontal .cmsmasters_profile_subtitle, 
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_but,
	.cmsmasters_items_filter_wrap .cmsmasters_items_sort_but,
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list,
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li a,
	.cmsmasters_quotes_slider .cmsmasters_quote_subtitle, 
	.cmsmasters_twitter_wrap .cmsmasters_twitter_item_content, 
	.comment-respond .comment-reply-title, 
	.cmsmasters_toggles .cmsmasters_toggle_title a, 
	.widget_custom_popular_projects_entries .cmsmasters_slider_project_inner .cmsmasters_slider_project_title,
	.widget_custom_popular_projects_entries .cmsmasters_slider_project_inner .cmsmasters_slider_project_title a,
	.widget_custom_latest_projects_entries .cmsmasters_slider_project_inner .cmsmasters_slider_project_title,
	.widget_custom_latest_projects_entries .cmsmasters_slider_project_inner .cmsmasters_slider_project_title a,
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap > li > ul > li > a, 
	.cmsmasters_sitemap_wrap .cmsmasters_sitemap_category > li > a,
	.cmsmasters_open_profile .cmsmasters_profile_header .cmsmasters_profile_subtitle {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h4_font_google_font']) . $cmsmasters_option['dream-city' . '_h4_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h4_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h4_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h4_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h4_font_text_decoration'] . ";
	}
	
	.widgettitle {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h4_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h4_font_line_height'] + 2) . "px;
	}
	
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_but,
	.cmsmasters_items_filter_wrap .cmsmasters_items_sort_but,
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list,
	.cmsmasters_items_filter_wrap .cmsmasters_items_filter_list li a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h4_font_font_size'] - 1) . "px;
	}
	
	/* Finish H4 Font */


	/* Start H5 Font */
	h5,
	h5 a,
	.cmsmasters_post_default .cmsmasters_post_read_more,
	.cmsmasters_post_masonry .cmsmasters_post_read_more,
	.cmsmasters_post_timeline .cmsmasters_post_read_more,
	.cmsmasters_slider_post .cmsmasters_slider_post_read_more, 
	.cmsmasters_project_grid .cmsmasters_slider_post_read_more, 
	.cmsmasters_slider_project .cmsmasters_slider_post_read_more, 
	.cmsmasters_stats.stats_type_vertical .cmsmasters_stat_wrap .cmsmasters_stat_title, 
	.cmsmasters_stats.stats_mode_bars .cmsmasters_stat_wrap .cmsmasters_stat_counter_wrap, 
	.cmsmasters_stats.stats_mode_circles .cmsmasters_stat_wrap .cmsmasters_stat .cmsmasters_stat_inner .cmsmasters_stat_counter_wrap .cmsmasters_stat_units, 
	.cmsmasters_archive_type .cmsmasters_archive_item_type,
	.cmsmasters_woo_tabs.cmsmasters_tabs .cmsmasters_tabs_list_item a,
	.cmsmasters_pricing_table .cmsmasters_period,
	.cmsmasters_stats.stats_type_horizontal .cmsmasters_stat_wrap .cmsmasters_stat_title,
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_title,
	.post_nav > span a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h5_font_google_font']) . $cmsmasters_option['dream-city' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h5_font_text_decoration'] . ";
	}
	
	.cmsmasters_pricing_table .cmsmasters_period {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_line_height'] + 2) . "px;
	}
	
	.cmsmasters_counters .cmsmasters_counter_wrap .cmsmasters_counter .cmsmasters_counter_inner .cmsmasters_counter_title {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_font_size'] + 4) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_line_height'] + 4) . "px;
	}
	
	.cmsmasters_comment_item .cmsmasters_comment_item_cont_info {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_line_height'] - 4) . "px;
	}
	
	.widget_rss ul li * {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h5_font_google_font']) . $cmsmasters_option['dream-city' . '_h5_font_system_font'] . ";
	}
	/* Finish H5 Font */


	/* Start H6 Font */
	h6,
	h6 a,
	.widget_text ul li a, 
	.widget_categories ul li a, 
	.widget_archive ul li a, 
	.widget_meta ul li a, 
	.widget_recent_comments ul li a, 
	.widget_recent_entries ul li a, 
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_lpr_tabs_cont > a,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tab.tab_comments li .cmsmasters_comments_post_title a,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tabs_list .cmsmasters_tabs_list_item a, 
	.cmsmasters_breadcrumbs .cmsmasters_breadcrumbs_inner *,
	.cmsmasters_img .cmsmasters_img_caption,
	.footer_nav > li > a,
	.cmsmasters_quotes_slider .cmsmasters_quote_site,
	.cmsmasters_quotes_slider .cmsmasters_quote_site a,
	.cmsmasters_quotes_grid .cmsmasters_quote_site,
	.cmsmasters_quotes_grid .cmsmasters_quote_site a,
	.cmsmasters_table caption,
	.widget_custom_contact_form_entries .cmsmasters-form-builder *,
	.widget_custom_contact_info_entries div *,
	.widget_custom_contact_info_entries span *,
	.widget_nav_menu .menu a,
	.widget_pages ul li a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h6_font_google_font']) . $cmsmasters_option['dream-city' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h6_font_text_decoration'] . ";
	}
	
	.cmsmasters_breadcrumbs .cmsmasters_breadcrumbs_inner * {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h6_font_google_font']) . $cmsmasters_option['dream-city' . '_h6_font_system_font'] . ";
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_font_size'] - 1) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_line_height'] - 1) . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h6_font_font_style'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h6_font_text_decoration'] . ";
	}
	
	.widget_recent_entries ul li span {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_font_size'] - 2) . "px;
	}

	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_lpr_tabs_cont .published {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_font_size'] - 3) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_font_size'] - 3) . "px;
	}
	/* Finish H6 Font */


	/* Start Button Font */
	.cmsmasters_button, 
	.button, 
	input[type=submit], 
	input[type=button], 
	button,
	.widget_custom_contact_form_entries .cmsmasters-form-builder .button,
	.widget_custom_contact_form_entries .cmsmasters-form-builder .button *,
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tabs_list_item a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_button_font_google_font']) . $cmsmasters_option['dream-city' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_button_font_text_transform'] . ";
	}
	
	.cmsmasters-form-builder .cmsmasters_button, 
	.wpcf7-form .wpcf7-submit {
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_button_font_line_height'] + 8) . "px;
	}
	
	.cmsmasters_post_default .cmsmasters_post_read_more,
	.cmsmasters_post_masonry .cmsmasters_post_read_more,
	.cmsmasters_post_timeline .cmsmasters_post_read_more {
		line-height:" . $cmsmasters_option['dream-city' . '_content_font_line_height'] . "px;
	}
	
	.gform_wrapper .gform_footer input.button, 
	.gform_wrapper .gform_footer input[type=submit] {
		font-size:" . $cmsmasters_option['dream-city' . '_button_font_font_size'] . "px !important;
	}
	
	.widget_custom_posts_tabs_entries .cmsmasters_tabs .cmsmasters_tabs_list_item a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_button_font_font_size'] - 1) . "px;
	}
	
	.widget_custom_contact_form_entries .cmsmasters-form-builder .button,
	.widget_custom_contact_form_entries .cmsmasters-form-builder .button * {
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_button_font_line_height'] - 4) . "px;
	}
	
	.cmsmasters_button.cmsmasters_but_icon_dark_bg, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg, 
	.cmsmasters_button.cmsmasters_but_icon_divider, 
	.cmsmasters_button.cmsmasters_but_icon_inverse {
		padding-left:" . ((int) $cmsmasters_option['dream-city' . '_button_font_line_height'] + 20) . "px;
	}
	
	.cmsmasters_button.cmsmasters_but_icon_dark_bg:before, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg:before, 
	.cmsmasters_button.cmsmasters_but_icon_divider:before, 
	.cmsmasters_button.cmsmasters_but_icon_inverse:before, 
	.cmsmasters_button.cmsmasters_but_icon_dark_bg:after, 
	.cmsmasters_button.cmsmasters_but_icon_light_bg:after, 
	.cmsmasters_button.cmsmasters_but_icon_divider:after, 
	.cmsmasters_button.cmsmasters_but_icon_inverse:after {
		width:" . $cmsmasters_option['dream-city' . '_button_font_line_height'] . "px;
	}
	/* Finish Button Font */


	/* Start Small Text Font */
	small, 
	.cmsmasters_slider_post .cmsmasters_slider_post_cont_info *, 
	.cmsmasters_slider_project .cmsmasters_slider_project_cont_info *, 
	.cmsmasters_project_grid .cmsmasters_project_cont_info *, 
	.cmsmasters_project_puzzle .cmsmasters_project_cont_info *, 
	.cmsmasters_open_post .cmsmasters_post_cont_info *,
	.cmsmasters_post_default .cmsmasters_post_cont_info *,
	.cmsmasters_post_masonry .cmsmasters_post_cont_info *,
	.cmsmasters_post_timeline .cmsmasters_post_cont_info *,
	.cmsmasters_comment_item .cmsmasters_comment_item_content *,
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_user_name,
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_user_name a,
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_category,
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_category a,
	#wp-calendar *, 
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_date_wrap, 
	.cmsmasters_slider_post .cmsmasters_slider_post_date, 
	.cmsmasters_single_slider .cmsmasters_post_date, 
	.cmsmasters_post_default .cmsmasters_post_info,
	.cmsmasters_post_masonry .cmsmasters_post_info,
	.cmsmasters_open_post .cmsmasters_post_meta_info,
	.cmsmasters_post_timeline .cmsmasters_post_info,
	form .formError .formErrorContent {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_small_font_google_font']) . $cmsmasters_option['dream-city' . '_small_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_small_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_small_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_small_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_small_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_small_font_text_transform'] . ";
	}
	
	.cmsmasters_slider_post .cmsmasters_slider_post_read_more, 
	.cmsmasters_slider_post .cmsmasters_slider_post_date {
		line-height:" . ((int)  $cmsmasters_option['dream-city' . '_small_font_line_height'] + 1) . "px;
	}
	
	.cmsmasters-form-builder small,
	.wpcf7-form small {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_small_font_line_height'] - 2) . "px;
	}
	
	.cmsmasters_post_timeline .cmsmasters_post_info {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] + 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_small_font_line_height'] + 2). "px;
		font-weight:400;
	}
	
	.cmsmasters_archive_type .cmsmasters_archive_item_info .cmsmasters_archive_item_date_wrap, 
	.cmsmasters_post_masonry .cmsmasters_post_info {
		line-height:" . $cmsmasters_option['dream-city' . '_content_font_line_height'] . "px;
	}
	
	.gform_wrapper .description, 
	.gform_wrapper .gfield_description, 
	.gform_wrapper .gsection_description, 
	.gform_wrapper .instruction {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_small_font_google_font']) . $cmsmasters_option['dream-city' . '_small_font_system_font'] . " !important;
		font-size:" . $cmsmasters_option['dream-city' . '_small_font_font_size'] . "px !important;
		line-height:" . $cmsmasters_option['dream-city' . '_small_font_line_height'] . "px !important;
	}
	/* Finish Small Text Font */


	/* Start Text Fields Font */
	.wpcf7-form p, 
	.cmsmasters-form-builder label, 
	input:not([type=submit]):not([type=button]):not([type=radio]):not([type=checkbox]),
	textarea,
	select,
	option,
	code {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_input_font_google_font']) . $cmsmasters_option['dream-city' . '_input_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_input_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_input_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_input_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_input_font_font_style'] . ";
	}
	
	.cmsmasters_header_search_form input:not([type=button]):not([type=checkbox]):not([type=file]):not([type=hidden]):not([type=image]):not([type=radio]):not([type=reset]):not([type=submit]):not([type=color]):not([type=range]) {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_input_font_google_font']) . $cmsmasters_option['dream-city' . '_input_font_system_font'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_input_font_font_style'] . ";
	}
	
	.gform_wrapper input:not([type=submit]):not([type=button]):not([type=radio]):not([type=checkbox]),
	.gform_wrapper textarea, 
	.gform_wrapper select {
		font-size:" . $cmsmasters_option['dream-city' . '_input_font_font_size'] . "px !important;
	}
	
	#page input[type=number] {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_input_font_font_size'] + 1) . "px;
	}
	
	/* Finish Text Fields Font */


	/* Start Blockquote Font */
	blockquote,
	.cmsmasters_quotes_grid .cmsmasters_quote_content,
	.cmsmasters_quotes_slider .cmsmasters_quote_content {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_quote_font_google_font']) . $cmsmasters_option['dream-city' . '_quote_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_quote_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_quote_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_quote_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_quote_font_font_style'] . ";
	}
	
	q {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_quote_font_google_font']) . $cmsmasters_option['dream-city' . '_quote_font_system_font'] . ";
		font-weight:" . $cmsmasters_option['dream-city' . '_quote_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_quote_font_font_style'] . ";
	}
	
	.cmsmasters_quotes_grid .cmsmasters_quote_content {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_quote_font_font_size'] - 8) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_quote_font_line_height'] - 6)  . "px;
	}
	/* Finish Blockquote Font */

/***************** Finish Theme Font Styles ******************/


";
	
	
	return apply_filters('dream_city_theme_fonts_filter', $custom_css);
}

