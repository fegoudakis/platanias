<?php 
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Theme Admin Options
 * Created by CMSMasters
 * 
 */


/* Single Options */
function dream_city_single_options_meta_fields($custom_all_meta_fields) {
	$cmsmasters_option = dream_city_get_global_options();
	
	
	$custom_all_meta_fields_new = array();
	
	
	if (
		(isset($_GET['post_type']) && $_GET['post_type'] == 'profile') || 
		(isset($_POST['post_type']) && $_POST['post_type'] == 'profile') || 
		(isset($_GET['post']) && get_post_type($_GET['post']) == 'profile') 
	) {
		foreach ($custom_all_meta_fields as $custom_all_meta_field) {
			if (
				$custom_all_meta_field['id'] == 'cmsmasters_profile_social'
			) {	
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Profile Contact Info Title', 'dream-city'), 
					'desc'	=> '', 
					'id'	=> 'cmsmasters_profile_contact_info_block_title', 
					'type'	=> 'text_long', 
					'hide'	=> '', 
					'std'	=> '' 
				);
				
				$custom_all_meta_fields_new[] = array( 
					'label'	=> esc_html__('Profile Contact Info', 'dream-city'), 
					'desc'	=> esc_html__('Add contact info for this profile', 'dream-city'), 
					'id'	=> 'cmsmasters_profile_contact_info', 
					'type'	=> 'contact', 
					'hide'	=> '', 
					'std'	=> '' 
				);
				
				
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			} else {
				$custom_all_meta_fields_new[] = $custom_all_meta_field;
			}
		}
	} else {
		$custom_all_meta_fields_new = $custom_all_meta_fields;
	}
	
	
	return $custom_all_meta_fields_new;
}

add_filter('get_custom_all_meta_fields_filter', 'dream_city_single_options_meta_fields');
