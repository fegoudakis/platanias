<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.1
 * 
 * TGM-Plugin-Activation 2.6.1
 * Created by CMSMasters
 * 
 */


require_once(get_template_directory() . '/framework/class/class-tgm-plugin-activation.php');


if (!function_exists('dream_city_register_theme_plugins')) {

function dream_city_register_theme_plugins() { 
	$plugins = array( 
		array( 
			'name'					=> esc_html__('CMSMasters Contact Form Builder', 'dream-city'), 
			'slug'					=> 'cmsmasters-contact-form-builder', 
			'source'				=> get_template_directory_uri() . '/theme-framework/admin/plugins/cmsmasters-contact-form-builder.zip', 
			'required'				=> false, 
			'version'				=> '1.3.8', 
			'force_activation'		=> false, 
			'force_deactivation' 	=> false 
		), 
		array( 
			'name'					=> esc_html__('CMSMasters Content Composer', 'dream-city'), 
			'slug'					=> 'cmsmasters-content-composer', 
			'source'				=> get_template_directory_uri() . '/theme-framework/admin/plugins/cmsmasters-content-composer.zip', 
			'required'				=> true, 
			'version'				=> '2.0.2', 
			'force_activation'		=> false, 
			'force_deactivation' 	=> false 
		), 
		array( 
			'name'					=> esc_html__('CMSMasters Mega Menu', 'dream-city'), 
			'slug'					=> 'cmsmasters-mega-menu', 
			'source'				=> get_template_directory_uri() . '/theme-framework/admin/plugins/cmsmasters-mega-menu.zip', 
			'required'				=> true, 
			'version'				=> '1.2.7', 
			'force_activation'		=> false, 
			'force_deactivation' 	=> false 
		), 
		array( 
			'name' 					=> esc_html__('LayerSlider WP', 'dream-city'), 
			'slug' 					=> 'LayerSlider', 
			'source'				=> get_template_directory_uri() . '/theme-framework/admin/plugins/LayerSlider.zip', 
			'required'				=> false, 
			'version'				=> '6.1.6', 
			'force_activation'		=> false, 
			'force_deactivation' 	=> false 
		), 
		array( 
			'name' 					=> esc_html__('Revolution Slider', 'dream-city'), 
			'slug' 					=> 'revslider', 
			'source'				=> get_template_directory_uri() . '/theme-framework/admin/plugins/revslider.zip', 
			'required'				=> false, 
			'version'				=> '5.3.1.5', 
			'force_activation'		=> false, 
			'force_deactivation' 	=> false 
		), 
		array( 
			'name' 					=> esc_html__('The Events Calendar', 'dream-city'), 
			'slug' 					=> 'the-events-calendar', 
			'required'				=> false 
		), 
		array( 
			'name' 					=> esc_html__('Contact Form 7', 'dream-city'), 
			'slug' 					=> 'contact-form-7', 
			'required' 				=> false 
		), 
		array( 
			'name' 					=> esc_html__('WordPress SEO by Yoast', 'dream-city'), 
			'slug' 					=> 'wordpress-seo', 
			'required' 				=> false 
		), 
		array( 
			'name'					=> esc_html__('Instagram Feed', 'dream-city'), 
			'slug'					=> 'instagram-feed', 
			'required'				=> false 
 		), 
		array( 
			'name'					=> esc_html__('MailPoet Newsletters', 'dream-city'), 
			'slug'					=> 'wysija-newsletters', 
			'required'				=> false 
		) 
	);
	
	
	$config = array( 
		'id' => 			'dream-city', 
		'menu' => 			'theme-required-plugins', 
		'strings' => array( 
			'page_title' => 	esc_html__('Theme Required & Recommended Plugins', 'dream-city'), 
			'menu_title' => 	esc_html__('Theme Plugins', 'dream-city'), 
			'return' => 		esc_html__('Return to Theme Required & Recommended Plugins', 'dream-city') 
		) 
	);
	
	
	tgmpa($plugins, $config);
}

}

add_action('tgmpa_register', 'dream_city_register_theme_plugins');

