<?php 
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Theme Admin Settings
 * Created by CMSMasters
 * 
 */


 /* General Settings */
function dream_city_theme_options_general_fields($options, $tab) {
	if ($tab == 'footer') {
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_bg_image_enable', 
			'title' => esc_html__('Footer Background Image Visibility by Default', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => 1 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_bg_image', 
			'title' => esc_html__('Footer Background Image by Default', 'dream-city'), 
			'desc' => esc_html__('Choose your footer background image by default.', 'dream-city'), 
			'type' => 'upload', 
			'std' => '', 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_bg_repeat', 
			'title' => esc_html__('Footer Background Repeat by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'no-repeat', 
			'choices' => array( 
				esc_html__('No Repeat', 'dream-city') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'dream-city') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'dream-city') . '|repeat-y', 
				esc_html__('Repeat', 'dream-city') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_bg_pos', 
			'title' => esc_html__('Background Position', 'dream-city'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => 'top center', 
			'choices' => array( 
				esc_html__('Top Left', 'dream-city') . '|top left', 
				esc_html__('Top Center', 'dream-city') . '|top center', 
				esc_html__('Top Right', 'dream-city') . '|top right', 
				esc_html__('Center Left', 'dream-city') . '|center left', 
				esc_html__('Center Center', 'dream-city') . '|center center', 
				esc_html__('Center Right', 'dream-city') . '|center right', 
				esc_html__('Bottom Left', 'dream-city') . '|bottom left', 
				esc_html__('Bottom Center', 'dream-city') . '|bottom center', 
				esc_html__('Bottom Right', 'dream-city') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_bg_attachment', 
			'title' => esc_html__('Footer Background Attachment by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'fixed', 
			'choices' => array( 
				esc_html__('Scroll', 'dream-city') . '|scroll', 
				esc_html__('Fixed', 'dream-city') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_bg_size', 
			'title' => esc_html__('Footer Background Size by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => 'cover', 
			'choices' => array( 
				esc_html__('Auto', 'dream-city') . '|auto', 
				esc_html__('Cover', 'dream-city') . '|cover', 
				esc_html__('Contain', 'dream-city') . '|contain' 
			) 
		);
	}
	
	
	return $options;
}

add_filter('cmsmasters_options_general_fields_filter', 'dream_city_theme_options_general_fields', 10, 2);


/* Color Settings */
function dream_city_theme_options_color_fields($options, $tab) {
	$defaults = dream_city_color_schemes_defaults();
	
	
	if ($tab != 'header' && $tab != 'navigation' && $tab != 'header_top') {
		$options[] = array( 
			'section' => $tab . '_section', 
			'id' => 'dream-city' . '_' . $tab . '_secondary', 
			'title' => esc_html__('Secondary Color', 'dream-city'), 
			'desc' => esc_html__('Secondary color for some elements', 'dream-city'), 
			'type' => 'rgba', 
			'std' => (isset($defaults[$tab])) ? $defaults[$tab]['secondary'] : $defaults['default']['secondary'] 
		);
	}
	
	
	return $options;
}

add_filter('cmsmasters_options_color_fields_filter', 'dream_city_theme_options_color_fields', 10, 2);