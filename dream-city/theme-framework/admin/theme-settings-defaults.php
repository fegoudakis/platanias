<?php 
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Theme Settings Defaults
 * Created by CMSMasters
 * 
 */


/* Theme Settings General Default Values */
if (!function_exists('dream_city_settings_general_defaults')) {

function dream_city_settings_general_defaults($id = false) {
	$settings = array( 
		'general' => array( 
			'dream-city' . '_theme_layout' => 		'liquid', 
			'dream-city' . '_logo_type' => 			'image', 
			'dream-city' . '_logo_url' => 			'|' . get_template_directory_uri() . '/theme-framework/img/logo.png', 
			'dream-city' . '_logo_url_retina' => 		'|' . get_template_directory_uri() . '/theme-framework/img/logo_retina.png', 
			'dream-city' . '_logo_title' => 			get_bloginfo('name') ? get_bloginfo('name') : 'Dream City', 
			'dream-city' . '_logo_subtitle' => 		'', 
			'dream-city' . '_logo_custom_color' => 	0, 
			'dream-city' . '_logo_title_color' => 		'', 
			'dream-city' . '_logo_subtitle_color' => 	'' 
		), 
		'bg' => array( 
			'dream-city' . '_bg_col' => 			'#ffffff', 
			'dream-city' . '_bg_img_enable' => 	0, 
			'dream-city' . '_bg_img' => 			'', 
			'dream-city' . '_bg_rep' => 			'no-repeat', 
			'dream-city' . '_bg_pos' => 			'top center', 
			'dream-city' . '_bg_att' => 			'scroll', 
			'dream-city' . '_bg_size' => 			'cover' 
		), 
		'header' => array( 
			'dream-city' . '_fixed_header' => 				1, 
			'dream-city' . '_header_overlaps' => 				1, 
			'dream-city' . '_header_top_line' => 				1, 
			'dream-city' . '_header_top_height' => 			'29', 
			'dream-city' . '_header_top_line_short_info' => 	'', 
			'dream-city' . '_header_top_line_add_cont' => 	'nav', 
			'dream-city' . '_header_styles' => 				'default', 
			'dream-city' . '_header_mid_height' => 			'81', 
			'dream-city' . '_header_bot_height' => 			'65', 
			'dream-city' . '_header_search' => 				1, 
			'dream-city' . '_header_add_cont' => 				'social', 
			'dream-city' . '_header_add_cont_cust_html' => 	'' 
		), 
		'content' => array( 
			'dream-city' . '_layout' => 					'r_sidebar', 
			'dream-city' . '_archives_layout' => 			'r_sidebar', 
			'dream-city' . '_search_layout' => 			'r_sidebar', 
			'dream-city' . '_other_layout' => 			'r_sidebar', 
			'dream-city' . '_heading_alignment' => 		'center', 
			'dream-city' . '_heading_scheme' => 			'default', 
			'dream-city' . '_heading_bg_image_enable' => 	0, 
			'dream-city' . '_heading_bg_image' => 		'', 
			'dream-city' . '_heading_bg_repeat' => 		'no-repeat', 
			'dream-city' . '_heading_bg_attachment' => 	'scroll', 
			'dream-city' . '_heading_bg_size' => 			'cover', 
			'dream-city' . '_heading_bg_color' => 		'#424242', 
			'dream-city' . '_heading_height' => 			'410', 
			'dream-city' . '_breadcrumbs' => 				1, 
			'dream-city' . '_bottom_scheme' => 			'first', 
			'dream-city' . '_bottom_sidebar' => 			1, 
			'dream-city' . '_bottom_sidebar_layout' => 	'14141414' 
		), 
		'footer' => array( 
			'dream-city' . '_footer_scheme' => 				'footer', 
			'dream-city' . '_footer_type' => 					'small', 
			'dream-city' . '_footer_additional_content' => 	'social', 
			'dream-city' . '_footer_logo' => 					1, 
			'dream-city' . '_footer_logo_url' => 				'|' . get_template_directory_uri() . '/theme-framework/img/logo_footer.png', 
			'dream-city' . '_footer_logo_url_retina' => 		'|' . get_template_directory_uri() . '/theme-framework/img/logo_footer_retina.png', 
			'dream-city' . '_footer_nav' => 					1, 
			'dream-city' . '_footer_social' => 				1, 
			'dream-city' . '_footer_html' => 					'', 
			'dream-city' . '_footer_copyright' => 			'Dream City' . ' &copy; 2017 / ' . esc_html__('All Rights Reserved', 'dream-city') 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



/* Add Google Font */
if (!function_exists('dream_city_add_foogle_font')) {

function dream_city_add_foogle_font($fonts) {
	$fonts['Hind:300,400,500'] = 'Hind';
	
	
	return $fonts;
}

}

add_filter('dream_city_google_fonts_list_filter', 'dream_city_add_foogle_font');



/* Theme Settings Fonts Default Values */
if (!function_exists('dream_city_settings_font_defaults')) {

function dream_city_settings_font_defaults($id = false) {
	$settings = array( 
		'content' => array( 
			'dream-city' . '_content_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'14', 
				'line_height' => 		'22', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			) 
		), 
		'link' => array( 
			'dream-city' . '_link_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'14', 
				'line_height' => 		'22', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'dream-city' . '_link_hover_decoration' => 	'none' 
		), 
		'nav' => array( 
			'dream-city' . '_nav_title_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'16', 
				'line_height' => 		'24', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			), 
			'dream-city' . '_nav_dropdown_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'14', 
				'line_height' => 		'20', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			) 
		), 
		'heading' => array( 
			'dream-city' . '_h1_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'36', 
				'line_height' => 		'42', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'dream-city' . '_h2_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'22', 
				'line_height' => 		'32', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'dream-city' . '_h3_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'20', 
				'line_height' => 		'32', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'dream-city' . '_h4_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'16', 
				'line_height' => 		'26', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'dream-city' . '_h5_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'14', 
				'line_height' => 		'22', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			), 
			'dream-city' . '_h6_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'14', 
				'line_height' => 		'22', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none', 
				'text_decoration' => 	'none' 
			) 
		), 
		'other' => array( 
			'dream-city' . '_button_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'13', 
				'line_height' => 		'41', 
				'font_weight' => 		'500', 
				'font_style' => 		'normal', 
				'text_transform' => 	'uppercase' 
			), 
			'dream-city' . '_small_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'14', 
				'line_height' => 		'20', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal', 
				'text_transform' => 	'none' 
			), 
			'dream-city' . '_input_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'16', 
				'line_height' => 		'22', 
				'font_weight' => 		'normal', 
				'font_style' => 		'normal' 
			), 
			'dream-city' . '_quote_font' => array( 
				'system_font' => 		"Arial, Helvetica, 'Nimbus Sans L', sans-serif", 
				'google_font' => 		'Hind:300,400,500', 
				'font_size' => 			'24', 
				'line_height' => 		'38', 
				'font_weight' => 		'300', 
				'font_style' => 		'normal' 
			) 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// WP Color Picker Palettes
if (!function_exists('cmsmasters_color_picker_palettes')) {

function cmsmasters_color_picker_palettes() {
	$palettes = array( 
		'#6f6f6f', 
		'#ff4d58', 
		'#a0a0a0', 
		'#282828', 
		'#fafafa', 
		'#ffffff', 
		'#dedede', 
		'#486acd'
	);
	
	
	return $palettes;
}

}



// Theme Settings Color Schemes Default Colors
if (!function_exists('dream_city_color_schemes_defaults')) {

function dream_city_color_schemes_defaults($id = false) {
	$settings = array( 
		'default' => array( // content default color scheme
			'color' => 		'#6f6f6f', 
			'link' => 		'#ff4d58', 
			'hover' => 		'#a0a0a0', 
			'heading' => 	'#282828', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#dedede', 
			'secondary' =>	'#486acd'
		), 
		'header' => array( // Header color scheme
			'mid_color' => 		'#6f6f6f', 
			'mid_link' => 		'#2a2a2a', 
			'mid_hover' => 		'#ff4d58', 
			'mid_bg' => 		'#ffffff', 
			'mid_bg_scroll' => 	'#ffffff', 
			'mid_border' => 	'rgba(255,255,255,0)', 
			'bot_color' => 		'#6f6f6f', 
			'bot_link' => 		'#2a2a2a', 
			'bot_hover' => 		'#ff4d58', 
			'bot_bg' => 		'#ffffff', 
			'bot_bg_scroll' => 	'#ffffff', 
			'bot_border' => 	'rgba(255,255,255,0)' 
		), 
		'navigation' => array( // Navigation color scheme
			'title_link' => 			'#2a2a2a', 
			'title_link_hover' => 		'#ff4d58', 
			'title_link_current' => 	'#ff5b66', 
			'title_link_subtitle' => 	'rgba(143,143,143,0.7)', 
			'title_link_bg' => 			'rgba(255,255,255,0)', 
			'title_link_bg_hover' => 	'rgba(255,255,255,0)', 
			'title_link_bg_current' => 	'rgba(255,255,255,0)', 
			'title_link_border' => 		'rgba(255,255,255,0)', 
			'dropdown_text' => 			'#4c4c4c', 
			'dropdown_bg' => 			'#fafafa', 
			'dropdown_border' => 		'rgba(255,255,255,0)', 
			'dropdown_link' => 			'#4c4c4c', 
			'dropdown_link_hover' => 	'#ff4d58', 
			'dropdown_link_subtitle' => 'rgba(143,143,143,0.7)', 
			'dropdown_link_highlight' => '#ff4d58', 
			'dropdown_link_border' => 	'rgba(255,255,255,0)' 
		), 
		'header_top' => array( // Header Top color scheme
			'color' => 					'#ffffff', 
			'link' => 					'#ffffff', 
			'hover' => 					'rgba(255,255,255,.5)', 
			'bg' => 					'#486acd', 
			'border' => 				'rgba(255,255,255,0)', 
			'title_link' => 			'#ffffff', 
			'title_link_hover' => 		'rgba(255,255,255,0.5)', 
			'title_link_bg' => 			'rgba(255,255,255,0)', 
			'title_link_bg_hover' => 	'rgba(255,255,255,0)', 
			'title_link_border' => 		'rgba(255,255,255,0)', 
			'dropdown_bg' => 			'#fafafa', 
			'dropdown_border' => 		'rgba(255,255,255,0)', 
			'dropdown_link' => 			'#4c4c4c', 
			'dropdown_link_hover' => 	'#ff4d58', 
			'dropdown_link_highlight' => '#ff4d58', 
			'dropdown_link_border' => 	'rgba(255,255,255,0)' 
		), 
		'footer' => array( // Footer color scheme
			'color' => 		'#ffffff', 
			'link' => 		'#ffffff', 
			'hover' => 		'rgba(255,255,255,0.5)', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#486acd', 
			'alternate' => 	'#486acd', 
			'border' => 	'rgba(220,221,225,0.2)', 
			'secondary' =>	'#ffffff'
		), 
		'first' => array( // custom color scheme 1
			'color' => 		'#6f6f6f', 
			'link' => 		'#ff4d58', 
			'hover' => 		'#a0a0a0', 
			'heading' => 	'#282828', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#dedede', 
			'secondary' =>	'#486acd'
		), 
		'second' => array( // custom color scheme 2
			'color' => 		'#ffffff', 
			'link' => 		'rgba(255,255,255,0.6)', 
			'hover' => 		'#486acd', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#ffffff', 
			'alternate' => 	'rgba(255,255,255,0.05)', 
			'border' => 	'#dedede', 
			'secondary' =>	'rgba(255,255,255,0.8)'
		), 
		'third' => array( // custom color scheme 3
			'color' => 		'#ffffff', 
			'link' => 		'#ff4d58', 
			'hover' => 		'#a0a0a0', 
			'heading' => 	'#ffffff', 
			'bg' => 		'#fafafa', 
			'alternate' => 	'#ffffff', 
			'border' => 	'#dedede', 
			'secondary' =>	'#486acd'
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// Theme Settings Elements Default Values
if (!function_exists('dream_city_settings_element_defaults')) {

function dream_city_settings_element_defaults($id = false) {
	$settings = array( 
		'sidebar' => array( 
			'dream-city' . '_sidebar' => 	'' 
		), 
		'icon' => array( 
			'dream-city' . '_social_icons' => array( 
				'cmsmasters-icon-facebook-1|#|' . esc_html__('Facebook', 'dream-city') . '|false||', 
				'cmsmasters-icon-gplus-1|#|' . esc_html__('Google+', 'dream-city') . '|false||', 
				'cmsmasters-icon-instagram|#|' . esc_html__('Instagram', 'dream-city') . '|false||', 
				'cmsmasters-icon-twitter|#|' . esc_html__('Twitter', 'dream-city') . '|false||', 
				'cmsmasters-icon-youtube-play|#|' . esc_html__('YouTube', 'dream-city') . '|false||' 
			) 
		), 
		'lightbox' => array( 
			'dream-city' . '_ilightbox_skin' => 					'dark', 
			'dream-city' . '_ilightbox_path' => 					'vertical', 
			'dream-city' . '_ilightbox_infinite' => 				0, 
			'dream-city' . '_ilightbox_aspect_ratio' => 			1, 
			'dream-city' . '_ilightbox_mobile_optimizer' => 		1, 
			'dream-city' . '_ilightbox_max_scale' => 				1, 
			'dream-city' . '_ilightbox_min_scale' => 				0.2, 
			'dream-city' . '_ilightbox_inner_toolbar' => 			0, 
			'dream-city' . '_ilightbox_smart_recognition' => 		0, 
			'dream-city' . '_ilightbox_fullscreen_one_slide' => 	0, 
			'dream-city' . '_ilightbox_fullscreen_viewport' => 	'center', 
			'dream-city' . '_ilightbox_controls_toolbar' => 		1, 
			'dream-city' . '_ilightbox_controls_arrows' => 		0, 
			'dream-city' . '_ilightbox_controls_fullscreen' => 	1, 
			'dream-city' . '_ilightbox_controls_thumbnail' => 	1, 
			'dream-city' . '_ilightbox_controls_keyboard' => 		1, 
			'dream-city' . '_ilightbox_controls_mousewheel' => 	1, 
			'dream-city' . '_ilightbox_controls_swipe' => 		1, 
			'dream-city' . '_ilightbox_controls_slideshow' => 	0 
		), 
		'sitemap' => array( 
			'dream-city' . '_sitemap_nav' => 			1, 
			'dream-city' . '_sitemap_categs' => 		1, 
			'dream-city' . '_sitemap_tags' => 		1, 
			'dream-city' . '_sitemap_month' => 		1, 
			'dream-city' . '_sitemap_pj_categs' => 	1, 
			'dream-city' . '_sitemap_pj_tags' => 		1 
		), 
		'error' => array( 
			'dream-city' . '_error_color' => 				'#ffffff', 
			'dream-city' . '_error_bg_color' => 			'#424242', 
			'dream-city' . '_error_bg_img_enable' => 		0, 
			'dream-city' . '_error_bg_image' => 			'', 
			'dream-city' . '_error_bg_rep' => 			'no-repeat', 
			'dream-city' . '_error_bg_pos' => 			'top center', 
			'dream-city' . '_error_bg_att' => 			'scroll', 
			'dream-city' . '_error_bg_size' => 			'cover', 
			'dream-city' . '_error_search' => 			1, 
			'dream-city' . '_error_sitemap_button' => 	1, 
			'dream-city' . '_error_sitemap_link' => 		'' 
		), 
		'code' => array( 
			'dream-city' . '_custom_css' => 			'', 
			'dream-city' . '_custom_js' => 			'', 
			'dream-city' . '_gmap_api_key' => 		'', 
			'dream-city' . '_api_key' => 				'', 
			'dream-city' . '_api_secret' => 			'', 
			'dream-city' . '_access_token' => 		'', 
			'dream-city' . '_access_token_secret' => 	'' 
		), 
		'recaptcha' => array( 
			'dream-city' . '_recaptcha_public_key' => 	'', 
			'dream-city' . '_recaptcha_private_key' => 	'' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



// Theme Settings Single Posts Default Values
if (!function_exists('dream_city_settings_single_defaults')) {

function dream_city_settings_single_defaults($id = false) {
	$settings = array( 
		'post' => array( 
			'dream-city' . '_blog_post_layout' => 		'r_sidebar', 
			'dream-city' . '_blog_post_title' => 			1, 
			'dream-city' . '_blog_post_date' => 			1, 
			'dream-city' . '_blog_post_cat' => 			1, 
			'dream-city' . '_blog_post_author' => 		1, 
			'dream-city' . '_blog_post_comment' => 		1, 
			'dream-city' . '_blog_post_tag' => 			1, 
			'dream-city' . '_blog_post_like' => 			1, 
			'dream-city' . '_blog_post_nav_box' => 		1, 
			'dream-city' . '_blog_post_share_box' => 		1, 
			'dream-city' . '_blog_post_author_box' => 	1, 
			'dream-city' . '_blog_more_posts_box' => 		'popular', 
			'dream-city' . '_blog_more_posts_count' => 	'3', 
			'dream-city' . '_blog_more_posts_pause' => 	'5' 
		), 
		'project' => array( 
			'dream-city' . '_portfolio_project_title' => 			1, 
			'dream-city' . '_portfolio_project_details_title' => 	esc_html__('Project details', 'dream-city'), 
			'dream-city' . '_portfolio_project_date' => 			1, 
			'dream-city' . '_portfolio_project_cat' => 			1, 
			'dream-city' . '_portfolio_project_author' => 		1, 
			'dream-city' . '_portfolio_project_comment' => 		0, 
			'dream-city' . '_portfolio_project_tag' => 			0, 
			'dream-city' . '_portfolio_project_like' => 			1, 
			'dream-city' . '_portfolio_project_link' => 			0, 
			'dream-city' . '_portfolio_project_share_box' => 		1, 
			'dream-city' . '_portfolio_project_nav_box' => 		1, 
			'dream-city' . '_portfolio_project_author_box' => 	1, 
			'dream-city' . '_portfolio_more_projects_box' => 		'popular', 
			'dream-city' . '_portfolio_more_projects_count' => 	'4', 
			'dream-city' . '_portfolio_more_projects_pause' => 	'5', 
			'dream-city' . '_portfolio_project_slug' => 			'project', 
			'dream-city' . '_portfolio_pj_categs_slug' => 		'pj-categs', 
			'dream-city' . '_portfolio_pj_tags_slug' => 			'pj-tags' 
		), 
		'profile' => array( 
			'dream-city' . '_profile_post_title' => 			1, 
			'dream-city' . '_profile_post_details_title' => 	esc_html__('Profile details', 'dream-city'), 
			'dream-city' . '_profile_post_cat' => 			1, 
			'dream-city' . '_profile_post_comment' => 		1, 
			'dream-city' . '_profile_post_like' => 			1, 
			'dream-city' . '_profile_post_nav_box' => 		1, 
			'dream-city' . '_profile_post_share_box' => 		1, 
			'dream-city' . '_profile_post_slug' => 			'profile', 
			'dream-city' . '_profile_pl_categs_slug' => 		'pl-categs' 
		) 
	);
	
	
	if ($id) {
		return $settings[$id];
	} else {
		return $settings;
	}
}

}



/* Project Puzzle Proportion */
if (!function_exists('dream_city_project_puzzle_proportion')) {

function dream_city_project_puzzle_proportion() {
	return 0.7245;
}

}



/* Theme Image Thumbnails Size */
if (!function_exists('dream_city_get_image_thumbnail_list')) {

function dream_city_get_image_thumbnail_list() {
	$list = array( 
		'cmsmasters-small-thumb' => array( 
			'width' => 		90, 
			'height' => 	90, 
			'crop' => 		true 
		), 
		'cmsmasters-square-thumb' => array( 
			'width' => 		300, 
			'height' => 	300, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Square', 'dream-city') 
		), 
		'cmsmasters-blog-masonry-thumb' => array( 
			'width' => 		580, 
			'height' => 	386, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Masonry Blog', 'dream-city') 
		), 
		'cmsmasters-project-thumb' => array( 
			'width' => 		580, 
			'height' => 	420, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project', 'dream-city') 
		), 
		'cmsmasters-project-slider-thumb' => array( 
			'width' => 		580, 
			'height' => 	490, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project Slider', 'dream-city') 
		), 
		'cmsmasters-project-masonry-thumb' => array( 
			'width' => 		580, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry Project', 'dream-city') 
		), 
		'post-thumbnail' => array( 
			'width' => 		860, 
			'height' => 	530, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Featured', 'dream-city') 
		), 
		'cmsmasters-masonry-thumb' => array( 
			'width' => 		860, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry', 'dream-city') 
		), 
		'cmsmasters-full-thumb' => array( 
			'width' => 		1160, 
			'height' => 	600, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Full', 'dream-city') 
		), 
		'cmsmasters-project-full-thumb' => array( 
			'width' => 		1160, 
			'height' => 	650, 
			'crop' => 		true, 
			'title' => 		esc_attr__('Project Full', 'dream-city') 
		), 
		'cmsmasters-full-masonry-thumb' => array( 
			'width' => 		1160, 
			'height' => 	9999, 
			'title' => 		esc_attr__('Masonry Full', 'dream-city') 
		) 
	);
	
	
	return $list;
}

}



/* Project Post Type Registration Rename */
if (!function_exists('dream_city_project_labels')) {

function dream_city_project_labels() {
	return array( 
		'name' => 					esc_html__('Υπηρεσίες', 'dream-city'), 
		'singular_name' => 			esc_html__('Υπηρεσία', 'dream-city'), 
		'menu_name' => 				esc_html__('Υπηρεσίες', 'dream-city'), 
		'all_items' => 				esc_html__('Όλες οι Υπηρεσίες', 'dream-city'), 
		'add_new' => 				esc_html__('Προσθήκη Νέας', 'dream-city'), 
		'add_new_item' => 			esc_html__('Προσθήκη Νέας Υηρεσίας', 'dream-city'), 
		'edit_item' => 				esc_html__('Επεξεργασία Υπηρεσίας', 'dream-city'), 
		'new_item' => 				esc_html__('Νέα Υπηρεσία', 'dream-city'), 
		'view_item' => 				esc_html__('Δες την Υπηρεσία', 'dream-city'), 
		'search_items' => 			esc_html__('Αναζήτηση Υπηρεσίας', 'dream-city'), 
		'not_found' => 				esc_html__('Δεν βρέθηκαν Υπηρεσίες', 'dream-city'), 
		'not_found_in_trash' => 	esc_html__('Δεν βρέθηκαν Υπηρεσίες στον κάδο', 'dream-city') 
	);
}

}

 add_filter('cmsmasters_project_labels_filter', 'dream_city_project_labels');


if (!function_exists('dream_city_pj_categs_labels')) {

function dream_city_pj_categs_labels() {
	return array( 
		'name' => 					esc_html__('Κατηγορίες Υπηρεσιών', 'dream-city'), 
		'singular_name' => 			esc_html__('Κατηγορία Υπηρεσιών', 'dream-city') 
	);
}

}

 add_filter('cmsmasters_pj_categs_labels_filter', 'dream_city_pj_categs_labels');


if (!function_exists('dream_city_pj_tags_labels')) {

function dream_city_pj_tags_labels() {
	return array( 
		'name' => 					esc_html__('Tags Υπηρεσιών', 'dream-city'), 
		'singular_name' => 			esc_html__('Tag Υπηρεσίας', 'dream-city') 
	);
}

}

 add_filter('cmsmasters_pj_tags_labels_filter', 'dream_city_pj_tags_labels');



/* Profile Post Type Registration Rename */
if (!function_exists('dream_city_profile_labels')) {

function dream_city_profile_labels() {
	return array( 
		'name' => 					esc_html__('Profiles', 'dream-city'), 
		'singular_name' => 			esc_html__('Profiles', 'dream-city'), 
		'menu_name' => 				esc_html__('Profiles', 'dream-city'), 
		'all_items' => 				esc_html__('All Profiles', 'dream-city'), 
		'add_new' => 				esc_html__('Add New', 'dream-city'), 
		'add_new_item' => 			esc_html__('Add New Profile', 'dream-city'), 
		'edit_item' => 				esc_html__('Edit Profile', 'dream-city'), 
		'new_item' => 				esc_html__('New Profile', 'dream-city'), 
		'view_item' => 				esc_html__('View Profile', 'dream-city'), 
		'search_items' => 			esc_html__('Search Profiles', 'dream-city'), 
		'not_found' => 				esc_html__('No Profiles found', 'dream-city'), 
		'not_found_in_trash' => 	esc_html__('No Profiles found in Trash', 'dream-city') 
	);
}

}

// add_filter('cmsmasters_profile_labels_filter', 'dream_city_profile_labels');


if (!function_exists('dream_city_pl_categs_labels')) {

function dream_city_pl_categs_labels() {
	return array( 
		'name' => 					esc_html__('Profile Categories', 'dream-city'), 
		'singular_name' => 			esc_html__('Profile Category', 'dream-city') 
	);
}

}

// add_filter('cmsmasters_pl_categs_labels_filter', 'dream_city_pl_categs_labels');

