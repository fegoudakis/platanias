<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Post Default Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_post_metadata = !is_home() ? explode(',', $cmsmasters_metadata) : array();


$date = (in_array('date', $cmsmasters_post_metadata) || is_home()) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_post_metadata) || is_home())) ? true : false;
$author = (in_array('author', $cmsmasters_post_metadata) || is_home()) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_post_metadata) || is_home())) ? true : false;
$likes = (in_array('likes', $cmsmasters_post_metadata) || is_home()) ? true : false;
$more = (in_array('more', $cmsmasters_post_metadata) || is_home()) ? true : false;


$cmsmasters_post_format = get_post_format();

?>
<?php if (is_page(7366) || is_page(71342)) : ?>
<article style="padding: 30px 0 0;"id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_archive_type'); ?>>
<?php
	 $cat = get_the_category( $post->ID );
	/*echo '<pre>';
	var_dump($cat);
	echo '</pre>';*/
	$no_photo = $cat[0]->term_id;
	$no_photo2 = $cat[0]->parent;

	if ($no_photo != 100 && $no_photo2 != 101 && $no_photo != 188 ){
		if (!post_password_required() && has_post_thumbnail()) { ?>
			<div class="cmsmasters_archive_item_img_wrap front_thumbnail_size nea_epikaira" style="background-image:url('<?php the_post_thumbnail_url(); ?>');">
				
			
			</div>
		<?php } ?>
		
<?php }	?>
	<div class="cmsmasters_archive_item_cont_wrap" >
	
	<div class="cmsmasters_archive_item_type">
			<?php
			/*$post_type_obj = get_post_type_object(get_post_type());

			echo '<span>' . $post_type_obj->labels->singular_name . '</span>';*/
			if ($date) {
			echo '<div class="cmsmasters_post_info entry-meta">';
			
				$date ? dream_city_get_post_date('page', 'default') : '';
				
			echo '</div>';
		}
			?>
		</div>
		<?php

		if (cmsmasters_title(get_the_ID(), false) != get_the_ID()) {
			?>
			<header class="cmsmasters_archive_item_header entry-header">
				<h2 class="cmsmasters_archive_item_title entry-title">
					<a href="<?php the_permalink(); ?>">
						<?php cmsmasters_title(get_the_ID(), true); ?>
					</a>
				</h2>
			</header>
			<?php
		}


		if (theme_excerpt(30, false) != '') {
			echo cmsmasters_divpdel('<div class="cmsmasters_archive_item_content entry-content">' . "\n" .
				wpautop(theme_excerpt(30, false)) .
			'</div>' . "\n");
		} ?>
		<?php
		if (get_post_type() == 'post' || $current_tax != '') {
			echo '<footer class="cmsmasters_archive_item_info entry-meta">';

			/*	if (get_post_type() == 'post') {
					echo '<span class="cmsmasters_archive_item_user_name">' .
						esc_html__('by', 'dream-city') . ' ' .
						'<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '" rel="author" title="' . esc_attr__('Posts by', 'dream-city') . ' ' . get_the_author_meta('display_name') . '">' . get_the_author_meta('display_name') . '</a>' .
					'</span>';
				}*/

				if ($current_tax != '') {
					echo '<span class="cmsmasters_archive_item_category">' .
						esc_html__('in', 'dream-city') . ' ' .
						dream_city_get_the_category_list(get_the_ID(), $current_tax, ', ') .
					'</span>';
				}


			/*	if (get_post_type() == 'post') {
					echo '<span class="cmsmasters_archive_item_date_wrap">' .
						'<abbr class="published cmsmasters_archive_item_date" title="' . esc_attr(get_the_date()) . '">';


							if (cmsmasters_title(get_the_ID(), false) == get_the_ID()) {
								echo '<a href="' . esc_url(get_permalink()) . '">' .
									get_the_date() .
								'</a>';
							} else {
								echo get_the_date();
							}


						echo '</abbr>' .
						'<abbr class="dn date updated" title="' . esc_attr(get_the_modified_date()) . '">' .
							get_the_modified_date() .
						'</abbr>' .
					'</span>';
				}*/

			echo '</footer>';
		}
		?>
		
	</div>
</article>
<?php else : ?>
<!--_________________________ Start Post Default Article _________________________ -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_post_default'); ?>>
	<div class="cmsmasters_post_cont">
		<?php
		if ($date) {
			echo '<div class="cmsmasters_post_info entry-meta">';
			
				$date ? dream_city_get_post_date('page', 'default') : '';
				
			echo '</div>';
		}
		
		
		dream_city_post_heading(get_the_ID(), 'h2');
		
		
		if ($cmsmasters_post_format == 'audio') {
			$cmsmasters_post_audio_links = get_post_meta(get_the_ID(), 'cmsmasters_post_audio_links', true);
			
			dream_city_post_type_audio($cmsmasters_post_audio_links);
		}
		
		
		if ($author || $categories) {
			echo '<div class="cmsmasters_post_cont_info entry-meta">';
				
				if (is_sticky()) {
					echo '<span class="cmsmasters_sticky cmsmasters_theme_icon_sticky">' . esc_html__('Featured Post', 'dream-city') . '</span>';
				}
			
				$author ? dream_city_get_post_author('page') : '';
				
				$categories ? dream_city_get_post_category(get_the_ID(), 'category', 'page') : '';
				
			echo '</div>';
		}
		
		
		if ($cmsmasters_post_format == 'image') {
			$cmsmasters_post_image_link = get_post_meta(get_the_ID(), 'cmsmasters_post_image_link', true);
			
			dream_city_post_type_image(get_the_ID(), $cmsmasters_post_image_link);
		} elseif ($cmsmasters_post_format == 'gallery') {
			$cmsmasters_post_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsmasters_post_images', true))));
			
			dream_city_post_type_slider(get_the_ID(), $cmsmasters_post_images, 'post-thumbnail');
		} elseif ($cmsmasters_post_format == 'video') {
			$cmsmasters_post_video_type = get_post_meta(get_the_ID(), 'cmsmasters_post_video_type', true);
			$cmsmasters_post_video_link = get_post_meta(get_the_ID(), 'cmsmasters_post_video_link', true);
			$cmsmasters_post_video_links = get_post_meta(get_the_ID(), 'cmsmasters_post_video_links', true);
			
			dream_city_post_type_video(get_the_ID(), $cmsmasters_post_video_type, $cmsmasters_post_video_link, $cmsmasters_post_video_links);
		} elseif ($cmsmasters_post_format == '' && !post_password_required() && has_post_thumbnail()) {
			dream_city_thumb(get_the_ID(), 'post-thumbnail', true, false, false, false, false, true, false);
		}
		
		
		dream_city_post_exc_cont();
		
		
		if ($more || $likes || $comments) {
			echo '<footer class="cmsmasters_post_footer entry-meta">';
				
				if ($more) {
					echo '<div class="cmsmasters_post_read_more_wrap">';
					
						$more ? dream_city_post_more(get_the_ID()) : '';
						
					echo '</div>';
				}
				
				
				if ($likes || $comments) {
					echo '<div class="cmsmasters_post_meta_info entry-meta">';
					
						$likes ? dream_city_get_post_likes('page') : '';
						
						$comments ? dream_city_get_post_comments('page') : '';
						
					echo '</div>';
				}
				
			echo '</footer>';
		}
		?>
	</div>
</article>
<?php endif; ?>
<!--_________________________ Finish Post Default Article _________________________ -->
<!--
<div class="cmsmasters_row_margin cmsmasters_11">
<div class="cmsmasters_column one_first firstwords">
<div id="cmsmasters_heading_76f290902e" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
	<h1 class="cmsmasters_heading">Ανακαλύψτε το Δήμο μας</h1>
</div><div id="cmsmasters_heading_d401e63b8c" class="cmsmasters_heading_wrap cmsmasters_heading_align_center">
	<h2 class="cmsmasters_heading">Ψάξτε υπηρεσίες, εκδηλώσεις, πληροφορίες και άλλα…</h2>
</div>
<div class="cmsmasters_sidebar sidebar_layout_11 cmsmasters_custom_search">
<aside id="search-3" class="widget widget_search">
<div class="search_bar_wrap">
    <form method="get" action="http://cpdev.crowdpolicy.com/platanias/search/">
		<p class="search_field">
			<input name="keyword" placeholder="εισάγετε λέξεις κλειδιά..." value="" type="search">
		</p>
		<p class="search_button">
			<button type="submit" class="cmsmasters_theme_icon_search"></button>
		</p>
	</form>
</div>

</aside>
<div class="cl"></div>
</div>
</div>
</div>
-->