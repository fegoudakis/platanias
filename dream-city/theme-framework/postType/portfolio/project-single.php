<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Project Single Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_option = dream_city_get_global_options();


$cmsmasters_project_title = get_post_meta(get_the_ID(), 'cmsmasters_project_title', true);

$cmsmasters_project_features = get_post_meta(get_the_ID(), 'cmsmasters_project_features', true);


$cmsmasters_project_sharing_box = get_post_meta(get_the_ID(), 'cmsmasters_project_sharing_box', true);


$cmsmasters_project_link_text = get_post_meta(get_the_ID(), 'cmsmasters_project_link_text', true);
$cmsmasters_project_link_url = get_post_meta(get_the_ID(), 'cmsmasters_project_link_url', true);
$cmsmasters_project_link_target = get_post_meta(get_the_ID(), 'cmsmasters_project_link_target', true);


$cmsmasters_project_details_title = get_post_meta(get_the_ID(), 'cmsmasters_project_details_title', true);


$cmsmasters_project_features_one_title = get_post_meta(get_the_ID(), 'cmsmasters_project_features_one_title', true);
$cmsmasters_project_features_one = get_post_meta(get_the_ID(), 'cmsmasters_project_features_one', true);

$cmsmasters_project_features_two_title = get_post_meta(get_the_ID(), 'cmsmasters_project_features_two_title', true);
$cmsmasters_project_features_two = get_post_meta(get_the_ID(), 'cmsmasters_project_features_two', true);

$cmsmasters_project_features_three_title = get_post_meta(get_the_ID(), 'cmsmasters_project_features_three_title', true);
$cmsmasters_project_features_three = get_post_meta(get_the_ID(), 'cmsmasters_project_features_three', true);


$cmsmasters_project_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsmasters_project_images', true))));


$project_details = '';

if (
	$cmsmasters_option['dream-city' . '_portfolio_project_like'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_date'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_cat'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_comment'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_author'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_tag'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_link'] || 
	(
		!empty($cmsmasters_project_features[0][0]) && 
		!empty($cmsmasters_project_features[0][1])
	)
) {
	$project_details = 'true';
}


$project_sidebar = '';

if (
	$project_details == 'true' || 
	(
		!empty($cmsmasters_project_features_one[0][0]) && 
		!empty($cmsmasters_project_features_one[0][1])
	) || (
		!empty($cmsmasters_project_features_two[0][0]) && 
		!empty($cmsmasters_project_features_two[0][1])
	) || (
		!empty($cmsmasters_project_features_three[0][0]) && 
		!empty($cmsmasters_project_features_three[0][1])
	)
) {
	$project_sidebar = 'true';
}


$cmsmasters_post_format = get_post_format();

?>



<!--_________________________ Start Project Single Article _________________________ -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_open_project'); ?>>
	<?php
	if (
		(
			!post_password_required() && 
			!(sizeof($cmsmasters_project_images) > 1)
		) || 
		$cmsmasters_post_format != ''
	) {
		if ($cmsmasters_project_title == 'true') {
			echo '<header class="cmsmasters_project_header entry-header">';
				dream_city_project_title_nolink(get_the_ID(), 'h2');
			echo '</header>';
		}
	}
	
	
	echo '<div class="project_content' . 
		(($project_sidebar == 'true') ? ' with_sidebar' : '') . 	
		((!post_password_required() && (sizeof($cmsmasters_project_images) > 1) && $cmsmasters_post_format == '') ? ' project_standard_slider' : '') . 
	'">';
		
		if ($cmsmasters_post_format == 'gallery') {
			$cmsmasters_project_columns = get_post_meta(get_the_ID(), 'cmsmasters_project_columns', true);
			
			dream_city_post_type_gallery(get_the_ID(), $cmsmasters_project_images, $cmsmasters_project_columns, 'cmsmasters-project-thumb', 'cmsmasters-project-full-thumb');
		} elseif ($cmsmasters_post_format == 'video') {
			$cmsmasters_project_video_type = get_post_meta(get_the_ID(), 'cmsmasters_project_video_type', true);
			$cmsmasters_project_video_link = get_post_meta(get_the_ID(), 'cmsmasters_project_video_link', true);
			$cmsmasters_project_video_links = get_post_meta(get_the_ID(), 'cmsmasters_project_video_links', true);
			
			dream_city_post_type_video(get_the_ID(), $cmsmasters_project_video_type, $cmsmasters_project_video_link, $cmsmasters_project_video_links, 'cmsmasters-project-full-thumb');
		} /*elseif ($cmsmasters_post_format == '') {
			$cmsmasters_project_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsmasters_project_images', true))));
			
			dream_city_post_type_slider(get_the_ID(), $cmsmasters_project_images, 'cmsmasters-full-masonry-thumb');
		}
		*/ 
		
		if (!post_password_required() && (sizeof($cmsmasters_project_images) > 1) && $cmsmasters_post_format == '') {
			if ($cmsmasters_project_title == 'true') {
				echo '<header class="cmsmasters_project_header entry-header">';
					dream_city_project_title_nolink(get_the_ID(), 'h2');
				echo '</header>';
			}
		}
		
		
		if (get_the_content() != '') {
			echo '<div class="cmsmasters_project_content entry-content">' . "\n";
				
				the_content();
				
				$rows = get_field('file_repeater', $post->ID);
				if($rows) {	?>
					<br>
					<h4 style="color:#044a85;">Αρχεία</h4>
					<ul id="ul_accordion">
		
			<?php	foreach($rows as $row) { ?>
						<li>	
							<p class="accordion-file"><a target="_blank" href="<?php echo $row['add_file']; ?>"><i class="fa fa-arrow-circle-o-down fa-accordion-file" aria-hidden="true"></i><?php echo $row['file_name']; ?></a></p>	
						
						</li>
			<?php	} ?>
					</ul>
			<?php	}
								
				
				wp_link_pages(array( 
					'before' => '<div class="subpage_nav">' . '<strong>' . esc_html__('Pages', 'dream-city') . ':</strong>', 
					'after' => '</div>', 
					'link_before' => ' [ ', 
					'link_after' => ' ] ' 
				));
				
				
				if ($cmsmasters_project_sharing_box == 'true') {
					dream_city_sharing_box(esc_html__('Like this project?', 'dream-city'), 'h4');
				}
				
			echo '</div><!--asdf-->';
		}
		
		
		if ((!post_password_required()) && (sizeof($cmsmasters_project_images) > 1)  && $cmsmasters_post_format == '') {
			if ($project_sidebar == 'true') {
				echo '<div class="project_sidebar">';
					
					if ($project_details == 'true') {
						echo '<div class="project_details entry-meta">' . 
							'<h3 class="project_details_title">' . esc_html($cmsmasters_project_details_title) . '</h3>';
							
							dream_city_get_project_likes('post');
							
							dream_city_get_project_comments('post');
							
							dream_city_get_project_author('post');
							
							dream_city_get_project_date('post');
							
							dream_city_get_project_category(get_the_ID(), 'pj-categs', 'post');
							
							dream_city_get_project_tags(get_the_ID(), 'pj-tags');
							
							dream_city_get_project_features('details', $cmsmasters_project_features, false, 'h4', true);
							
							dream_city_project_link($cmsmasters_project_link_text, $cmsmasters_project_link_url, $cmsmasters_project_link_target);
							
						echo '</div>';
					}
					
					
					dream_city_get_project_features('features', $cmsmasters_project_features_one, $cmsmasters_project_features_one_title, 'h3', true);
					
					dream_city_get_project_features('features', $cmsmasters_project_features_two, $cmsmasters_project_features_two_title, 'h3', true);
					
					dream_city_get_project_features('features', $cmsmasters_project_features_three, $cmsmasters_project_features_three_title, 'h3', true);
					
				echo '</div>';
			}
		}

	global $post;
    $title = $post->post_name;

	//$title = get_the_title();
	$taxonomy_name = 'pj-categs';

	$terms = wp_get_post_terms($post->ID, $taxonomy_name); 

	$parent_term = $terms[0]->term_taxonomy_id;


	$term_children = get_term_children( $parent_term, $taxonomy_name );
	$names=[];

	foreach ( $term_children as $child ) {
		
		$term = get_term_by( 'id', $child, $taxonomy_name );
		
		if ($term->slug == $title){
			$tax_id = $term->term_id;
		}
		array_push($names,$term->slug);
	}

//$output = preg_replace('#\p{Pd}#u', '-', $names[3]);
//$string = str_replace(array('-','–','-','-'), '–', $names[3]);


	if(in_array($title,$names)){
				
		$args = array(
				'posts_per_page' => -1,
				'post_type' => 'project',
				'orderby' => 'menu_order',
				'order'   => 'ASC',
				'tax_query' => array(
					array(
						'taxonomy' => $taxonomy_name,
						'field'    => 'id',
						'terms'    => $tax_id,
						'include_children' => false,
					),
				),
			);
		$query = new WP_Query( $args );
		
?>

		<div class="accordion-inside-service-content">  <!--style="width:68%"-->
<?php
		if ( $query->have_posts() ) {
			$i = 0;
			$b = 0;
			while ( $query->have_posts() ) {
				$query->the_post();
								
				$subaccordion = $post->post_name;
				++$i;

?>
				<button style="padding: 5px; margin: 5px 0;" class="accordion"><?php echo  get_the_title();?><i class="fa fa-angle-down" aria-hidden="true"></i></button>
				
				<div  class="panel" style="padding: 10px; border-radius: 5px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35);">
					<div id="print-<?php echo $i; ?>"> <!--id ="dvContainer"-->
						<p><?php echo  the_content(); ?></p>
<?php
					
					$rows = get_field('file_repeater', $post->ID);
					if($rows){
?>
						<h4 style="color:#044a85;">Αρχεία</h4>
						<ul id="ul_accordion">
<?php			
						foreach($rows as $row)	{
?>
						<li>	
							<p class="accordion-file"><a target="_blank" href="<?php echo $row['add_file']; ?>"><i class="fa fa-arrow-circle-o-down fa-accordion-file" aria-hidden="true"></i><?php echo $row['file_name']; ?></a></p>	
						
						</li>
<?php					}	
?>
						</ul>
<?php				}
?>
					</div>
					
					<button style="margin-top:30px; float:right;" type="button" onclick="printContent('print-<?php echo $i;?>')" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i></button>
					
<?php
					if (in_array($subaccordion,$names)){
						$param = array(
						'posts_per_page' => -1,
						'post_type' => 'project',
						'orderby' => 'menu_order',
						'order'   => 'ASC',
						'tax_query' => array(
							array(
								'taxonomy' => $taxonomy_name,
								'field'    => 'slug',
								'terms'    => $subaccordion,
								//'include_children' => false,
								),
							),
						);
						$subquery = new WP_Query( $param );					
						
?>
						<!--<div style="padding: 10px; border-radius: 5px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35);">-->
<?php
						if ( $subquery->have_posts() ) {
							
							while ( $subquery->have_posts() ) {
								$subquery->the_post();
								++$b;
?>				
							<button style="padding: 5px; margin: 5px 0;" class="accordion2"><?php echo  get_the_title();?><i class="fa fa-angle-down" aria-hidden="true"></i></button>
							
							<div class="panel2" style="padding: 10px; border-radius: 5px; box-shadow: 0px 0px 10px rgba(0, 0, 0, 0.35);">
								<div id="insideprint-<?php echo $b; ?>">
									<p><?php echo  the_content(); ?> </p>
<?php
								$rows = get_field('file_repeater', $post->ID);
									
									if($rows){
?>
										<h4 style="color:#044a85;">Αρχεία</h4>
										<ul id="ul_accordion">
<?php									
										foreach($rows as $row){
?>
											<li>	
												<p class="accordion-file"><a target="_blank" href="<?php echo $row['add_file']; ?>"><i class="fa fa-arrow-circle-o-down fa-accordion-file" aria-hidden="true"></i><?php echo $row['file_name']; ?></a>
												</p>	
											
											</li>
<?php									}
?>
										</ul>
<?php	
									}
?>
								</div>
							
								<button type="button" onclick="printContent('insideprint-<?php echo $b;?>')" id="btnPrint"><i class="fa fa-print" aria-hidden="true"></i></button>
								
							</div>
<?php 						} 
						}
?>
						
<?php
					} wp_reset_postdata(); 
?>	
			</div>
			<!--</div>-->
			
<?php		} wp_reset_postdata();
		} 
		echo '</div>'; // End of accordion-inside-service-content div
	} 	
	
	echo '</div>';
	
	
	if ($project_sidebar == 'true') {
		if (
			(!post_password_required()) && 
			!(sizeof($cmsmasters_project_images) > 1) ||
			$cmsmasters_post_format != ''
		) {
			echo '<div class="project_sidebar">';
				
				if ($project_details == 'true') {
					echo '<div class="project_details entry-meta">' . 
						'<h3 class="project_details_title">Επιλογές</h3>';

                        dream_city_get_project_likes('post');
						
						dream_city_get_project_comments('post');
						
						dream_city_get_project_author('post');
						
						dream_city_get_project_date('post');
						
						dream_city_get_project_category(get_the_ID(), 'pj-categs', 'post');
						
						dream_city_get_project_tags(get_the_ID(), 'pj-tags');
						
						dream_city_get_project_features('details', $cmsmasters_project_features, false, 'h3', true);
						
						dream_city_project_link($cmsmasters_project_link_text, $cmsmasters_project_link_url, $cmsmasters_project_link_target);

						
						if (get_post_type() == 'project') {
			
								$category = get_the_category();
								
								$the_term = wp_get_post_terms( $post->ID, 'pj-categs' );								
								
								$term = $the_term[0];								
								//echo get_category_parents($the_term[0], true, $sep) . ' <span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
																			
							}
							
						$pages = get_posts(array(
								  'post_type' => 'project',
								  'numberposts' => -1,
								  'tax_query' => array(
									array(
									  'taxonomy' => 'pj-categs',
									  'field' => 'id',
									  'terms' => $term, // Where term_id of Term 1 is "1".
									  'include_children' => false
									)
								  )
								));
							echo '<pre>';	
							//var_dump($pages);
							echo '</pre>';
							foreach ($pages as $page){
								
								$link = $page->guid;
								echo '<li class="page_item"><a href="'.$link.'">'.$page->post_title.'</a></li>';
								
								//echo $link.'<br>';
							}
							
							
							/*$term_id = 118;
                            $taxonomy_name = 'pj-categs';
                            $term_children = get_term_children( $term_id, $taxonomy_name );

                            echo '<ul>';
                            foreach ( $term_children as $child ) {
                                $term = get_term_by( 'id', $child, $taxonomy_name );
                                echo '<li class="page_item"><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
                            }
                            echo '</ul>';*/
/*
                            $term_id = 122 ;
                            $taxonomy_name = 'pj-categs';
                            $term_children = get_term_children( $term_id, $taxonomy_name );

                            echo '<ul>';
                            foreach ( $term_children as $child ) {
                                $term = get_term_by( 'id', $child, $taxonomy_name );
                                echo '<li class="page_item"><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
                            }
                            echo '</ul>';*/
							
						//echo $terms;
//					$args = array(
//						'post_type' => 'project',
//						'post_status' => 'publish',
//						'posts_per_page' => -1,
//						'tax_query' => array(
//							array(
//								'taxonomy' => 'pj-categs',
//								'field' => 'id',
//								'terms' => '122'
//							)
//						)
//					);
//					$the_query = new WP_Query( $args );
//					while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
<!--                    <li class="animal-listing" id="post---><?php //the_ID(); ?><!--">-->
<!--	                     <a href="--><?php //the_permalink(); ?><!--">--><?php //the_title(); ?><!--</a>-->
<!--                    </li>-->
<!--				--><?php
//                    endwhile;


                   // echo do_shortcode('[ratings]');

					echo '</div>';
				}
				
				
				dream_city_get_project_features('features', $cmsmasters_project_features_one, $cmsmasters_project_features_one_title, 'h3', true);
				
				dream_city_get_project_features('features', $cmsmasters_project_features_two, $cmsmasters_project_features_two_title, 'h3', true);
				
				dream_city_get_project_features('features', $cmsmasters_project_features_three, $cmsmasters_project_features_three_title, 'h3', true);
				
			echo '</div>';
		}
	}
	?>
</article>


	
<script>
	
var acc = document.getElementsByClassName("accordion");
var panel = document.getElementsByClassName('panel');

for (var i = 0; i < acc.length; i++) {
    acc[i].onclick = function() {
    	var setClasses = !this.classList.contains('active');
        setClass(acc, 'active', 'remove');
        setClass(panel, 'show', 'remove');
        
       	if (setClasses) {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
}

function setClass(els, className, fnName) {
    for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
    }
}

var acc2 = document.getElementsByClassName("accordion2");
var panel2 = document.getElementsByClassName('panel2');

for (var i = 0; i < acc2.length; i++) {
    acc2[i].onclick = function() {
    	var setClasses2 = !this.classList.contains('active');
        setClass2(acc2, 'active', 'remove');
        setClass2(panel2, 'show', 'remove');
        
       	if (setClasses2) {
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");
        }
    }
}

function setClass2(els, className, fnName) {
    for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
    }
}
	
	
	
/*	
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
	//console.log(acc[i]);
    acc[i].onclick = function(){
        this.classList.toggle("active");
        var panel = this.nextElementSibling;
		console.log(panel);
        if (panel.style.display === "block") {
            panel.style.display = "none";
			
        } else {
            panel.style.display = "block";
			//$( ".panel" ).append( "<button type='button' id='btnPrint'><i class='fa fa-print' aria-hidden='true'></i> Εκτύπωση</button>" );
        }
    }
}
*/
function printContent(el){
	var divContents = $("#" + el).html();
            var printWindow = window.open('', '', 'height=400,width=800');
            //printWindow.document.write('<html><head><title>DIV Contents</title>');
            printWindow.document.write('</head><body >');
            printWindow.document.write(divContents);
            printWindow.document.write('</body></html>');
            printWindow.document.close();
            printWindow.print();
			

}
	/*var restorepage = document.body.innerHTML;
	var printcontent = document.getElementById(el).innerHTML;
	document.body.innerHTML = printcontent;
	window.print();
	document.body.innerHTML = restorepage;*/

</script>
<!--_________________________ Finish Project Single Article _________________________ -->

