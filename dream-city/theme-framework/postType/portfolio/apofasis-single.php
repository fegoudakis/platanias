<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Project Single Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_option = dream_city_get_global_options();


$cmsmasters_project_title = get_post_meta(get_the_ID(), 'cmsmasters_project_title', true);

$cmsmasters_project_features = get_post_meta(get_the_ID(), 'cmsmasters_project_features', true);


$cmsmasters_project_sharing_box = get_post_meta(get_the_ID(), 'cmsmasters_project_sharing_box', true);


$cmsmasters_project_link_text = get_post_meta(get_the_ID(), 'cmsmasters_project_link_text', true);
$cmsmasters_project_link_url = get_post_meta(get_the_ID(), 'cmsmasters_project_link_url', true);
$cmsmasters_project_link_target = get_post_meta(get_the_ID(), 'cmsmasters_project_link_target', true);


$cmsmasters_project_details_title = get_post_meta(get_the_ID(), 'cmsmasters_project_details_title', true);


$cmsmasters_project_features_one_title = get_post_meta(get_the_ID(), 'cmsmasters_project_features_one_title', true);
$cmsmasters_project_features_one = get_post_meta(get_the_ID(), 'cmsmasters_project_features_one', true);

$cmsmasters_project_features_two_title = get_post_meta(get_the_ID(), 'cmsmasters_project_features_two_title', true);
$cmsmasters_project_features_two = get_post_meta(get_the_ID(), 'cmsmasters_project_features_two', true);

$cmsmasters_project_features_three_title = get_post_meta(get_the_ID(), 'cmsmasters_project_features_three_title', true);
$cmsmasters_project_features_three = get_post_meta(get_the_ID(), 'cmsmasters_project_features_three', true);


$cmsmasters_project_images = explode(',', str_replace(' ', '', str_replace('img_', '', get_post_meta(get_the_ID(), 'cmsmasters_project_images', true))));


$project_details = '';

if (
	$cmsmasters_option['dream-city' . '_portfolio_project_like'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_date'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_cat'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_comment'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_author'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_tag'] || 
	$cmsmasters_option['dream-city' . '_portfolio_project_link'] || 
	(
		!empty($cmsmasters_project_features[0][0]) && 
		!empty($cmsmasters_project_features[0][1])
	)
) {
	$project_details = 'true';
}


$project_sidebar = '';

if (
	$project_details == 'true' || 
	(
		!empty($cmsmasters_project_features_one[0][0]) && 
		!empty($cmsmasters_project_features_one[0][1])
	) || (
		!empty($cmsmasters_project_features_two[0][0]) && 
		!empty($cmsmasters_project_features_two[0][1])
	) || (
		!empty($cmsmasters_project_features_three[0][0]) && 
		!empty($cmsmasters_project_features_three[0][1])
	)
) {
	$project_sidebar = 'true';
}


$cmsmasters_post_format = get_post_format();

?>
<!--_________________________ Start Project Single Article _________________________ -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_open_project'); ?>>
	<?php	
	
	echo '<div class="project_content">';
		
		
			echo '<div class="cmsmasters_project_content entry-content">' . "\n";
				$the_epitropous = wp_get_post_terms($post->ID, 'epitropi', array("fields" => "names"));
				the_content();
				echo '<ul class="apoul">';
				echo '<li style="font-weight:bold;">A/A : </li>';
				echo '<li>'.get_field( 'aa' ).'</li>';
				echo '</ul>';
				
				echo '<ul class="apoul">';
				echo '<li style="font-weight:bold;">Τίτλος : </li>';
				echo '<li>'.get_the_title( ).'</li>';
				echo '</ul>';
				
				echo '<ul class="apoul">';
				echo '<li style="font-weight:bold;">Στοιχεία : </li>';
				echo '<li>'.get_field( 'stoixeia' ).'</li>';
				echo '</ul>';
				
				echo '<ul class="apoul">';
				echo '<li style="font-weight:bold;">Επιτρόπη : </li>';
				echo '<li>'.$the_epitropous[0].'</li>';
				echo '</ul>';
				
				echo '<ul class="apoul">';
				echo '<li style="font-weight:bold;">Έτος : </li>';
				echo '<li>'.get_field( 'year' ).'</li>';
				echo '</ul>';
				
				echo '<div>';
				echo '<a href="http://46.4.136.58/platanias/'.get_field( 'arxeio' ).'">Προβολή Αρχείου</a>';
				echo '</div>';
				
				if (get_field( 'ada' )) {
					
					echo '<div>'; ?>
					
					
					<a href='https://et.diavgeia.gov.gr/search?advanced=true&query=ada:"<?php echo get_field( 'ada' ); ?>"&page=0'>Προβολή ΑΔΑ ΔΥΑΥΓΕΙΑ</a>

				<?php
					echo '</div>';
					
				}
				
				wp_link_pages(array( 
					'before' => '<div class="subpage_nav">' . '<strong>' . esc_html__('Pages', 'dream-city') . ':</strong>', 
					'after' => '</div>', 
					'link_before' => ' [ ', 
					'link_after' => ' ] ' 
				));
				//https://et.diavgeia.gov.gr/search?advanced=true&query=ada:"dwrr"&page=0    ada
				
				if ($cmsmasters_project_sharing_box == 'true') {
					dream_city_sharing_box(esc_html__('Like this project?', 'dream-city'), 'h4');
				}
				
			echo '</div>';

		
	echo '</div>';
	
	
	/*if ($project_sidebar == 'true') {
		if (
			(!post_password_required()) && 
			!(sizeof($cmsmasters_project_images) > 1) ||
			$cmsmasters_post_format != ''
		) {
			echo '<div class="project_sidebar">';
				
				if ($project_details == 'true') {
					echo '<div class="project_details entry-meta">' . 
						'<h3 class="project_details_title">Επιλογές</h3>';

                        dream_city_get_project_likes('post');
						
						dream_city_get_project_comments('post');
						
						dream_city_get_project_author('post');
						
						dream_city_get_project_date('post');
						
						dream_city_get_project_category(get_the_ID(), 'pj-categs', 'post');
						
						dream_city_get_project_tags(get_the_ID(), 'pj-tags');
						
						dream_city_get_project_features('details', $cmsmasters_project_features, false, 'h3', true);
						
						dream_city_project_link($cmsmasters_project_link_text, $cmsmasters_project_link_url, $cmsmasters_project_link_target);

							
                            $term_id = 118;
                            $taxonomy_name = 'pj-categs';
                            $term_children = get_term_children( $term_id, $taxonomy_name );

                            echo '<ul>';
                            foreach ( $term_children as $child ) {
                                $term = get_term_by( 'id', $child, $taxonomy_name );
                                echo '<li class="page_item"><a href="' . get_term_link( $child, $taxonomy_name ) . '">' . $term->name . '</a></li>';
                            }
                            echo '</ul>';

								<?php


					echo '</div>';
				}
				
				
				dream_city_get_project_features('features', $cmsmasters_project_features_one, $cmsmasters_project_features_one_title, 'h3', true);
				
				dream_city_get_project_features('features', $cmsmasters_project_features_two, $cmsmasters_project_features_two_title, 'h3', true);
				
				dream_city_get_project_features('features', $cmsmasters_project_features_three, $cmsmasters_project_features_three_title, 'h3', true);
				
			echo '</div>';
		}
	}*/
	?>
</article>
<!--_________________________ Finish Project Single Article _________________________ -->

