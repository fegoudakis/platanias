<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Posts Slider Post Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_metadata = explode(',', $cmsmasters_post_metadata);


$title = in_array('title', $cmsmasters_metadata) ? true : false;
$excerpt = (in_array('excerpt', $cmsmasters_metadata) && dream_city_slider_post_check_exc_cont('post')) ? true : false;
$date = in_array('date', $cmsmasters_metadata) ? true : false;
$categories = (get_the_category() && (in_array('categories', $cmsmasters_metadata))) ? true : false;
$author = in_array('author', $cmsmasters_metadata) ? true : false;
$comments = (comments_open() && (in_array('comments', $cmsmasters_metadata))) ? true : false;
$likes = in_array('likes', $cmsmasters_metadata) ? true : false;
$more = in_array('more', $cmsmasters_metadata) ? true : false;


$cmsmasters_post_format = get_post_format();

?>
<!--_________________________ Start Posts Slider Post Article _________________________ -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_slider_post'); ?>>
	<div class="cmsmasters_slider_post_outer">
	<?php
		echo '<div class="cmsmasters_slider_post_img_wrap">';
		
			dream_city_thumb_rollover(get_the_ID(), 'cmsmasters-blog-masonry-thumb', false, true, false, false, false, false, false, false, true, false, false);
			
			if ($likes || $comments) {
				echo '<div class="cmsmasters_slider_post_meta_info entry-meta">' . 
					'<div class="cmsmasters_slider_post_meta_info_inner">';
				
					$likes ? dream_city_slider_post_like('post') : '';
					
					$comments ? dream_city_get_slider_post_comments('post') : '';
					
				echo '</div>' . 
				'</div>';
			}
			
		echo '</div>';
		
		
		if ($date || $title || $excerpt || $more || $author || $categories || $likes || $comments) {
			echo '<div class="cmsmasters_slider_post_inner">';
				
				if ($author || $categories) {
					echo '<div class="cmsmasters_slider_post_cont_info entry-meta">';
						
						$categories ? dream_city_get_slider_post_category(get_the_ID(), 'category', 'post') : '';
					
						$author ? dream_city_get_slider_post_author('post') : '';
						
					echo '</div>';
				}
				
				$title ? dream_city_slider_post_heading(get_the_ID(), 'post', 'h3') : '';
				
				$excerpt ? dream_city_slider_post_exc_cont('post') : '';
				
				if ($date || $more) {
					echo '<footer class="cmsmasters_slider_post_footer entry-meta">';
						
						$date ? dream_city_get_slider_post_date('post') : '';
						
						$more ? dream_city_slider_post_more(get_the_ID()) : '';
						
					echo '</footer>';
				}
				
			echo '</div>';
		}
	?>
	</div>
</article>
<!--_________________________ Finish Posts Slider Post Article _________________________ -->

