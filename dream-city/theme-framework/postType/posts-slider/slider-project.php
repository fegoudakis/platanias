<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Posts Slider Project Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_metadata = explode(',', $cmsmasters_project_metadata);


$title = in_array('title', $cmsmasters_metadata) ? true : false;
$excerpt = (in_array('excerpt', $cmsmasters_metadata) && dream_city_slider_post_check_exc_cont('project')) ? true : false;
$categories = (get_the_terms(get_the_ID(), 'pj-categs') && in_array('categories', $cmsmasters_metadata)) ? true : false;
$comments = (comments_open() && in_array('comments', $cmsmasters_metadata)) ? true : false;
$likes = in_array('likes', $cmsmasters_metadata) ? true : false;
$more = in_array('more', $cmsmasters_metadata) ? true : false;


$cmsmasters_project_link_url = get_post_meta(get_the_ID(), 'cmsmasters_project_link_url', true);
$cmsmasters_project_link_redirect = get_post_meta(get_the_ID(), 'cmsmasters_project_link_redirect', true);


$cmsmasters_post_format = get_post_format();

?>
<!--_________________________ Start Posts Slider Project Article _________________________ -->

<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_slider_project'); ?>>
	<div class="cmsmasters_slider_project_outer">
	<?php
		echo '<div class="cmsmasters_slider_project_img_wrap">';
	
			dream_city_thumb_rollover(get_the_ID(), 'cmsmasters-project-slider-thumb', false, false, false, false, false, false, false, false, true, $cmsmasters_project_link_redirect, $cmsmasters_project_link_url);
			
			if ($likes || $comments) {
				echo '<footer class="cmsmasters_slider_project_footer entry-meta">' . 
					'<div class="cmsmasters_slider_project_footer_inner">';
				
						($likes) ? dream_city_slider_post_like('project') : '';
						
						($comments) ? dream_city_get_slider_post_comments('project') : '';
						
				echo '</div>' . 
				'</footer>';
			}
			
		echo '</div>';
		
		
		if ($categories || $excerpt || $title) {
			echo '<div class="cmsmasters_slider_project_inner">';
				
				$title ? dream_city_slider_post_heading(get_the_ID(), 'project', 'h2', $cmsmasters_project_link_redirect, $cmsmasters_project_link_url) : '';
				
				if ($categories) {
					echo '<div class="cmsmasters_slider_project_cont_info entry-meta">';
					
						dream_city_get_slider_post_category(get_the_ID(), 'pj-categs', 'project');
						
					echo '</div>';
				}
				
				$excerpt ? dream_city_slider_post_exc_cont('project') : '';
				
				$more ? dream_city_slider_post_more(get_the_ID()) : '';
				
			echo '</div>';
		}
	?>
	</div>
</article>
<!--_________________________ Finish Posts Slider Project Article _________________________ -->

