<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.1
 * 
 * Profile Single Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_option = dream_city_get_global_options();


$cmsmasters_profile_title = get_post_meta(get_the_ID(), 'cmsmasters_profile_title', true);

$cmsmasters_profile_subtitle = get_post_meta(get_the_ID(), 'cmsmasters_profile_subtitle', true);

$cmsmasters_profile_features = get_post_meta(get_the_ID(), 'cmsmasters_profile_features', true);

$cmsmasters_profile_social = get_post_meta(get_the_ID(), 'cmsmasters_profile_social', true);


$cmsmasters_profile_details_title = get_post_meta(get_the_ID(), 'cmsmasters_profile_details_title', true);

$cmsmasters_profile_contact_info_block_title = get_post_meta(get_the_ID(), 'cmsmasters_profile_contact_info_block_title', true);

$cmsmasters_profile_contact_info = get_post_meta(get_the_ID(), 'cmsmasters_profile_contact_info', true);

$cmsmasters_profile_sharing_box = get_post_meta(get_the_ID(), 'cmsmasters_profile_sharing_box', true);


$cmsmasters_profile_features_one_title = get_post_meta(get_the_ID(), 'cmsmasters_profile_features_one_title', true);
$cmsmasters_profile_features_one = get_post_meta(get_the_ID(), 'cmsmasters_profile_features_one', true);

$cmsmasters_profile_features_two_title = get_post_meta(get_the_ID(), 'cmsmasters_profile_features_two_title', true);
$cmsmasters_profile_features_two = get_post_meta(get_the_ID(), 'cmsmasters_profile_features_two', true);

$cmsmasters_profile_features_three_title = get_post_meta(get_the_ID(), 'cmsmasters_profile_features_three_title', true);
$cmsmasters_profile_features_three = get_post_meta(get_the_ID(), 'cmsmasters_profile_features_three', true);


$profile_details = '';

if (
	$cmsmasters_option['dream-city' . '_profile_post_cat'] || 
	$cmsmasters_option['dream-city' . '_profile_post_comment'] || 
	(
		!empty($cmsmasters_profile_features[0][0]) && 
		!empty($cmsmasters_profile_features[0][1])
	)
) {
	$profile_details = 'true';
}


$profile_sidebar = '';

if (
	$profile_details == 'true' || 
	$cmsmasters_profile_social != '' || 
	(
		!empty($cmsmasters_profile_features_one[0][0]) && 
		!empty($cmsmasters_profile_features_one[0][1])
	) || (
		!empty($cmsmasters_profile_features_two[0][0]) && 
		!empty($cmsmasters_profile_features_two[0][1])
	) || (
		!empty($cmsmasters_profile_features_three[0][0]) && 
		!empty($cmsmasters_profile_features_three[0][1])
	)
) {
	$profile_sidebar = 'true';
}

?>
<!--_________________________ Start Profile Single Article _________________________ -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_open_profile'); ?>>
	<?php
	if ($cmsmasters_profile_title == 'true') {
		echo '<header class="cmsmasters_profile_header entry-header">';
			dream_city_profile_title_nolink(get_the_ID(), 'h1', $cmsmasters_profile_subtitle, 'h5');
		echo '</header>';
	}
	
	
	echo '<div class="profile_content' . (($profile_sidebar == 'true') ? ' with_sidebar' : '') . '">';
		
		if (get_the_content() != '') {
			echo '<div class="cmsmasters_profile_content entry-content">' . "\n";
				
				the_content();
				
				
					$rows = get_field('file_repeater');
					if($rows)
					{
				?>
						<h4 style="color:#044a85;">Αρχεία</h4>
						<ul id="ul_accordion">
				<?php		foreach($rows as $row)
						{
				?>
						<li>	
							<p class="accordion-file"><a target="_blank" href="<?php echo $row['add_file']; ?>"><i class="fa fa-arrow-circle-o-down fa-accordion-file" aria-hidden="true"></i><?php echo $row['file_name']; ?></a></p>	
						
						</li>
				<?php		}
				?>
						</ul>
				<?php	
					}
				
				
				wp_link_pages(array( 
					'before' => '<div class="subpage_nav">' . '<strong>' . esc_html__('Pages', 'dream-city') . ':</strong>', 
					'after' => '</div>', 
					'link_before' => ' [ ', 
					'link_after' => ' ] ' 
				));
	
	
				if ($cmsmasters_profile_sharing_box == 'true') {
					dream_city_sharing_box(esc_html__('Like this profile?', 'dream-city'), 'h4');
				}
				
				
			echo '</div>';
		}
		
	echo '</div>';
	
	
	if ($profile_sidebar == 'true') {
		echo '<div class="profile_sidebar">';
		
			if ($profile_details == 'true') {
				echo '<div class="profile_details entry-meta">' . 
					'<h3 class="profile_details_title">' . esc_html($cmsmasters_profile_details_title) . '</h3>';
					
					dream_city_get_profile_likes('post');
					
					dream_city_get_profile_comments('post');
					
				//	dream_city_get_profile_features('details', $cmsmasters_profile_features, false, 'h3', true);
					
					dream_city_get_profile_category(get_the_ID(), 'pl-categs', 'post');
					
				echo '</div>';
			}
			
			
			dream_city_get_profile_features('features', $cmsmasters_profile_features_one, $cmsmasters_profile_features_one_title, 'h3', true);
			
			dream_city_get_profile_features('features', $cmsmasters_profile_features_two, $cmsmasters_profile_features_two_title, 'h3', true);
			
			dream_city_get_profile_features('features', $cmsmasters_profile_features_three, $cmsmasters_profile_features_three_title, 'h3', true);
			
			dream_city_get_profile_contact_info($cmsmasters_profile_contact_info, $cmsmasters_profile_contact_info_block_title, 'h3', 'post');
			
			
			dream_city_profile_open_social_icons($cmsmasters_profile_social, get_the_ID(), esc_html__('Socials', 'dream-city'), 'h3');
		?>	
			


<?php $profile_id = get_the_ID(); 

if($profile_id == 9427){ ?>	
<hr>	
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
		<ul>
			<a href="http://beta.platanias.gr/profile/gavriil-kouris/" ><li>Γενικός Γραμματέας</li></a>
			<a href="http://beta.platanias.gr/diikisi/antidimarchi-dimou-platania/"><li>Αντιδήμαρχοι</li></a>
			<a href="http://beta.platanias.gr/diikisi/entetalmeni-dimotiki-symvouli/"><li>Εντεταλμένοι Δημοτικοί Σύμβουλοι</li></a>
			<a href="http://beta.platanias.gr/diikisi/symvoulia-dimotikon-topikon-kinotiton/"><li>Συμβούλια Δημοτικών / Τοπικών Κοινοτήτων</li></a>	
		</ul>		
	</div>
	
<?php	
}elseif($profile_id == 71744){ ?>
<hr>	
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
		<ul>
			<a href="http://beta.platanias.gr/profile/giannhsmalandrakhs2/" ><li>Δήμαρχος Δήμου Πλατανιά</li></a>
			<a href="http://beta.platanias.gr/diikisi/antidimarchi-dimou-platania/"><li>Αντιδήμαρχοι</li></a>
			<a href="http://beta.platanias.gr/diikisi/entetalmeni-dimotiki-symvouli/"><li>Εντεταλμένοι Δημοτικοί Σύμβουλοι</li></a>
			<a href="http://beta.platanias.gr/diikisi/symvoulia-dimotikon-topikon-kinotiton/"><li>Συμβούλια Δημοτικών / Τοπικών Κοινοτήτων</li></a>	
		</ul>	
	</div>
<?php } ?>

			
			
	<?php		
		echo '</div>';
	}
	?>
</article>
<!--_________________________ Finish Profile Single Article _________________________ -->

