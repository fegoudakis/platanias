<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.1
 * 
 * Profile Vertical Template
 * Created by CMSMasters
 * 
 */


$cmsmasters_profile_contact_info = get_post_meta(get_the_ID(), 'cmsmasters_profile_contact_info', true);

$cmsmasters_profile_social = get_post_meta(get_the_ID(), 'cmsmasters_profile_social', true);

$cmsmasters_profile_subtitle = get_post_meta(get_the_ID(), 'cmsmasters_profile_subtitle', true);

?>
<!--_________________________ Start Profile Vertical Article _________________________ -->
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_profile_vertical'); ?>>
	<div class="profile_outer">
		<div class="profile_inner">
		<?php
			if (has_post_thumbnail()) {
				echo '<div class="pl_img_wrap">';
				
					dream_city_thumb(get_the_ID(), 'cmsmasters-square-thumb', true, false, false, false, false);
					
					dream_city_profile_social_icons($cmsmasters_profile_social, $profile_id);
					
				echo '</div>';
			}
			
			echo '<div class="cmsmasters_profile_header_wrap">';
				
				dream_city_profile_heading(get_the_ID(), 'vertical', 'h3', $cmsmasters_profile_subtitle, 'h6');
				
				if (!has_post_thumbnail()) {
					dream_city_profile_social_icons($cmsmasters_profile_social);
				}
				
				dream_city_get_profile_contact_info($cmsmasters_profile_contact_info, false, 'h3');
				
			echo '</div>';
			
			dream_city_profile_exc_cont('vertical');
		?>
		</div>
	</div>
</article>
<!--_________________________ Finish Profile Vertical Article _________________________ -->

