<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 *
 * Archive Template
 * Created by CMSMasters
 *
 */


$current_tax = '';

$current_tax .= (has_term('category') ? 'category' : '');
$current_tax .= (has_term('pj-categs') ? 'pj-categs' : '');
$current_tax .= (has_term('product_cat') ? 'product_cat' : '');
$current_tax .= (has_term('tribe_events_cat') ? 'tribe_events_cat' : '');
$current_tax .= (has_term('cp-categs') ? 'cp-categs' : '');
$current_tax .= (has_term('events_category') ? 'events_category' : '');

?>

<?php
//echo get_post_type();
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('cmsmasters_archive_type'); ?>>
	<?php
if(!is_category( '101' )){	//id101 for prokyryxi-proslipsis 
	if (!post_password_required() && has_post_thumbnail()) {
		echo '<div class="cmsmasters_archive_item_img_wrap">';
			dream_city_thumb(get_the_ID(), 'cmsmasters-project-slider-thumb');
			//dream_city_thumb(get_the_ID(), 'cmsmasters-square-thumb');
		echo '</div>';
	}
}
	?>
	<div class="cmsmasters_archive_item_cont_wrap">
	
		
		<?php if(get_post_type() == 'project'): ?>
			<div class="cmsmasters_archive_item_type">
				<?php
				$post_type_obj = get_post_type_object(get_post_type());
				$the_term = wp_get_post_terms( $post->ID, 'pj-categs' );
				echo '<span>' . $the_term[0]->name . '</span>';
				?>
			</div>
		<?php

		if (cmsmasters_title(get_the_ID(), false) != get_the_ID()) {
			?>
			<header class="cmsmasters_archive_item_header entry-header">
				<h2 class="cmsmasters_archive_item_title entry-title">
					<a href="<?php the_permalink(); ?>">
						<?php cmsmasters_title(get_the_ID(), true); ?>
					</a>
				</h2>
			</header>
			<?php
		}


		if (theme_excerpt(55, false) != '') {
			echo cmsmasters_divpdel('<div class="cmsmasters_archive_item_content entry-content">' . "\n" .
				wpautop(theme_excerpt(55, false)) .
			'</div>' . "\n");
		} ?>
		
		
	
		
		<?php else: ?>
		
		
		
		
		
		<div class="cmsmasters_archive_item_type">
			<?php
			$post_type_obj = get_post_type_object(get_post_type());				

			//echo '<span>' . $post_type_obj->labels->singular_name . '</span>'; //Display post type
			echo '<span>' . get_the_category(get_the_ID())[0]->name . '</span>'; //Display category
			
			
			if (get_post_type() == 'post') {
				echo '<span class="cmsmasters_archive_item_date_wrap">' .
					'<abbr class="published cmsmasters_archive_item_date" title="' . esc_attr(get_the_date()) . '">';


						if (cmsmasters_title(get_the_ID(), false) == get_the_ID()) {
							echo '<a href="' . esc_url(get_permalink()) . '">' .
								get_the_date() .
							'</a>';
						} else {
							echo get_the_date();
						}


					echo '</abbr>' .
					'<abbr class="dn date updated" title="' . esc_attr(get_the_modified_date()) . '">' .
						get_the_modified_date() .
					'</abbr>' .
				'</span>';
			}

			?>
		</div>
		<?php

		if (cmsmasters_title(get_the_ID(), false) != get_the_ID()) {
			?>
			<header class="cmsmasters_archive_item_header entry-header">
				<h2 class="cmsmasters_archive_item_title entry-title">
					<a href="<?php the_permalink(); ?>">
						<?php cmsmasters_title(get_the_ID(), true); ?>
					</a>
				</h2>
			</header>
			<?php
		}


		if (theme_excerpt(55, false) != '') {
			echo cmsmasters_divpdel('<div class="cmsmasters_archive_item_content entry-content">' . "\n" .
				wpautop(theme_excerpt(55, false)) .
			'</div>' . "\n");
		} ?>

<?php
		if (get_post_type() == 'post' || $current_tax != '') {
			echo '<footer class="cmsmasters_archive_item_info entry-meta">';

			/*	if (get_post_type() == 'post') {
					echo '<span class="cmsmasters_archive_item_user_name">' .
						esc_html__('by', 'dream-city') . ' ' .
						'<a href="' . esc_url(get_author_posts_url(get_the_author_meta('ID'))) . '" rel="author" title="' . esc_attr__('Posts by', 'dream-city') . ' ' . get_the_author_meta('display_name') . '">' . get_the_author_meta('display_name') . '</a>' .
					'</span>';
				}*/

				if ($current_tax != '') {
					echo '<span class="cmsmasters_archive_item_category">' .
						esc_html__('in', 'dream-city') . ' ' .
						dream_city_get_the_category_list(get_the_ID(), $current_tax, ', ') .
					'</span>';
				}


				/*if (get_post_type() == 'post') {
					echo '<span class="cmsmasters_archive_item_date_wrap">' .
						'<abbr class="published cmsmasters_archive_item_date" title="' . esc_attr(get_the_date()) . '">';


							if (cmsmasters_title(get_the_ID(), false) == get_the_ID()) {
								echo '<a href="' . esc_url(get_permalink()) . '">' .
									get_the_date() .
								'</a>';
							} else {
								echo get_the_date();
							}


						echo '</abbr>' .
						'<abbr class="dn date updated" title="' . esc_attr(get_the_modified_date()) . '">' .
							get_the_modified_date() .
						'</abbr>' .
					'</span>';
				}*/

			echo '</footer>';
		}
		?>
		<?php endif; ?>
	</div>
</article>