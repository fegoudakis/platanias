/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Theme Scripts
 * Created by CMSMasters
 * 
 */


jQuery(document).ready(function() { 
	"use strict";
	
	(function ($) {
		$('.touch .pl_img_wrap').on('click', function() { 
		
			return false;
			
		} );
	} )(jQuery);
	
	
	
	/* Header Top Hide Toggle */
	(function ($) { 
		$('.header_top_but').on('click', function () { 
			var headerTopBut = $(this), 
				headerTopButArrow = headerTopBut.find('> span'), 
				headerTopOuter = headerTopBut.parents('.header_top').find('.header_top_outer');
			
			if (headerTopBut.hasClass('opened')) {
				headerTopOuter.slideUp();
				
				headerTopButArrow.removeClass('cmsmasters_theme_icon_slide_top').addClass('cmsmasters_theme_icon_slide_bottom');
				
				headerTopBut.removeClass('opened').addClass('closed');
			} else if (headerTopBut.hasClass('closed')) {
				headerTopOuter.slideDown();
				
				headerTopButArrow.removeClass('cmsmasters_theme_icon_slide_bottom').addClass('cmsmasters_theme_icon_slide_top');
				
				headerTopBut.removeClass('closed').addClass('opened');
			}
		} );
	} )(jQuery);
	
	
	
	/* Header Search Form */
	(function ($) { 
		$('.cmsmasters_header_search_but').on('click', function () { 
			$('.cmsmasters_header_search_form').addClass('cmsmasters_show');
			
			$('.cmsmasters_header_search_form').find('input[type=search]').focus();
		} );
		
		
		$('.cmsmasters_header_search_form_close').on('click', function () { 
			$('.cmsmasters_header_search_form').removeClass('cmsmasters_show');
		} );
	} )(jQuery);
	
	
	
	/* Stats Run */
	(function ($) { 
		if ( 
			!checker.os.iphone && 
			!checker.os.ipod && 
			!checker.os.ipad && 
			!checker.os.blackberry && 
			!checker.os.android && 
			!checker.ua.ie9 
		) {
			$('.cmsmasters_stats.stats_mode_circles').waypoint(function () { 
				var i = 1;
				
				
				$(this).find('.cmsmasters_stat').each(function () { 
					var el = $(this);
					
					
					setTimeout(function () { 
						el.easyPieChart( { 
							size : 			180, 
							lineWidth : 	5, 
							lineCap : 		'square', 
							animate : 		1000, 
							scaleColor : 	false, 
							trackColor : 	false, 
							barColor : function () { 
								return ($(this.el).data('bar-color')) ? $(this.el).data('bar-color') : cmsmasters_script.primary_color;
							}, 
							onStep : function (from, to, val) { 
								$(this.el).find('.cmsmasters_stat_counter').text(~~val);
							} 
						} );
					}, 500 * i);
					
					
					i += 1;
				} );
			}, { 
				offset : 		'100%' 
			} );
		} else {
			$('.cmsmasters_stats.stats_mode_circles').find('.cmsmasters_stat').easyPieChart( { 
				size : 			180, 
				lineWidth : 	5, 
				lineCap : 		'square', 
				animate : 		1000, 
				scaleColor : 	false, 
				trackColor : 	false, 
				barColor : function () { 
					return ($(this.el).data('bar-color')) ? $(this.el).data('bar-color') : cmsmasters_script.primary_color;
				}, 
				onStep : function (from, to, val) { 
					$(this.el).find('.cmsmasters_stat_counter').text(~~val);
				} 
			} );
		}
		
		
		if ( 
			!checker.os.iphone && 
			!checker.os.ipod && 
			!checker.os.ipad && 
			!checker.os.blackberry && 
			!checker.os.android && 
			!checker.ua.ie9 
		) {
			$('.cmsmasters_counters').waypoint(function () { 
				var i = 1;
				
				
				$(this).find('.cmsmasters_counter').each(function () { 
					var el = $(this);
					
					
					setTimeout(function () { 
						el.easyPieChart( { 
							size : 			140, 
							lineWidth : 	0, 
							lineCap : 		'square', 
							animate : 		1500, 
							scaleColor : 	false, 
							trackColor : 	false, 
							barColor : 		'#ffffff', 
							onStep : function (from, to, val) { 
								$(this.el).find('.cmsmasters_counter_counter').text(~~val);
							} 
						} );
					}, 500 * i);
					
					
					i += 1;
				} );
			}, { 
				offset : 		'100%' 
			} );
		} else {
			$('.cmsmasters_counters').find('.cmsmasters_counter').easyPieChart( { 
				size : 			140, 
				lineWidth : 	0, 
				lineCap : 		'square', 
				animate : 		1500, 
				scaleColor : 	false, 
				trackColor : 	false, 
				barColor : 		'#ffffff', 
				onStep : function (from, to, val) { 
					$(this.el).find('.cmsmasters_counter_counter').text(~~val);
				} 
			} );
		}
		
		
		if ( 
			!checker.os.iphone && 
			!checker.os.ipod && 
			!checker.os.ipad && 
			!checker.os.blackberry && 
			!checker.os.android && 
			!checker.ua.ie9 
		) {
			$('.cmsmasters_stats.stats_mode_bars').waypoint(function () { 
				$(this).addClass('shortcode_animated').find('.cmsmasters_stat').each(function () { 
					var el = $(this);
					
					
					el.easyPieChart( { 
						size : 			140, 
						lineWidth : 	0, 
						lineCap : 		'square', 
						animate : 		1500, 
						scaleColor : 	false, 
						trackColor : 	false, 
						barColor : 		'#ffffff', 
						onStep : function (from, to, val) { 
							$(this.el).find('.cmsmasters_stat_counter').text(~~val);
						} 
					} );
				} );
			}, { 
				offset : 		'100%' 
			} );
		} else {
			$('.cmsmasters_stats.stats_mode_bars').addClass('shortcode_animated').find('.cmsmasters_stat').easyPieChart( { 
				size : 			140, 
				lineWidth : 	0, 
				lineCap : 		'square', 
				animate : 		1500, 
				scaleColor : 	false, 
				trackColor : 	false, 
				barColor : 		'#ffffff', 
				onStep : function (from, to, val) { 
					$(this.el).find('.cmsmasters_stat_counter').text(~~val);
				} 
			} );
		}
	} )(jQuery);
} );



/*!
 * Fixed Header Function
 */
(function(e){"use strict";e.fn.cmsmastersFixedHeaderScroll=function(t){var n={headerTop:".header_top",headerMid:".header_mid",headerBot:".header_bot",navBlock:"nav",navList:"#navigation",navTopList:"#top_line_nav",respNavButton:".responsive_nav",respTopNavButton:".responsive_top_nav",fixedClass:".fixed_header",fixedClassBlock:"#page",respHideBlocks:"",maxWidthMid:checker.ua.safari&&!checker.ua.chrome?1024:1007,maxWidthBot:checker.ua.safari&&!checker.ua.chrome?1024:1007,changeTopHeight:true,changeMidHeight:true,mobileDisabled:true},r=this,i={},s=i;i={init:function(){i.options=i.o=e.extend({},n,t);i.el=r;i.vars=i.v={};i.v.newTopHeight=0;i.v.newMidHeight=0;i.setHeaderVars();i.startHeader()},setHeaderVars:function(){i.v.headerMidString=i.o.headerMid;i.v.headerTop=i.el.find("> "+i.o.headerTop);i.v.headerMid=i.el.find("> "+i.v.headerMidString);i.v.headerBot=i.el.find("> "+i.o.headerBot);i.v.respNavButton=i.el.find(i.o.respNavButton);i.v.respTopNavButton=i.el.find(i.o.respTopNavButton);i.v.respHideBlocks=e(i.o.respHideBlocks);i.v.navListString=i.o.navList;i.v.navTopListString=i.o.navTopList;i.v.navBlockString=i.o.navBlock;i.v.navBlock=i.el.find(i.v.navListString).parents(i.v.navBlockString);i.v.navTopBlock=i.el.find(i.v.navTopListString).parents(i.v.navBlockString);i.v.midChangeHeightBlocks=e(i.v.headerMidString);i.v.midChangeHeightBlocksResp=e(i.v.headerMidString);i.v.topHeight=0;i.v.midHeight=i.v.headerMid.attr("data-height");i.v.win=e(window);i.v.winScrollTop=i.v.win.scrollTop();i.v.winMidScrollTop=i.v.winScrollTop-i.v.topHeight;i.v.isMobile="ontouchstart"in document.documentElement;i.v.mobileDisabled=i.v.win.width()<i.o.maxWidthMid?true:false},startHeader:function(){if(i.v.headerTop.length>0){i.v.topHeight=i.v.headerTop.attr("data-height")}i.attachEvents();i.v.win.trigger("scroll")},attachEvents:function(){i.v.respNavButton.bind("click",function(){if(i.v.respNavButton.is(":not(.active)")){i.v.navBlock.css({display:"block"});i.v.respHideBlocks.css({display:"none"});i.v.respNavButton.addClass("active")}else{i.v.navBlock.css({display:"none"});i.v.respHideBlocks.css({display:"block"});i.v.respNavButton.removeClass("active")}return false});i.v.respTopNavButton.bind("click",function(){if(i.v.respTopNavButton.is(":not(.active)")){i.v.navTopBlock.css({display:"block"});i.v.respHideBlocks.css({display:"none"});i.v.respTopNavButton.addClass("active")}else{i.v.navTopBlock.css({display:"none"});i.v.respHideBlocks.css({display:"block"});i.v.respTopNavButton.removeClass("active")}return false});i.v.win.bind("scroll",function(){if(i.el.parents(i.o.fixedClassBlock).is(":not("+i.o.fixedClass+")")||i.v.mobileDisabled&&i.v.isMobile){return false}if(i.v.win.width()>i.o.maxWidthMid){i.getScrollTop();i.headerTransform()}});i.v.win.bind("resize",function(){if(i.el.parents(i.o.fixedClassBlock).is(":not("+i.o.fixedClass+")")||i.v.mobileDisabled&&i.v.isMobile){return false}if(i.v.headerBot.length>0){i.headerResize(i.o.maxWidthBot)}else{i.headerResize(i.o.maxWidthMid)}})},getScrollTop:function(){i.v.winScrollTop=i.v.win.scrollTop();i.v.winMidScrollTop=i.v.winScrollTop-i.v.topHeight},headerTransform:function(){if(i.v.winScrollTop<i.v.topHeight){i.v.headerMid.removeClass('header_mid_scroll');i.v.headerBot.removeClass('header_bot_scroll');i.v.newTopHeight=i.v.topHeight-i.v.winScrollTop;i.v.headerTop.css({overflow:"hidden",height:i.v.newTopHeight+"px"});if(i.v.winScrollTop<=3){i.v.headerTop.css({overflow:"inherit"})}i.v.midChangeHeightBlocks.css({height:i.v.midHeight+"px"});}else{i.v.headerTop.css({overflow:"hidden",height:0});if(i.v.winMidScrollTop<i.v.midHeight/3){i.v.headerMid.removeClass('header_mid_scroll');i.v.headerBot.removeClass('header_bot_scroll');i.v.newMidHeight=i.v.midHeight-i.v.winMidScrollTop}else{i.v.headerMid.addClass('header_mid_scroll');i.v.headerBot.addClass('header_bot_scroll');i.v.newMidHeight=i.v.midHeight/1.5}i.v.midChangeHeightBlocks.css({height:i.v.newMidHeight+"px"});}},headerResize:function(e){if(i.v.win.width()>e){i.v.navBlock.removeAttr("style");i.v.respHideBlocks.removeAttr("style");i.v.respNavButton.removeClass("active");i.getScrollTop();i.headerTransform()}else{i.v.headerTop.removeAttr("style");i.v.midChangeHeightBlocksResp.css("height","auto")}}};i.init()}})(jQuery);



/*!
 * Responsive Navigation Function
 */
!function(s){"use strict";s.fn.cmsmastersResponsiveNav=function(n){var t={submenu:"ul.sub-menu",respButton:".responsive_nav",startWidth:checker.ua.safari&&!checker.ua.chrome?1024:1007},e=this,i={};i={init:function(){i.o=s.extend({},t,n),i.el=e,i.v={},i.v.pLinkText="",i.v.subLinkToggle=void 0,i.setVars(),i.restartNav()},setVars:function(){i.v.submenu=i.el.find(i.o.submenu),i.v.subLink=i.v.submenu.closest("li").find("> a"),i.v.respButton=s(i.o.respButton),i.v.startWidth=i.o.startWidth,i.v.win=s(window),i.v.trigger=!1,i.v.counter=0,i.startEvent()},buildNav:function(){i.v.trigger=!0,i.v.counter=1,i.v.subLink.each(function(){""===s(this).text()&&(i.v.pLinkText=s(this).closest("ul").closest("li").find("> a").text(),s(this).addClass("cmsmasters_resp_nav_custom_text").html('<span class="nav_item_wrap"><span class="nav_title">'+i.v.counter+'. '+i.v.pLinkText+'</span></span>'),i.v.counter+=1),s(this).append('<span class="cmsmasters_resp_nav_toggle cmsmasters_theme_icon_resp_nav_slide_down" />')}),i.v.subLinkToggle=i.v.subLink.find("> span.cmsmasters_resp_nav_toggle"),i.v.submenu.hide(),i.attachEvents()},restartNav:function(){!i.v.trigger&&i.v.win.width()<=i.v.startWidth?i.buildNav():i.v.trigger&&i.v.win.width()>i.v.startWidth&&i.destroyNav()},resetNav:function(){i.v.subLinkToggle.removeClass("cmsmasters_theme_icon_resp_nav_slide_up").addClass("cmsmasters_theme_icon_resp_nav_slide_down"),i.v.submenu.hide()},destroyNav:function(){i.v.subLink.each(function(){s(this).hasClass("cmsmasters_resp_nav_custom_text")&&s(this).removeClass("cmsmasters_resp_nav_custom_text").text(""),s(this).find("span.cmsmasters_resp_nav_toggle").remove()}),i.v.submenu.css("display",""),i.v.trigger=!1,i.detachEvents()},startEvent:function(){i.v.win.on("resize",function(){i.restartNav()})},attachEvents:function(){i.v.subLinkToggle.on("click",function(){return s(this).hasClass("cmsmasters_theme_icon_resp_nav_slide_up")?(s(this).removeClass("cmsmasters_theme_icon_resp_nav_slide_up").addClass("cmsmasters_theme_icon_resp_nav_slide_down").closest("li").find("ul.sub-menu").hide(),s(this).closest("li").find("span.cmsmasters_resp_nav_toggle").removeClass("cmsmasters_theme_icon_resp_nav_slide_up").addClass("cmsmasters_theme_icon_resp_nav_slide_down")):s(this).removeClass("cmsmasters_theme_icon_resp_nav_slide_down").addClass("cmsmasters_theme_icon_resp_nav_slide_up").closest("li").find("ul.sub-menu").eq(0).show(),!1}),i.v.respButton.on("click",function(){i.v.trigger&&s(this).hasClass("active")&&i.resetNav()})},detachEvents:function(){i.v.subLinkToggle.off("click")}},i.init()}}(jQuery);

