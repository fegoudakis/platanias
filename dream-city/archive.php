<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Blog Archives Page Template
 * Created by CMSMasters
 * 
 */


get_header();


list($cmsmasters_layout) = dream_city_theme_page_layout_scheme();


echo '<!--_________________________ Start Content _________________________ -->' . "\n";


if ($cmsmasters_layout == 'r_sidebar') {
	if(is_category( '101' ) || is_category( '169' ) || is_category( '170' ) || is_category( '171' ) || is_category( '173' ) || is_category( '168' )){	
	echo '<div class="content entry">' . "\n\t";
	}else{
	echo '<div class="content entry" style="width:100%">' . "\n\t";
	}
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo '<div class="content entry fr">' . "\n\t";
} else {
	echo '<div class="middle_content entry">';
}


echo '<div class="cmsmasters_archive">' . "\n";


if (!have_posts()) : 
	echo '<h2>' . esc_html__('Δεν υπάρχει περιεχόμενο.', 'dream-city') . '</h2>';
else : 

	 if (get_post_type() == 'project') {
		 
		 $have_child_term = get_term( get_queried_object()->term_id, 'pj-categs' );

		 
		 echo term_description( get_queried_object()->term_id, 'pj-categs' );
		// var_dump($have_child_term);
		 
		 //MONO ROOT YPIRESDIES
		if ($have_child_term->parent == 0) {
			
			$terms = get_terms( array(
			    'taxonomy' => 'pj-categs',
			    'hide_empty' => true,
				// 'parent'   => 0
			) );
			
			/*echo '<pre>';
			print_r($terms);
			echo '</pre>';*/?>
			 
		<div style="margin-top:45px;">	 
		<?php
			 foreach($terms as $term){
				 if ($term->parent == get_queried_object()->term_id	) {
					// echo "AAaaAA <a href='".get_site_url()."/Ενότητα/". $term->slug ."'>". $term->name . "</a><br>"; 
					 $name = $term->name ;
					 
		?>
		
			<div class="hover">
				<div class="post post-masonry" >

					<div class="post-masonry-content box-style">
					<?php  //echo get_post_type() ; ?>

						<div class="bg-image">
							<a style=" color:#3183ee; font-size:16px;" href="<?php echo get_site_url()."/Ενότητα/". $term->slug;?>">
							  <h2 style="text-align:center; color: white; padding: 40px 5px; font-size:26px;"><?php echo $name; ?></h2>
							</a>
						</div>

						<div class="post-content" style="padding-top:0;">

							<h4 style="text-align:center;"><a style=" color:#3183ee; font-size:16px;" href="<?php echo get_site_url()."/Ενότητα/". $term->slug;?>">Περισσότερα ><?php //echo $name; ?></a></h4>

						</div>

					</div>

				</div>
			</div>
		
					 
				<?php	 
				 }
			 }?>
		</div>	
	<?php		
		} else {
			/*$have_child = $term_children = get_terms(
					'pj-categs',
					array(
						'parent' => 118,
					)
				);*/
				$term_id = get_queried_object_id(); 
				$taxonomy_name = 'pj-categs';

				$args = array('parent' => $term_id,'parent' => $term_id, 'hide_empty' => false );
				$have_child = get_terms( $taxonomy_name, $args);

			/*if ($have_child_term->parent == 0) {

			}*/
			//$have_child = get_term_children(get_queried_object()->term_id, 'pj-categs');
			echo '<pre>';
			//print_r($have_child);
			echo '</pre>';
		?>
		<div style="margin-top:45px;">
			<?php if ($have_child) {
				foreach ($have_child as $termid) {
					echo '<pre>';
				//	print_r($termid->name);
					echo '</pre>';
					
					$childs = get_term_by('id', $termid, 'pj-categs');
					echo '<pre>';
					//print_r($childs);
					echo '</pre>';
					//$name = $termid->name;
					//echo $childs;
					//$slug = $termid->slug;
					//echo $slug;
					//echo "AAaaAA <a href='".get_site_url()."/Ενότητα/". $childs->slug ."'>". $childs->name . "</a><br>";
					 ?>
					 
					<div class="hover">
						 <div class="post post-masonry" >
							<div class="post-masonry-content box-style">
									<div class="bg-image">
										<a style=" color:#3183ee; font-size:16px;" href="<?php echo get_site_url()."/Ενότητα/". $termid->slug;?>">
										  <h2 style="text-align:center; color: white; padding: 40px 5px; font-size:26px;"><?php echo $termid->name; ?></h2>
										</a>
									</div>

									<div class="post-content" style="padding-top:0;">
										<h4 style="text-align:center;"><a style=" color:#3183ee; font-size:16px;" href="<?php echo get_site_url()."/Ενότητα/". $termid->slug;?>">Περισσότερα ><?php //echo $name; ?></a></h4>
									</div>
							</div>
						</div>
					</div>
			<?php
					//print_r($childs);
					/*foreach ($childs as $child) {
						echo '<pre>';
						print_r($child);
						echo '</pre>';
						
					}*/
				} ?>
		</div>
			<?php } else {
				while (have_posts()) : the_post();
					
					get_template_part('theme-framework/template/archive');
					
				endwhile;
			}	
			
		 
		}
		 

		 
		 
	 } else {
		 
		 
		while (have_posts()) : the_post();
			
			get_template_part('theme-framework/template/archive');
			
		endwhile;
		
		echo cmsmasters_pagination(); 
			 
	}
		 
	 

	
	
		 
endif;


echo '</div>' . "\n" . 
'</div>' . "\n" . 
'<!-- _________________________ Finish Content _________________________ -->' . "\n\n";

if ($cmsmasters_layout == 'r_sidebar') {
	echo "\n" . '<!-- _________________________ Start Sidebar _________________________ -->' . "\n" . 
	'<div class="sidebar">' . "\n";
	if (is_category( '101' )|| is_category( '169' ) || is_category( '170' ) || is_category( '171' ) || is_category( '173' ) || is_category( '168' )){
		//$categories = get_the_category();
	/*	echo '<pre>';
		var_dump($categories);
		echo '</pre>';*/
		//$cur = single_cat_title();
		//echo $cur;
		$categories=get_categories(
			array( 'parent' => 101 )
		);
		/*echo '<pre>';
		var_dump($categories);
		echo '</pre>';*/
		
		echo '<div class="cat-names" style="">';
		echo '<p>Προκηρύξεις - Προσλήψεις βάση Χρονολογίας</p>';
		echo '<ul>';
		
		foreach ($categories as $category){
			
			$cat_id = $category->term_id;
			$cat_name = $category->name;
			
			echo '<li><a href="'.get_category_link($cat_id).'">'.$cat_name;'</a></li>';
			
		}
		
		echo '</ul>';
		echo '</div>';
	}

	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- _________________________ Finish Sidebar _________________________ -->' . "\n";
} elseif ($cmsmasters_layout == 'l_sidebar') {
	echo "\n" . '<!-- _________________________ Start Sidebar _________________________ -->' . "\n" . 
	'<div class="sidebar fl">' . "\n";
	
	get_sidebar();
	
	echo "\n" . '</div>' . "\n" . 
	'<!-- _________________________ Finish Sidebar _________________________ -->' . "\n";
}


get_footer();

?>


