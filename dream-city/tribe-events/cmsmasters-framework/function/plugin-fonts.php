<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.1
 * 
 * Tribe Events Fonts Rules
 * Created by CMSMasters
 * 
 */


function dream_city_tribe_events_fonts($custom_css) {
	$cmsmasters_option = dream_city_get_global_options();
	
	
	$custom_css .= "
/***************** Start Tribe Events Font Styles ******************/

	/* Start Content Font */
	.widget .vcalendar .cmsmasters_widget_event_info, 
	.widget .vcalendar .cmsmasters_widget_event_info a, 
	.tribe-this-week-events-widget .tribe-this-week-event .duration, 
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue, 
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue a, 
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-],
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-] a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_content_font_google_font']) . $cmsmasters_option['dream-city' . '_content_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_content_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_content_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_content_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_content_font_font_style'] . ";
	}
	
	.tribe-this-week-events-widget .tribe-this-week-event .duration, 
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue, 
	.tribe-this-week-events-widget .tribe-this-week-event .tribe-venue a, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info, 
	.tribe-mini-calendar-list-wrapper .cmsmasters_widget_event_info a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 1) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_content_font_line_height'] - 4) . "px;
	}
	
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-],
	table.tribe-events-calendar tbody td div[id*=tribe-events-daynum-] a, 
	table.tribe-events-calendar tbody td .tribe-events-viewmore a,
	.tribe-events-notices > ul > li,
	.tribe-events-venue .cmsmasters_events_venue_header_right a,
	.tribe-events-organizer .cmsmasters_events_organizer_header_right a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 2) . "px;
	}
	
	.tribe-mini-calendar thead th,
	.tribe-mini-calendar tbody,
	.tribe-mini-calendar tbody a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 2) . "px;
		line-height:20px;
	}
	
	.tribe-events-grid .column.first, 
	.tribe-events-grid .tribe-week-grid-hours {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 4) . "px;
	}
	
	
	.tribe-this-week-events-widget .tribe-this-week-widget-header-date {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_content_font_font_size'] - 1) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_content_font_line_height'] - 4) . "px;
	}
	
	.tribe-events-grid .tribe-week-grid-hours {
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_content_font_line_height'] - 8) . "px;
	}
	
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info {
			font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_content_font_google_font']) . $cmsmasters_option['dream-city' . '_content_font_system_font'] . ";
			font-size:" . $cmsmasters_option['dream-city' . '_content_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['dream-city' . '_content_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['dream-city' . '_content_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['dream-city' . '_content_font_font_style'] . ";
		}
	}
	
	/* Finish Content Font */
	
	
	/* Start Link Font */
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .cmsmasters_widget_event_info a {
			font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_link_font_google_font']) . $cmsmasters_option['dream-city' . '_link_font_system_font'] . ";
			font-size:" . $cmsmasters_option['dream-city' . '_link_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['dream-city' . '_link_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['dream-city' . '_link_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['dream-city' . '_link_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['dream-city' . '_link_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['dream-city' . '_link_font_text_decoration'] . ";
		}
	}
	
	/* Finish Link Font */
	
	
	/* Start H1 Font */
	.tribe-events-list .type-tribe_events .cmsmasters_events_start_date .cmsmasters_event_day,
	.cmsmasters_single_event .cmsmasters_events_start_date .cmsmasters_event_day,
	.tribe-events-venue .cmsmasters_events_start_date .cmsmasters_event_day,
	.tribe-events-organizer .cmsmasters_events_start_date .cmsmasters_event_day {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h1_font_google_font']) . $cmsmasters_option['dream-city' . '_h1_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h1_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h1_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h1_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h1_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h1_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h1_font_text_decoration'] . ";
	}
	
	.tribe-events-list .type-tribe_events .cmsmasters_events_start_date .cmsmasters_event_day,
	.cmsmasters_single_event .cmsmasters_events_start_date .cmsmasters_event_day,
	.tribe-events-venue .cmsmasters_events_start_date .cmsmasters_event_day,
	.tribe-events-organizer .cmsmasters_events_start_date .cmsmasters_event_day {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 50) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] + 50) . "px;
		font-weight:400;
	}
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-adv-list-widget .cmsmasters_event_date.date_for_content .cmsmasters_event_date_day {
			font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 50) . "px;
			line-height:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] + 50) . "px;
			font-weight:400;
		}
	}
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-countdown-widget .tribe-countdown-time {
			font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h1_font_google_font']) . $cmsmasters_option['dream-city' . '_h1_font_system_font'] . ";
			font-size:" . $cmsmasters_option['dream-city' . '_h1_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['dream-city' . '_h1_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['dream-city' . '_h1_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['dream-city' . '_h1_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['dream-city' . '_h1_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['dream-city' . '_h1_font_text_decoration'] . ";
		}
		
		.cmsmasters_row .one_first .tribe-events-countdown-widget .tribe-countdown-time {
			font-size:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_font_size'] + 28) . "px;
			line-height:" . ((int) $cmsmasters_option['dream-city' . '_h1_font_line_height'] + 22) . "px;
		}
	}
	/* Finish H1 Font */
	
	
	/* Start H2 Font */
	.tribe-events-page-title,
	.tribe-events-page-title a,
	.cmsmasters_event_date:not(.date_for_content) .cmsmasters_event_date_month,
	.tribe-events-countdown-widget .tribe-countdown-time {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h2_font_google_font']) . $cmsmasters_option['dream-city' . '_h2_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h2_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h2_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h2_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h2_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h2_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h2_font_text_decoration'] . ";
	}
	
	.tribe-events-countdown-widget .tribe-countdown-time {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_font_size'] - 4) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_line_height'] - 12) . "px;
	}
	
	.cmsmasters_event_date:not(.date_for_content) .cmsmasters_event_date_month {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_font_size'] + 6) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h2_font_line_height'] + 6) . "px;	
	}
	
	/* Finish H2 Font */
	
	
	/* Start H3 Font */
	.tribe-mobile-day .tribe-mobile-day-date, 
	.tribe-events-list .tribe-events-list-event-title,
	.tribe-events-list .tribe-events-list-event-title a,
	.tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .entry-title a, 
	.tribe-events-list-widget .tribe-events-list-widget-content-wrap .entry-title a, 
	.cmsmasters_single_event .tribe-events-single-event-title {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h3_font_google_font']) . $cmsmasters_option['dream-city' . '_h3_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h3_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h3_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h3_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h3_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h3_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h3_font_text_decoration'] . ";
	}
	
	.tribe-events-list .tribe-events-list-event-title,
	.tribe-events-list .tribe-events-list-event-title a,
	.cmsmasters_single_event .tribe-events-single-event-title {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h3_font_font_size'] + 4) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_h3_font_line_height'] + 4) . "px;
	}
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .entry-title a {
			font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h3_font_google_font']) . $cmsmasters_option['dream-city' . '_h3_font_system_font'] . ";
			font-size:" . $cmsmasters_option['dream-city' . '_h3_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['dream-city' . '_h3_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['dream-city' . '_h3_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['dream-city' . '_h3_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['dream-city' . '_h3_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['dream-city' . '_h3_font_text_decoration'] . ";
		}
		
		.cmsmasters_row .one_first .one_first .tribe-events-adv-list-widget .tribe-events-list-widget-content-wrap .entry-title a {
			font-size:" . ((int) $cmsmasters_option['dream-city' . '_h3_font_font_size'] + 4) . "px;
			line-height:" . ((int) $cmsmasters_option['dream-city' . '_h3_font_line_height'] + 4) . "px;
		}
	}
	/* Finish H3 Font */
	
	
	/* Start H4 Font */
	.tribe-events-related-events-title, 
	.tribe-events-list .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_week,
	.cmsmasters_single_event .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_week,
	.tribe-events-venue .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_week,
	.tribe-events-organizer .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_week {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h4_font_google_font']) . $cmsmasters_option['dream-city' . '_h4_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h4_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h4_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h4_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h4_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h4_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h4_font_text_decoration'] . ";
	}
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-adv-list-widget .cmsmasters_event_date.date_for_content > span.cmsmasters_event_date_month_wrap .cmsmasters_event_date_week {
			font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h4_font_google_font']) . $cmsmasters_option['dream-city' . '_h4_font_system_font'] . ";
			font-size:" . $cmsmasters_option['dream-city' . '_h4_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['dream-city' . '_h4_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['dream-city' . '_h4_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['dream-city' . '_h4_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['dream-city' . '_h4_font_text_transform'] . ";
			text-decoration:" . $cmsmasters_option['dream-city' . '_h4_font_text_decoration'] . ";
		}
	}
	
	/* Finish H4 Font */
	
	
	/* Start H5 Font */
	.tribe-bar-filters-inner > div label,
	#tribe-events-content > .tribe-events-button,
	.cmsmasters_single_event .tribe-events-cost,
	.cmsmasters_single_event_meta dt, 
	.cmsmasters_single_event_meta dd a,
	.tribe-mobile-day .tribe-events-read-more, 
	.tribe-events-countdown-widget .tribe-countdown-text,
	.tribe-events-countdown-widget .tribe-countdown-text a, 
	.tribe-this-week-events-widget .tribe-events-viewmore a,
	.tribe-events-list .tribe-events-list-separator-month,
	.tribe-events-list .tribe-events-day-time-slot > h5,
	.cmsmasters_single_event .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_month,
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title,
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title a,
	.tribe-events-venue .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_month,
	.tribe-events-organizer .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_month {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h5_font_google_font']) . $cmsmasters_option['dream-city' . '_h5_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h5_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h5_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h5_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h5_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h5_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h5_font_text_decoration'] . ";
	}
	
	.tribe-this-week-events-widget .tribe-this-week-event .entry-title a, 
	.tribe_mini_calendar_widget .tribe-mini-calendar-list-wrapper .entry-title a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h5_font_font_size'] - 1) . "px;
	}
	/* Finish H5 Font */
	
	
	/* Start H6 Font */
	.tribe-this-week-events-widget .tribe-events-page-title, 
	.tribe-events-tooltip .entry-title,
	.tribe-mini-calendar [id*=tribe-mini-calendar-month],
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item_descr, 
	.cmsmasters_single_event_meta .cmsmasters_event_meta_info_item_title, 
	table.tribe-events-calendar tbody td .tribe-events-month-event-title,
	table.tribe-events-calendar tbody td .tribe-events-month-event-title a, 
	.tribe-mobile-day .tribe-events-event-schedule-details, 
	.tribe-mobile-day .tribe-event-schedule-details,
	table.tribe-events-calendar thead th,
	.tribe-events-grid .tribe-grid-header span {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_h6_font_google_font']) . $cmsmasters_option['dream-city' . '_h6_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_h6_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_h6_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_h6_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_h6_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_h6_font_text_transform'] . ";
		text-decoration:" . $cmsmasters_option['dream-city' . '_h6_font_text_decoration'] . ";
	}
	
	.tribe-events-tooltip .entry-title, 
	table.tribe-events-calendar tbody td .tribe-events-month-event-title,
	table.tribe-events-calendar tbody td .tribe-events-month-event-title a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_h6_font_font_size'] - 1) . "px;
	}
	/* Finish H6 Font */
	
	
	/* Start Button Font */
	#tribe-bar-views .tribe-bar-views-list li,
	#tribe-bar-views .tribe-bar-views-list li a {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_button_font_google_font']) . $cmsmasters_option['dream-city' . '_button_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_button_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_button_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_button_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_button_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_button_font_text_transform'] . ";
	}
	
	/* Finish Button Font */
	
	
	/* Start Small Text Font */
	.tribe-events-venue-widget .tribe-venue-widget-venue-name, 
	.cmsmasters_event_date,
	.tribe-events-countdown-widget .tribe-countdown-under,
	.tribe-events-tooltip .duration, 
	.tribe-related-events .cmsmasters_events_start_date, 
	.tribe-events-tooltip .tribe-events-event-body *, 
	.cmsmasters_event_date:not(.date_for_content) .cmsmasters_event_date_day, 
	.tribe-events-grid .tribe-week-event .vevent .entry-title a, 
	.tribe-events-list .cmsmasters_events_start_date .cmsmasters_event_month_week .cmsmasters_event_month, 
	.tribe-events-photo .tribe-events-event-meta,
	.tribe-events-photo .tribe-events-event-meta a,
	.tribe-events-photo .cmsmasters_event_date_details {
		font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_small_font_google_font']) . $cmsmasters_option['dream-city' . '_small_font_system_font'] . ";
		font-size:" . $cmsmasters_option['dream-city' . '_small_font_font_size'] . "px;
		line-height:" . $cmsmasters_option['dream-city' . '_small_font_line_height'] . "px;
		font-weight:" . $cmsmasters_option['dream-city' . '_small_font_font_weight'] . ";
		font-style:" . $cmsmasters_option['dream-city' . '_small_font_font_style'] . ";
		text-transform:" . $cmsmasters_option['dream-city' . '_small_font_text_transform'] . ";
	}
	
	.tribe-events-tooltip .tribe-events-event-body * {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] - 2) . "px;
		line-height:" . ((int) $cmsmasters_option['dream-city' . '_small_font_line_height'] - 1) . "px;
	}
	
	.tribe-events-grid .tribe-week-event .vevent .entry-title a, 
	.tribe-events-tooltip .duration {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] - 2) . "px;
	}
	
	.cmsmasters_event_date:not(.date_for_content) .cmsmasters_event_date_day {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] - 2) . "px;
		text-transform:uppercase;
	}
	
	.cmsmasters_event_date,
	.tribe-events-photo .tribe-events-event-meta,
	.tribe-events-countdown-widget .tribe-countdown-number .tribe-countdown-under,
	.tribe-events-photo .tribe-events-event-meta a {
		font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] - 1) . "px;
	}
	
	@media only screen and (min-width: 950px) {
		.cmsmasters_row .one_first .tribe-events-adv-list-widget .cmsmasters_event_date.date_for_content > span.cmsmasters_event_date_month_wrap .cmsmasters_event_date_month {
			font-family:" . dream_city_get_google_font($cmsmasters_option['dream-city' . '_small_font_google_font']) . $cmsmasters_option['dream-city' . '_small_font_system_font'] . ";
			font-size:" . $cmsmasters_option['dream-city' . '_small_font_font_size'] . "px;
			line-height:" . $cmsmasters_option['dream-city' . '_small_font_line_height'] . "px;
			font-weight:" . $cmsmasters_option['dream-city' . '_small_font_font_weight'] . ";
			font-style:" . $cmsmasters_option['dream-city' . '_small_font_font_style'] . ";
			text-transform:" . $cmsmasters_option['dream-city' . '_small_font_text_transform'] . ";
		}
		
		.cmsmasters_row .one_first .tribe-events-countdown-widget .tribe-countdown-number .tribe-countdown-under {
			font-size:" . ((int) $cmsmasters_option['dream-city' . '_small_font_font_size'] + 2) . "px;
			line-height:" . ((int) $cmsmasters_option['dream-city' . '_small_font_line_height'] + 2) . "px;
		}
	}
	
	/* Finish Small Text Font */

/***************** Finish Tribe Events Font Styles ******************/

";
	
	
	return $custom_css;
}

add_filter('dream_city_theme_fonts_filter', 'dream_city_tribe_events_fonts');

