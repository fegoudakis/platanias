<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Tribe Events Functions
 * Created by CMSMasters
 * 
 */


/* Load Parts */
require_once(get_template_directory() . '/tribe-events/cmsmasters-framework/admin/plugin-settings.php');
require_once(get_template_directory() . '/tribe-events/cmsmasters-framework/function/plugin-colors.php');
require_once(get_template_directory() . '/tribe-events/cmsmasters-framework/function/plugin-fonts.php');


/* Register CSS Styles and Scripts */
function dream_city_tribe_events_register_styles_scripts() {
	// Styles
	wp_enqueue_style('dream-city-tribe-events-style', get_template_directory_uri() . '/tribe-events/cmsmasters-framework/css/plugin-style.css', array(), '1.0.0', 'screen');
	
	wp_enqueue_style('dream-city-tribe-events-adaptive', get_template_directory_uri() . '/tribe-events/cmsmasters-framework/css/plugin-adaptive.css', array(), '1.0.0', 'screen');
	
	
	if (is_rtl()) {
		wp_enqueue_style('dream-city-tribe-events-rtl', get_template_directory_uri() . '/tribe-events/cmsmasters-framework/css/plugin-rtl.css', array(), '1.0.0', 'screen');
	}
}

add_action('wp_enqueue_scripts', 'dream_city_tribe_events_register_styles_scripts');


/* Replace Styles */
function dream_city_tribe_events_stylesheet_url() {
	$styleUrl = '';
	
	
	return $styleUrl;
}

add_filter('tribe_events_stylesheet_url', 'dream_city_tribe_events_stylesheet_url');


/* Replace Pro Styles */
function dream_city_tribe_events_pro_stylesheet_url() {
	$styleUrl = '';
	
	
	return $styleUrl;
}

add_filter('tribe_events_pro_stylesheet_url', 'dream_city_tribe_events_pro_stylesheet_url');


/* Replace Widget Styles */
function dream_city_tribe_events_pro_widget_calendar_stylesheet_url() {
	$styleUrl = '';
	
	
	return $styleUrl;
}

add_filter('tribe_events_pro_widget_calendar_stylesheet_url', 'dream_city_tribe_events_pro_widget_calendar_stylesheet_url');


/* Replace Responsive Styles */
function dream_city_tribe_events_mobile_breakpoint() {
    return 768;
}

add_filter('tribe_events_mobile_breakpoint', 'dream_city_tribe_events_mobile_breakpoint');

