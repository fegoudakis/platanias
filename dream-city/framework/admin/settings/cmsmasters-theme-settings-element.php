<?php 
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Admin Panel Element Options
 * Created by CMSMasters
 * 
 */


function dream_city_options_element_tabs() {
	$tabs = array();
	
	$tabs['sidebar'] = esc_attr__('Sidebars', 'dream-city');
	$tabs['icon'] = esc_attr__('Social Icons', 'dream-city');
	$tabs['lightbox'] = esc_attr__('Lightbox', 'dream-city');
	$tabs['sitemap'] = esc_attr__('Sitemap', 'dream-city');
	$tabs['error'] = esc_attr__('404', 'dream-city');
	$tabs['code'] = esc_attr__('Custom Codes', 'dream-city');
	
	if (class_exists('Cmsmasters_Form_Builder')) {
		$tabs['recaptcha'] = esc_attr__('reCAPTCHA', 'dream-city');
	}
	
	return apply_filters('cmsmasters_options_element_tabs_filter', $tabs);
}


function dream_city_options_element_sections() {
	$tab = dream_city_get_the_tab();
	
	switch ($tab) {
	case 'sidebar':
		$sections = array();
		
		$sections['sidebar_section'] = esc_attr__('Custom Sidebars', 'dream-city');
		
		break;
	case 'icon':
		$sections = array();
		
		$sections['icon_section'] = esc_attr__('Social Icons', 'dream-city');
		
		break;
	case 'lightbox':
		$sections = array();
		
		$sections['lightbox_section'] = esc_attr__('Theme Lightbox Options', 'dream-city');
		
		break;
	case 'sitemap':
		$sections = array();
		
		$sections['sitemap_section'] = esc_attr__('Sitemap Page Options', 'dream-city');
		
		break;
	case 'error':
		$sections = array();
		
		$sections['error_section'] = esc_attr__('404 Error Page Options', 'dream-city');
		
		break;
	case 'code':
		$sections = array();
		
		$sections['code_section'] = esc_attr__('Custom Codes', 'dream-city');
		
		break;
	case 'recaptcha':
		$sections = array();
		
		$sections['recaptcha_section'] = esc_attr__('Form Builder Plugin reCAPTCHA Keys', 'dream-city');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_element_sections_filter', $sections, $tab);	
} 


function dream_city_options_element_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = dream_city_get_the_tab();
	}
	
	
	$options = array();
	
	
	$defaults = dream_city_settings_element_defaults();
	
	
	switch ($tab) {
	case 'sidebar':
		$options[] = array( 
			'section' => 'sidebar_section', 
			'id' => 'dream-city' . '_sidebar', 
			'title' => esc_html__('Custom Sidebars', 'dream-city'), 
			'desc' => '', 
			'type' => 'sidebar', 
			'std' => $defaults[$tab]['dream-city' . '_sidebar'] 
		);
		
		break;
	case 'icon':
		$options[] = array( 
			'section' => 'icon_section', 
			'id' => 'dream-city' . '_social_icons', 
			'title' => esc_html__('Social Icons', 'dream-city'), 
			'desc' => '', 
			'type' => 'social', 
			'std' => $defaults[$tab]['dream-city' . '_social_icons'] 
		);
		
		break;
	case 'lightbox':
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_skin', 
			'title' => esc_html__('Skin', 'dream-city'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_skin'], 
			'choices' => array( 
				esc_html__('Dark', 'dream-city') . '|dark', 
				esc_html__('Light', 'dream-city') . '|light', 
				esc_html__('Mac', 'dream-city') . '|mac', 
				esc_html__('Metro Black', 'dream-city') . '|metro-black', 
				esc_html__('Metro White', 'dream-city') . '|metro-white', 
				esc_html__('Parade', 'dream-city') . '|parade', 
				esc_html__('Smooth', 'dream-city') . '|smooth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_path', 
			'title' => esc_html__('Path', 'dream-city'), 
			'desc' => esc_html__('Sets path for switching windows', 'dream-city'), 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_path'], 
			'choices' => array( 
				esc_html__('Vertical', 'dream-city') . '|vertical', 
				esc_html__('Horizontal', 'dream-city') . '|horizontal' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_infinite', 
			'title' => esc_html__('Infinite', 'dream-city'), 
			'desc' => esc_html__('Sets the ability to infinite the group', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_infinite'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_aspect_ratio', 
			'title' => esc_html__('Keep Aspect Ratio', 'dream-city'), 
			'desc' => esc_html__('Sets the resizing method used to keep aspect ratio within the viewport', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_aspect_ratio'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_mobile_optimizer', 
			'title' => esc_html__('Mobile Optimizer', 'dream-city'), 
			'desc' => esc_html__('Make lightboxes optimized for giving better experience with mobile devices', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_mobile_optimizer'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_max_scale', 
			'title' => esc_html__('Max Scale', 'dream-city'), 
			'desc' => esc_html__('Sets the maximum viewport scale of the content', 'dream-city'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_max_scale'], 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_min_scale', 
			'title' => esc_html__('Min Scale', 'dream-city'), 
			'desc' => esc_html__('Sets the minimum viewport scale of the content', 'dream-city'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_min_scale'], 
			'min' => 0.1, 
			'max' => 2, 
			'step' => 0.05 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_inner_toolbar', 
			'title' => esc_html__('Inner Toolbar', 'dream-city'), 
			'desc' => esc_html__('Bring buttons into windows, or let them be over the overlay', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_inner_toolbar'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_smart_recognition', 
			'title' => esc_html__('Smart Recognition', 'dream-city'), 
			'desc' => esc_html__('Sets content auto recognize from web pages', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_smart_recognition'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_fullscreen_one_slide', 
			'title' => esc_html__('Fullscreen One Slide', 'dream-city'), 
			'desc' => esc_html__('Decide to fullscreen only one slide or hole gallery the fullscreen mode', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_fullscreen_one_slide'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_fullscreen_viewport', 
			'title' => esc_html__('Fullscreen Viewport', 'dream-city'), 
			'desc' => esc_html__('Sets the resizing method used to fit content within the fullscreen mode', 'dream-city'), 
			'type' => 'select', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_fullscreen_viewport'], 
			'choices' => array( 
				esc_html__('Center', 'dream-city') . '|center', 
				esc_html__('Fit', 'dream-city') . '|fit', 
				esc_html__('Fill', 'dream-city') . '|fill', 
				esc_html__('Stretch', 'dream-city') . '|stretch' 
			) 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_toolbar', 
			'title' => esc_html__('Toolbar Controls', 'dream-city'), 
			'desc' => esc_html__('Sets buttons be available or not', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_toolbar'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_arrows', 
			'title' => esc_html__('Arrow Controls', 'dream-city'), 
			'desc' => esc_html__('Enable the arrow buttons', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_arrows'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_fullscreen', 
			'title' => esc_html__('Fullscreen Controls', 'dream-city'), 
			'desc' => esc_html__('Sets the fullscreen button', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_fullscreen'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_thumbnail', 
			'title' => esc_html__('Thumbnails Controls', 'dream-city'), 
			'desc' => esc_html__('Sets the thumbnail navigation', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_thumbnail'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_keyboard', 
			'title' => esc_html__('Keyboard Controls', 'dream-city'), 
			'desc' => esc_html__('Sets the keyboard navigation', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_keyboard'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_mousewheel', 
			'title' => esc_html__('Mouse Wheel Controls', 'dream-city'), 
			'desc' => esc_html__('Sets the mousewheel navigation', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_mousewheel'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_swipe', 
			'title' => esc_html__('Swipe Controls', 'dream-city'), 
			'desc' => esc_html__('Sets the swipe navigation', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_swipe'] 
		);
		
		$options[] = array( 
			'section' => 'lightbox_section', 
			'id' => 'dream-city' . '_ilightbox_controls_slideshow', 
			'title' => esc_html__('Slideshow Controls', 'dream-city'), 
			'desc' => esc_html__('Enable the slideshow feature and button', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_ilightbox_controls_slideshow'] 
		);
		
		break;
	case 'sitemap':
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'dream-city' . '_sitemap_nav', 
			'title' => esc_html__('Website Pages', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_sitemap_nav'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'dream-city' . '_sitemap_categs', 
			'title' => esc_html__('Blog Archives by Categories', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_sitemap_categs'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'dream-city' . '_sitemap_tags', 
			'title' => esc_html__('Blog Archives by Tags', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_sitemap_tags'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'dream-city' . '_sitemap_month', 
			'title' => esc_html__('Blog Archives by Month', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_sitemap_month'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'dream-city' . '_sitemap_pj_categs', 
			'title' => esc_html__('Portfolio Archives by Categories', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_sitemap_pj_categs'] 
		);
		
		$options[] = array( 
			'section' => 'sitemap_section', 
			'id' => 'dream-city' . '_sitemap_pj_tags', 
			'title' => esc_html__('Portfolio Archives by Tags', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_sitemap_pj_tags'] 
		);
		
		break;
	case 'error':
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_color', 
			'title' => esc_html__('Text Color', 'dream-city'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['dream-city' . '_error_color'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_color', 
			'title' => esc_html__('Background Color', 'dream-city'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_color'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_img_enable'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_image', 
			'title' => esc_html__('Background Image', 'dream-city'), 
			'desc' => esc_html__('Choose your custom error page background image.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_image'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_rep', 
			'title' => esc_html__('Background Repeat', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_rep'], 
			'choices' => array( 
				esc_html__('No Repeat', 'dream-city') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'dream-city') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'dream-city') . '|repeat-y', 
				esc_html__('Repeat', 'dream-city') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_pos', 
			'title' => esc_html__('Background Position', 'dream-city'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_pos'], 
			'choices' => array( 
				esc_html__('Top Left', 'dream-city') . '|top left', 
				esc_html__('Top Center', 'dream-city') . '|top center', 
				esc_html__('Top Right', 'dream-city') . '|top right', 
				esc_html__('Center Left', 'dream-city') . '|center left', 
				esc_html__('Center Center', 'dream-city') . '|center center', 
				esc_html__('Center Right', 'dream-city') . '|center right', 
				esc_html__('Bottom Left', 'dream-city') . '|bottom left', 
				esc_html__('Bottom Center', 'dream-city') . '|bottom center', 
				esc_html__('Bottom Right', 'dream-city') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_att', 
			'title' => esc_html__('Background Attachment', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_att'], 
			'choices' => array( 
				esc_html__('Scroll', 'dream-city') . '|scroll', 
				esc_html__('Fixed', 'dream-city') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_bg_size', 
			'title' => esc_html__('Background Size', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_error_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'dream-city') . '|auto', 
				esc_html__('Cover', 'dream-city') . '|cover', 
				esc_html__('Contain', 'dream-city') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_search', 
			'title' => esc_html__('Search Line', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_error_search'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_sitemap_button', 
			'title' => esc_html__('Sitemap Button', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_error_sitemap_button'] 
		);
		
		$options[] = array( 
			'section' => 'error_section', 
			'id' => 'dream-city' . '_error_sitemap_link', 
			'title' => esc_html__('Sitemap Page URL', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_error_sitemap_link'], 
			'class' => '' 
		);
		
		break;
	case 'code':
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_custom_css', 
			'title' => esc_html__('Custom CSS', 'dream-city'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['dream-city' . '_custom_css'], 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_custom_js', 
			'title' => esc_html__('Custom JavaScript', 'dream-city'), 
			'desc' => '', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['dream-city' . '_custom_js'], 
			'class' => 'allowlinebreaks' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_gmap_api_key', 
			'title' => esc_html__('Google Maps API key', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_gmap_api_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_api_key', 
			'title' => esc_html__('Twitter API key', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_api_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_api_secret', 
			'title' => esc_html__('Twitter API secret', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_api_secret'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_access_token', 
			'title' => esc_html__('Twitter Access token', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_access_token'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'code_section', 
			'id' => 'dream-city' . '_access_token_secret', 
			'title' => esc_html__('Twitter Access token secret', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_access_token_secret'], 
			'class' => '' 
		);
		
		break;
	case 'recaptcha':
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'dream-city' . '_recaptcha_public_key', 
			'title' => esc_html__('reCAPTCHA Public Key', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_recaptcha_public_key'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'recaptcha_section', 
			'id' => 'dream-city' . '_recaptcha_private_key', 
			'title' => esc_html__('reCAPTCHA Private Key', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_recaptcha_private_key'], 
			'class' => '' 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_element_fields_filter', $options, $tab);	
}

