<?php 
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Admin Panel General Options
 * Created by CMSMasters
 * 
 */


function dream_city_options_general_tabs() {
	$cmsmasters_option = dream_city_get_global_options();
	
	$tabs = array();
	
	$tabs['general'] = esc_attr__('General', 'dream-city');
	
	if ($cmsmasters_option['dream-city' . '_theme_layout'] === 'boxed') {
		$tabs['bg'] = esc_attr__('Background', 'dream-city');
	}
	
	$tabs['header'] = esc_attr__('Header', 'dream-city');
	$tabs['content'] = esc_attr__('Content', 'dream-city');
	$tabs['footer'] = esc_attr__('Footer', 'dream-city');
	
	return apply_filters('cmsmasters_options_general_tabs_filter', $tabs);
}


function dream_city_options_general_sections() {
	$tab = dream_city_get_the_tab();
	
	switch ($tab) {
	case 'general':
		$sections = array();
		
		$sections['general_section'] = esc_attr__('General Options', 'dream-city');
		
		break;
	case 'bg':
		$sections = array();
		
		$sections['bg_section'] = esc_attr__('Background Options', 'dream-city');
		
		break;
	case 'header':
		$sections = array();
		
		$sections['header_section'] = esc_attr__('Header Options', 'dream-city');
		
		break;
	case 'content':
		$sections = array();
		
		$sections['content_section'] = esc_attr__('Content Options', 'dream-city');
		
		break;
	case 'footer':
		$sections = array();
		
		$sections['footer_section'] = esc_attr__('Footer Options', 'dream-city');
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	return apply_filters('cmsmasters_options_general_sections_filter', $sections, $tab);
} 


function dream_city_options_general_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = dream_city_get_the_tab();
	}
	
	$options = array();
	
	
	$defaults = dream_city_settings_general_defaults();
	
	
	switch ($tab) {
	case 'general':
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_theme_layout', 
			'title' => esc_html__('Theme Layout', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_theme_layout'], 
			'choices' => array( 
				esc_html__('Liquid', 'dream-city') . '|liquid', 
				esc_html__('Boxed', 'dream-city') . '|boxed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_type', 
			'title' => esc_html__('Logo Type', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_logo_type'], 
			'choices' => array( 
				esc_html__('Image', 'dream-city') . '|image', 
				esc_html__('Text', 'dream-city') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_url', 
			'title' => esc_html__('Logo Image', 'dream-city'), 
			'desc' => esc_html__('Choose your website logo image.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_logo_url'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_url_retina', 
			'title' => esc_html__('Retina Logo Image', 'dream-city'), 
			'desc' => esc_html__('Choose logo image for retina displays. Logo for Retina displays should be twice the size of the default one.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_logo_url_retina'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_title', 
			'title' => esc_html__('Logo Title', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_logo_title'], 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_subtitle', 
			'title' => esc_html__('Logo Subtitle', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_logo_subtitle'], 
			'class' => 'nohtml' 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_custom_color', 
			'title' => esc_html__('Custom Text Colors', 'dream-city'), 
			'desc' => esc_html__('enable', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_logo_custom_color'] 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_title_color', 
			'title' => esc_html__('Logo Title Color', 'dream-city'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['dream-city' . '_logo_title_color'] 
		);
		
		$options[] = array( 
			'section' => 'general_section', 
			'id' => 'dream-city' . '_logo_subtitle_color', 
			'title' => esc_html__('Logo Subtitle Color', 'dream-city'), 
			'desc' => '', 
			'type' => 'rgba', 
			'std' => $defaults[$tab]['dream-city' . '_logo_subtitle_color'] 
		);
		
		break;
	case 'bg':
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_col', 
			'title' => esc_html__('Background Color', 'dream-city'), 
			'desc' => '', 
			'type' => 'color', 
			'std' => $defaults[$tab]['dream-city' . '_bg_col'] 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_img_enable', 
			'title' => esc_html__('Background Image Visibility', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_bg_img_enable'] 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_img', 
			'title' => esc_html__('Background Image', 'dream-city'), 
			'desc' => esc_html__('Choose your custom website background image url.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_bg_img'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_rep', 
			'title' => esc_html__('Background Repeat', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_bg_rep'], 
			'choices' => array( 
				esc_html__('No Repeat', 'dream-city') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'dream-city') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'dream-city') . '|repeat-y', 
				esc_html__('Repeat', 'dream-city') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_pos', 
			'title' => esc_html__('Background Position', 'dream-city'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['dream-city' . '_bg_pos'], 
			'choices' => array( 
				esc_html__('Top Left', 'dream-city') . '|top left', 
				esc_html__('Top Center', 'dream-city') . '|top center', 
				esc_html__('Top Right', 'dream-city') . '|top right', 
				esc_html__('Center Left', 'dream-city') . '|center left', 
				esc_html__('Center Center', 'dream-city') . '|center center', 
				esc_html__('Center Right', 'dream-city') . '|center right', 
				esc_html__('Bottom Left', 'dream-city') . '|bottom left', 
				esc_html__('Bottom Center', 'dream-city') . '|bottom center', 
				esc_html__('Bottom Right', 'dream-city') . '|bottom right' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_att', 
			'title' => esc_html__('Background Attachment', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_bg_att'], 
			'choices' => array( 
				esc_html__('Scroll', 'dream-city') . '|scroll', 
				esc_html__('Fixed', 'dream-city') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'bg_section', 
			'id' => 'dream-city' . '_bg_size', 
			'title' => esc_html__('Background Size', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'dream-city') . '|auto', 
				esc_html__('Cover', 'dream-city') . '|cover', 
				esc_html__('Contain', 'dream-city') . '|contain' 
			) 
		);
		
		break;
	case 'header':
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_fixed_header', 
			'title' => esc_html__('Fixed Header', 'dream-city'), 
			'desc' => esc_html__('enable', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_fixed_header'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_overlaps', 
			'title' => esc_html__('Header Overlaps Content', 'dream-city'), 
			'desc' => esc_html__('enable', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_header_overlaps'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_top_line', 
			'title' => esc_html__('Top Line', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_header_top_line'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_top_height', 
			'title' => esc_html__('Top Height', 'dream-city'), 
			'desc' => esc_html__('pixels', 'dream-city'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['dream-city' . '_header_top_height'], 
			'min' => '10' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_top_line_short_info', 
			'title' => esc_html__('Top Short Info', 'dream-city'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'dream-city') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['dream-city' . '_header_top_line_short_info'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_top_line_add_cont', 
			'title' => esc_html__('Top Additional Content', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_header_top_line_add_cont'], 
			'choices' => array( 
				esc_html__('None', 'dream-city') . '|none', 
				esc_html__('Top Line Social Icons', 'dream-city') . '|social', 
				esc_html__('Top Line Navigation', 'dream-city') . '|nav' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_styles', 
			'title' => esc_html__('Header Styles', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_header_styles'], 
			'choices' => array( 
				esc_html__('Default Style', 'dream-city') . '|default', 
				esc_html__('Compact Style Left Navigation', 'dream-city') . '|l_nav', 
				esc_html__('Compact Style Right Navigation', 'dream-city') . '|r_nav', 
				esc_html__('Compact Style Center Navigation', 'dream-city') . '|c_nav'
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_mid_height', 
			'title' => esc_html__('Header Middle Height', 'dream-city'), 
			'desc' => esc_html__('pixels', 'dream-city'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['dream-city' . '_header_mid_height'], 
			'min' => '40' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_bot_height', 
			'title' => esc_html__('Header Bottom Height', 'dream-city'), 
			'desc' => esc_html__('pixels', 'dream-city'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['dream-city' . '_header_bot_height'], 
			'min' => '20' 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_search', 
			'title' => esc_html__('Header Search', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_header_search'] 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_add_cont', 
			'title' => esc_html__('Header Additional Content', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_header_add_cont'], 
			'choices' => array( 
				esc_html__('None', 'dream-city') . '|none', 
				esc_html__('Header Social Icons', 'dream-city') . '|social', 
				esc_html__('Header Custom HTML', 'dream-city') . '|cust_html' 
			) 
		);
		
		$options[] = array( 
			'section' => 'header_section', 
			'id' => 'dream-city' . '_header_add_cont_cust_html', 
			'title' => esc_html__('Header Custom HTML', 'dream-city'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'dream-city') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['dream-city' . '_header_add_cont_cust_html'], 
			'class' => '' 
		);
		
		break;
	case 'content':
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_layout', 
			'title' => esc_html__('Layout Type by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['dream-city' . '_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_archives_layout', 
			'title' => esc_html__('Archives Layout Type', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['dream-city' . '_archives_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_search_layout', 
			'title' => esc_html__('Search Layout Type', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['dream-city' . '_search_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_other_layout', 
			'title' => esc_html__('Other Layout Type', 'dream-city'), 
			'desc' => 'Layout for pages of non-listed types', 
			'type' => 'radio_img', 
			'std' => $defaults[$tab]['dream-city' . '_other_layout'], 
			'choices' => array( 
				esc_html__('Right Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_r.jpg' . '|r_sidebar', 
				esc_html__('Left Sidebar', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/sidebar_l.jpg' . '|l_sidebar', 
				esc_html__('Full Width', 'dream-city') . '|' . get_template_directory_uri() . '/framework/admin/inc/img/fullwidth.jpg' . '|fullwidth' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_alignment', 
			'title' => esc_html__('Heading Alignment by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_heading_alignment'], 
			'choices' => array( 
				esc_html__('Left', 'dream-city') . '|left', 
				esc_html__('Right', 'dream-city') . '|right', 
				esc_html__('Center', 'dream-city') . '|center' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_scheme', 
			'title' => esc_html__('Heading Custom Color Scheme by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['dream-city' . '_heading_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_bg_image_enable', 
			'title' => esc_html__('Heading Background Image Visibility by Default', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_heading_bg_image_enable'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_bg_image', 
			'title' => esc_html__('Heading Background Image by Default', 'dream-city'), 
			'desc' => esc_html__('Choose your custom heading background image by default.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_heading_bg_image'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_bg_repeat', 
			'title' => esc_html__('Heading Background Repeat by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_heading_bg_repeat'], 
			'choices' => array( 
				esc_html__('No Repeat', 'dream-city') . '|no-repeat', 
				esc_html__('Repeat Horizontally', 'dream-city') . '|repeat-x', 
				esc_html__('Repeat Vertically', 'dream-city') . '|repeat-y', 
				esc_html__('Repeat', 'dream-city') . '|repeat' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_bg_attachment', 
			'title' => esc_html__('Heading Background Attachment by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_heading_bg_attachment'], 
			'choices' => array( 
				esc_html__('Scroll', 'dream-city') . '|scroll', 
				esc_html__('Fixed', 'dream-city') . '|fixed' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_bg_size', 
			'title' => esc_html__('Heading Background Size by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_heading_bg_size'], 
			'choices' => array( 
				esc_html__('Auto', 'dream-city') . '|auto', 
				esc_html__('Cover', 'dream-city') . '|cover', 
				esc_html__('Contain', 'dream-city') . '|contain' 
			) 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_bg_color', 
			'title' => esc_html__('Heading Background Color Overlay by Default', 'dream-city'), 
			'desc' => '',  
			'type' => 'rgba', 
			'std' => $defaults[$tab]['dream-city' . '_heading_bg_color'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_heading_height', 
			'title' => esc_html__('Heading Height by Default', 'dream-city'), 
			'desc' => esc_html__('pixels', 'dream-city'), 
			'type' => 'number', 
			'std' => $defaults[$tab]['dream-city' . '_heading_height'], 
			'min' => '0' 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_breadcrumbs', 
			'title' => esc_html__('Breadcrumbs Visibility by Default', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_breadcrumbs'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_bottom_scheme', 
			'title' => esc_html__('Bottom Custom Color Scheme', 'dream-city'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['dream-city' . '_bottom_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_bottom_sidebar', 
			'title' => esc_html__('Bottom Sidebar Visibility by Default', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_bottom_sidebar'] 
		);
		
		$options[] = array( 
			'section' => 'content_section', 
			'id' => 'dream-city' . '_bottom_sidebar_layout', 
			'title' => esc_html__('Bottom Sidebar Layout by Default', 'dream-city'), 
			'desc' => '', 
			'type' => 'select', 
			'std' => $defaults[$tab]['dream-city' . '_bottom_sidebar_layout'], 
			'choices' => array( 
				'1/1|11', 
				'1/2 + 1/2|1212', 
				'1/3 + 2/3|1323', 
				'2/3 + 1/3|2313', 
				'1/4 + 3/4|1434', 
				'3/4 + 1/4|3414', 
				'1/3 + 1/3 + 1/3|131313', 
				'1/2 + 1/4 + 1/4|121414', 
				'1/4 + 1/2 + 1/4|141214', 
				'1/4 + 1/4 + 1/2|141412', 
				'1/4 + 1/4 + 1/4 + 1/4|14141414' 
			) 
		);
		
		break;
	case 'footer':
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_scheme', 
			'title' => esc_html__('Footer Custom Color Scheme', 'dream-city'), 
			'desc' => '', 
			'type' => 'select_scheme', 
			'std' => $defaults[$tab]['dream-city' . '_footer_scheme'], 
			'choices' => cmsmasters_color_schemes_list() 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_type', 
			'title' => esc_html__('Footer Type', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_footer_type'], 
			'choices' => array( 
				esc_html__('Default', 'dream-city') . '|default', 
				esc_html__('Small', 'dream-city') . '|small' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_additional_content', 
			'title' => esc_html__('Footer Additional Content', 'dream-city'), 
			'desc' => '', 
			'type' => 'radio', 
			'std' => $defaults[$tab]['dream-city' . '_footer_additional_content'], 
			'choices' => array( 
				esc_html__('None', 'dream-city') . '|none', 
				esc_html__('Footer Navigation', 'dream-city') . '|nav', 
				esc_html__('Social Icons', 'dream-city') . '|social', 
				esc_html__('Custom HTML', 'dream-city') . '|text' 
			) 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_logo', 
			'title' => esc_html__('Footer Logo', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_footer_logo'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_logo_url', 
			'title' => esc_html__('Footer Logo', 'dream-city'), 
			'desc' => esc_html__('Choose your website footer logo image.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_footer_logo_url'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_logo_url_retina', 
			'title' => esc_html__('Footer Logo for Retina', 'dream-city'), 
			'desc' => esc_html__('Choose your website footer logo image for retina.', 'dream-city'), 
			'type' => 'upload', 
			'std' => $defaults[$tab]['dream-city' . '_footer_logo_url_retina'], 
			'frame' => 'select', 
			'multiple' => false 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_nav', 
			'title' => esc_html__('Footer Navigation', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_footer_nav'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_social', 
			'title' => esc_html__('Footer Social Icons', 'dream-city'), 
			'desc' => esc_html__('show', 'dream-city'), 
			'type' => 'checkbox', 
			'std' => $defaults[$tab]['dream-city' . '_footer_social'] 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_html', 
			'title' => esc_html__('Footer Custom HTML', 'dream-city'), 
			'desc' => '<strong>' . esc_html__('HTML tags are allowed!', 'dream-city') . '</strong>', 
			'type' => 'textarea', 
			'std' => $defaults[$tab]['dream-city' . '_footer_html'], 
			'class' => '' 
		);
		
		$options[] = array( 
			'section' => 'footer_section', 
			'id' => 'dream-city' . '_footer_copyright', 
			'title' => esc_html__('Copyright Text', 'dream-city'), 
			'desc' => '', 
			'type' => 'text', 
			'std' => $defaults[$tab]['dream-city' . '_footer_copyright'], 
			'class' => '' 
		);
		
		break;
	}
	
	return apply_filters('cmsmasters_options_general_fields_filter', $options, $tab);	
}

