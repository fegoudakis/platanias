<?php 
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Admin Panel Theme Settings Import/Export
 * Created by CMSMasters
 * 
 */


function dream_city_options_demo_tabs() {
	$tabs = array();
	
	
	$tabs['import'] = esc_attr__('Import', 'dream-city');
	$tabs['export'] = esc_attr__('Export', 'dream-city');
	
	
	return $tabs;
}


function dream_city_options_demo_sections() {
	$tab = dream_city_get_the_tab();
	
	
	switch ($tab) {
	case 'import':
		$sections = array();
		
		$sections['import_section'] = esc_html__('Theme Settings Import', 'dream-city');
		
		
		break;
	case 'export':
		$sections = array();
		
		$sections['export_section'] = esc_html__('Theme Settings Export', 'dream-city');
		
		
		break;
	default:
		$sections = array();
		
		
		break;
	}
	
	
	return $sections;
} 


function dream_city_options_demo_fields($set_tab = false) {
	if ($set_tab) {
		$tab = $set_tab;
	} else {
		$tab = dream_city_get_the_tab();
	}
	
	
	$options = array();
	
	
	switch ($tab) {
	case 'import':
		$options[] = array( 
			'section' => 'import_section', 
			'id' => 'dream-city' . '_demo_import', 
			'title' => esc_html__('Theme Settings', 'dream-city'), 
			'desc' => esc_html__("Enter your theme settings data here and click 'Import' button", 'dream-city'), 
			'type' => 'textarea', 
			'std' => '', 
			'class' => '' 
		);
		
		
		break;
	case 'export':
		$options[] = array( 
			'section' => 'export_section', 
			'id' => 'dream-city' . '_demo_export', 
			'title' => esc_html__('Theme Settings', 'dream-city'), 
			'desc' => esc_html__("Click here to export your theme settings data to the file", 'dream-city'), 
			'type' => 'button', 
			'std' => esc_html__('Export Theme Settings', 'dream-city'), 
			'class' => 'cmsmasters-demo-export' 
		);
		
		
		break;
	}
	
	
	return $options;	
}

