<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 * 
 * Breadcrumbs Function
 * Created by CMSMasters
 * 
 */


function dream_city_breadcrumbs($sep_text = ' / ') {
	global $post;
	
	
	$homeLink = esc_url(home_url('/'));
	
	$homeText = esc_attr__('Αρχική', 'dream-city');
	
	$sep = "\n\t" . '<span class="breadcrumbs_sep">' . $sep_text . '</span>' . "\n\t";
	
	
	$year_format = get_the_time('Y');
	
	$month_format = get_the_time('F');
	
	$month_number_format = get_the_time('n');
	
	$day_format = get_the_time('d');
	
	$day_full_format = get_the_time('l');
	
	
	$url_year = get_year_link($year_format);
	
	$url_month = get_month_link($year_format, $month_number_format);
	
	
	echo '<a href="' . esc_url($homeLink) . '" class="cms_home">' . esc_html($homeText) . '</a>' . $sep;
	
	
	if (is_single()) {
		
		if (get_post_type() == 'project') {
			
			$category = get_the_category();
			
			$the_term = wp_get_post_terms( $post->ID, 'pj-categs' );
			
			//$num_cat = count($the_term);
			
			//print_r($the_term);
			
			echo get_category_parents($the_term[0], true, $sep) . ' <span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
				
			//echo 'AAaaAA';
			
		} else {
				
			$category = get_the_category();
			//echo '<pre>';
			//var_dump ($category);
			//echo '</pre>';
			$num_cat = count($category);
						
			
			if ($num_cat < 1) {
				$prof = get_the_ID();
				if ( $prof = 9427  || $prof = 72381){
					echo '<a href="'. get_site_url().'/diikisi">Διοίκηση</a>'. $sep;
					echo '<span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
				}else{
				echo '<span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
				}
			} else if ($num_cat >= 1) {
				echo get_category_parents($category[0], true, $sep) . ' <span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
				
			}
			
		}
		

	} elseif (is_category()) {
		global $cat;

		
		$multiple_cats = get_category_parents($cat, true, $sep);
		
		$multiple_cats_array = explode($sep, $multiple_cats);
		
		$multiple_cats_num = count($multiple_cats_array);
		
		
		$i = 2;
		
		
		foreach ($multiple_cats_array as $single_cat) {
			echo $single_cat;
			
			
			if ($i < $multiple_cats_num) {
				echo $sep;
			}
			
			
			$i++;
		}
	} elseif (is_tag()) {
		echo '<span>' . single_tag_title('', false) . '</span>';
	} elseif (is_day()) {
		echo '<a href="' . esc_url($url_year) . '">' . esc_html($year_format) . '</a>' . 
			$sep . 
			'<a href="' . esc_url($url_month) . '">' . esc_html($month_format) . '</a>' . 
			$sep . 
			'<span>' . esc_html($day_format) . ' (' . esc_html($day_full_format) . ')</span>';
	} elseif (is_month()) {
		echo '<a href="' . esc_url($url_year) . '">' . esc_html($year_format) . '</a>' . $sep . '<span>' . esc_html($month_format) . '</span>';
	} elseif (is_year()) {
		echo '<span>' . esc_html($year_format) . '</span>';
	} elseif (is_search()) {
		echo '<span>' . esc_html__('Search results for', 'dream-city') . ": '" . esc_html(get_search_query()) . "'</span>";
	} elseif (is_page() && !$post->post_parent) {
		echo '<span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
	} elseif (is_page() && $post->post_parent) {
		$post_array = get_post_ancestors($post);
		
		
		krsort($post_array);
		
		
		foreach ($post_array as $key => $postid) {
			$post_ids = get_post($postid);
			
			$title = $post_ids->post_title;
			
			
			echo '<a href="' . esc_url(get_permalink($post_ids)) . '">' . esc_html($title) . '</a>' . $sep;
		}
		
		
		echo '<span>' . cmsmasters_title(get_the_ID(), false) . '</span>';
	} elseif (is_author()) {
		echo '<span>' . esc_html(get_the_author()) . '</span>';
	} elseif (is_tax('post_format')) {
		if (is_tax('post_format', 'post-format-gallery')) {
			echo '<span>' . esc_html_x('Galleries', 'post format archive title', 'dream-city') . '</span>';
		} elseif (is_tax('post_format', 'post-format-image')) {
			echo '<span>' . esc_html_x('Images', 'post format archive title', 'dream-city') . '</span>';
		} elseif (is_tax('post_format', 'post-format-video')) {
			echo '<span>' . esc_html_x('Videos', 'post format archive title', 'dream-city') . '</span>';
		} elseif (is_tax('post_format', 'post-format-audio')) {
			echo '<span>' . esc_html_x('Audio', 'post format archive title', 'dream-city') . '</span>';
		}
	} elseif (is_post_type_archive()) {
		echo '<span>' . esc_html(post_type_archive_title('', false)) . '</span>';
	} elseif (is_tax()) {
		
		
		
		if (get_post_type() == 'project') {
			
			$the_term = get_term( get_queried_object()->term_id, 'pj-categs' );
			
			if ( $the_term->parent == 0 ) {
				
				echo '<span> ' . esc_html(single_term_title('', false)) . '</span>';
				
			} else {
				
				$parent = get_term_by('id', $the_term->parent, 'pj-categs');
				
				if ($parent->parent == 0) {
					
					echo '<span> <a style="font-weight:bold;" href="'.get_site_url().'/Ενότητα/'.$parent->slug.'">' . $parent->name .'</a> / '. esc_html(single_term_title('', false)) . '</span>';
					
				} else {
					//print_r($parent);
					//echo $parent->name.'<br>';
					$parent2 = get_term_by('id', $parent->parent, 'pj-categs');
					echo '<span><a style="font-weight:bold;" href="'.get_site_url().'/Ενότητα/'.$parent2->slug.'">' .$parent2->name .'</a> / <a style="font-weight:bold;" href="'.get_site_url().'/Ενότητα/'.$parent->slug.'">'. $parent->name .'</a> / '. esc_html(single_term_title('', false)) . '</span>';
					
				}
					
			}
			
			
			/*echo '<pre>';
				print_r($parent);
			echo '</pre>';*/
			
		} else {
			
			echo '<span>' . esc_html(single_term_title('', false)) . '</span>';
			
		}
		
		
		
		
	} else {
		echo '<span>' . esc_html__('No breadcrumbs', 'dream-city') . '</span>';
	}
}

