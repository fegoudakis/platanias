<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version		1.0.0
 *
 * Single Project Template
 * Created by CMSMasters
 *
 */


get_header();

?>


<div class="row">
	<div class="container">
		<div id="primary">


				<h2 class="page-header prof-title-design">Details</h2></li>
				<?php echo $listing_focus_1; ?>
				<?php
				$detailsFileds = array('Διάλεξε ένα από τα δύο' => 'named_unnamed_request',
                                       'Διάλεξε μία ή περισσότερες επιλογές' => 'choices',
                                       'ΟΝΟΜΑ' => 'choices',
                                       'ΕΠΩΝΥΜΟ' => 'lastname',
                                       'ΟΝΟΜΑ ΠΑΤΕΡΑ' => 'father_name',
                                       'ΟΝΟΜΑ ΜΗΤΕΡΑΣ' => 'mother_name',
                                       'ΕΤΟΣ ΓΕΝΝΗΣΗΣ' => 'year_of_birth',
                                       'ΑΦΜ' => 'afm',
                                       'ΤΟΠΟΣ ΚΑΤΟΙΚΙΑΣ' => 'area',
                                       'ΔΙΕΥΘΥΝΣΗ ΚΑΤΟΙΚΙΑΣ ' => 'address',
                                       'ΑΡΙΘΜΟΣ' => 'address_number',
                                       'Τ.Κ' => 'tk',
                                       'ΤΗΛΕΦΩΝΟ ΣΤΑΘΕΡΟ' => 'phone',
                                       'ΤΗΛΕΦΩΝΟ ΚΙΝΗΤΟ' => 'mobile_number',
                                       'EMAIL' => 'email',
                                       'Θέλω να λάβω ενημέρωση σε αυτό το email για την εξέλιξη του θέματός μας.' => 'mail_notification',
                                       'ΕΠΙΛΕΞΤΕ ΤΟ ΘΕΜΑ' => 'thematic_area',
                                       'ΠΕΡΙΓΡΑΨΤΕ ΤΟ ΘΕΜΑ ΣΑΣ' => 'description_thematic_area',
                                       //'Αν το αίτημα σας έχει γεωγραφικό προσδιορισμό, παρακαλούμε συμπληρώστε το πάνω στον χάρτη' => 'map_location'
                                    );

				foreach ($detailsFileds as $thekey => $nfields) {

					$checkit = get_field($nfields);
					if ($checkit) {
						$output = '<li class="'.$nfields.'">';
						$output .= '<strong class="key">'.$thekey.'</strong>';

						if (is_array($checkit)){

							$output .= '<span class="value">'.get_field($nfields).'</span>';
						}

						$output .= '</li>';
						echo $output;

					}

				}
?>


                <ul>
                    <li id="title_section5"><h2 style="margin-top: 30px;" class="page-header prof-title-design">Location</h2></li>

						<?php

						$location = get_field('map_location');

						if( !empty($location) ):
							?>
                            <div class="acf-map">
                                <div class="marker" data-lat="<?php echo $location['lat']; ?>" data-lng="<?php echo $location['lng']; ?>"></div>
                            </div>
						<?php endif; ?>
                    
                </ul>


			</div>
		</div>
	</div>
</div>


<?php

get_footer();

?>
