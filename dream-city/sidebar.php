<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Sidebar Template
 * Created by CMSMasters
 * 
 */


if (is_singular()) {
	$sidebar_id = get_post_meta(get_the_ID(), 'cmsmasters_sidebar_id', true);
} elseif (CMSMASTERS_WOOCOMMERCE && is_shop()) {
	$sidebar_id = get_post_meta(wc_get_page_id('shop'), 'cmsmasters_sidebar_id', true);
}


if (isset($sidebar_id) && is_dynamic_sidebar($sidebar_id) && is_active_sidebar($sidebar_id)) {
	dynamic_sidebar($sidebar_id);
} else if ((is_home() || is_archive()) && is_active_sidebar('sidebar_archive')) {
	dynamic_sidebar('sidebar_archive');
} else if (is_search() && is_active_sidebar('sidebar_search')) {
	dynamic_sidebar('sidebar_search');
} else if(is_active_sidebar('sidebar_default') && is_single()){
	echo '<p>Κοινοποίηση</p><hr style="margin-top:0px; margin-bottom:5px;">';
	echo '<div style="padding-top:20px;">';
	$linkPost = get_permalink();
	echo '<a href="https://www.facebook.com/sharer/sharer.php?display=popup&u='.$linkPost.'" target="_blank" <i class="fa fa-facebook-official" style="font-size: 24px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i></a>';
	echo '<a href="https://plus.google.com/share?url='.$linkPost.'" target="_blank" <i class="fa fa-google-plus-square" style="font-size: 24px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i></a>';
	echo '<a href="https://plus.google.com/share?url='.$linkPost.'" target="_blank" <i class="fa fa-twitter" style="font-size: 24px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i></a>';
	echo '<a href="https://pinterest.com/pin/create/button/?url='.$linkPost.'" target="_blank" <i class="fa fa-pinterest" style="font-size: 24px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i></a>';
	echo '</div>';
	
	
} else if (is_active_sidebar('sidebar_default')) {
	if (is_page(15173)){
		echo 'Τα τοπικά συμβούλια του Δήμου Πλατανιά λειτουργούν με βάση την <a target="_blank" href="http://beta.platanias.gr/wp-content/uploads/2017/09/egiklios_49_10_org_leit.pdf">Εγκύκλιο Λειτουργίας Λειτουργία Δημοτικών και Τοπικών Κοινοτήτων</a> ';
	}
	//dynamic_sidebar('sidebar_default');
	/*echo '<p>Κοινοποίηση</p><hr style="margin-top:0px; margin-bottom:5px;">';
	
	echo '<i class="fa fa-facebook-official" style="font-size: 40px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i>';
	echo '<i class="fa fa-google-plus-square" style="font-size: 40px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i>';
	echo '<i class="fa fa-twitter-square" style="font-size: 40px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i>';
	echo '<i class="fa fa-pinterest-square" style="font-size: 40px; margin-right: 10px; color: #044a85;" aria-hidden="true"></i>';
	*/

}else {
	dream_city_sidebarDefaultText();
}

if(is_page(15145)){ ?>
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
		<ul>
			<a href="http://beta.platanias.gr/profile/gavriil-kouris/" ><li>Γενικός Γραμματέας</li></a>
			<a href="http://beta.platanias.gr/profile/giannhsmalandrakhs2/" ><li>Δήμαρχος Δήμου Πλατανιά</li></a>
			<a href="http://beta.platanias.gr/diikisi/entetalmeni-dimotiki-symvouli/"><li>Εντεταλμένοι Δημοτικοί Σύμβουλοι</li></a>
			<a href="http://beta.platanias.gr/diikisi/symvoulia-dimotikon-topikon-kinotiton/"><li>Συμβούλια Δημοτικών / Τοπικών Κοινοτήτων</li></a>	
		</ul>	
	</div>
<?php }elseif(is_page(71752)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/profile/gavriil-kouris/" ><li>Γενικός Γραμματέας</li></a>
				<a href="http://beta.platanias.gr/profile/giannhsmalandrakhs2/" ><li>Δήμαρχος Δήμου Πλατανιά</li></a>
				<a href="http://beta.platanias.gr/diikisi/antidimarchi-dimou-platania/"><li>Αντιδήμαρχοι</li></a>
				<a href="http://beta.platanias.gr/diikisi/symvoulia-dimotikon-topikon-kinotiton/"><li>Συμβούλια Δημοτικών / Τοπικών Κοινοτήτων</li></a>	
			</ul>	
	</div>
<?php }elseif(is_page(71756)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/profile/gavriil-kouris/" ><li>Γενικός Γραμματέας</li></a>
				<a href="http://beta.platanias.gr/profile/giannhsmalandrakhs2/" ><li>Δήμαρχος Δήμου Πλατανιά</li></a>
				<a href="http://beta.platanias.gr/diikisi/antidimarchi-dimou-platania/"><li>Αντιδήμαρχοι</li></a>
				<a href="http://beta.platanias.gr/diikisi/entetalmeni-dimotiki-symvouli/"><li>Εντεταλμένοι Δημοτικοί Σύμβουλοι</li></a>
					
			</ul>		
	</div>
	
	
<?php }

if(is_page(71760)){ ?>
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
			<ul>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ektelestiki-epitropi/" ><li>Εκτελεστική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ikonomiki-epitropi/" ><li>Οικονομική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/epitropi-piotitas-zois/"><li>Επιτροπή Ποιότητας Ζωής</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiki-epitropi-diavoulefsis/"><li>Δημοτική Επιτροπή Διαβούλευσης</li></a>	
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/lipes-epitropes-d-s/"><li>Λοιπές Επιτροπές Δ.Σ.</li></a>	
			</ul>	
	</div>
<?php }elseif(is_page(15153)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiko-symvoulio/" ><li>Δημοτικό Συμβούλιο</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ikonomiki-epitropi/" ><li>Οικονομική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/epitropi-piotitas-zois/"><li>Επιτροπή Ποιότητας Ζωής</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiki-epitropi-diavoulefsis/"><li>Δημοτική Επιτροπή Διαβούλευσης</li></a>	
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/lipes-epitropes-d-s/"><li>Λοιπές Επιτροπές Δ.Σ.</li></a>	
			</ul>		
	</div>
<?php }elseif(is_page(15155)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiko-symvoulio/" ><li>Δημοτικό Συμβούλιο</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ektelestiki-epitropi/" ><li>Εκτελεστική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/epitropi-piotitas-zois/"><li>Επιτροπή Ποιότητας Ζωής</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiki-epitropi-diavoulefsis/"><li>Δημοτική Επιτροπή Διαβούλευσης</li></a>	
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/lipes-epitropes-d-s/"><li>Λοιπές Επιτροπές Δ.Σ.</li></a>	
			</ul>	
	</div>
	<?php }elseif(is_page(15160)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiko-symvoulio/" ><li>Δημοτικό Συμβούλιο</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ektelestiki-epitropi/" ><li>Εκτελεστική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ikonomiki-epitropi/" ><li>Οικονομική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiki-epitropi-diavoulefsis/"><li>Δημοτική Επιτροπή Διαβούλευσης</li></a>	
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/lipes-epitropes-d-s/"><li>Λοιπές Επιτροπές Δ.Σ.</li></a>	
			</ul>			
	</div>
<?php }elseif(is_page(71763)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiko-symvoulio/" ><li>Δημοτικό Συμβούλιο</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ektelestiki-epitropi/" ><li>Εκτελεστική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ikonomiki-epitropi/" ><li>Οικονομική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/epitropi-piotitas-zois/"><li>Επιτροπή Ποιότητας Ζωής</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/lipes-epitropes-d-s/"><li>Λοιπές Επιτροπές Δ.Σ.</li></a>	
			</ul>		
	</div>
	<?php }elseif(is_page(15164)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiko-symvoulio/" ><li>Δημοτικό Συμβούλιο</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ektelestiki-epitropi/" ><li>Εκτελεστική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/ikonomiki-epitropi/" ><li>Οικονομική Επιτροπή</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/epitropi-piotitas-zois/"><li>Επιτροπή Ποιότητας Ζωής</li></a>
				<a href="http://beta.platanias.gr/dimotiko-symvoulio-epitropes-dimou/dimotiki-epitropi-diavoulefsis/"><li>Δημοτική Επιτροπή Διαβούλευσης</li></a>		
			</ul>		
	</div>
	
	
<?php }

if(is_page(71797)){ ?>
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
		<ul>
			<a href="http://beta.platanias.gr/dimotikes-ypiresies/diarthrosi-ypiresion-dimou-oef/" ><li>Διάρθρωση Υπηρεσίων Δήμου (ΟΕΥ)</li></a>
			<a href="http://beta.platanias.gr/dimotikes-ypiresies/epikinonia-me-to-dimo/"><li>Επικοινωνία με το Δήμο</li></a>
			<a href="http://beta.platanias.gr/dimotikes-ypiresies/kep/"><li>ΚΕΠ</li></a>
			
		</ul>	
	</div>
<?php }elseif(is_page(15183)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/organogramma/" ><li>Οργανόγραμμα</li></a>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/epikinonia-me-to-dimo/"><li>Επικοινωνία με το Δήμο</li></a>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/kep/"><li>ΚΕΠ</li></a>
				
			</ul>	
	</div>
<?php }elseif(is_page(15209)){ ?>
<div>
	<?php 
		$id = get_the_ID();
		$location = get_field('location', $id); 
		$phone = get_field('phone', $id); 
		$fax_area = get_field('fax_area', $id); 
		$mail_area = get_field('mail_area', $id); 
		?>
	<?php if ($location) {?>
		<div style="display:table;     padding: 5px;">			
				<i style="display:table-cell; padding-right: 15px; color:#3183ee;" class="fa fa-map-marker" aria-hidden="true"></i>
				<p style="padding:0px;"><?php echo $location; ?></p>
		</div>
	<?php } ?>
	<?php if ($phone) {?>
		<hr style="margin:0px;">
		<div style="display:table; padding: 5px;">
				<i style="display:table-cell;    font-size: 22px; padding-right: 15px; color:#3183ee;" class="fa fa-phone" aria-hidden="true"></i>
				<p style="padding:0px;"><?php echo $phone; ?></p>			
		</div>
	<?php } ?>
	<?php if ($fax_area) {?>
		<hr style="margin:0px;">
		<div style="display:table; padding: 5px;">
			<i style="display:table-cell;     font-size: 18px; padding-right: 15px; color:#3183ee;" class="fa fa-fax aria-hidden="true"></i>
			<p style="padding:0px;"><?php echo $fax_area; ?></p>				
		</div>
	<?php } ?>
	<?php if ($mail_area) {?>
		<hr style="margin:0px;">
		<div style="display:table; padding: 5px;">
			<i style="display:table-cell;     font-size: 16px; padding-right: 15px; color:#3183ee;" class="fa fa-envelope" aria-hidden="true"></i>
			<p style="padding:0px;"><?php echo $mail_area; ?></p>			
		</div>
	<?php } ?>
<hr>
	
</div>


		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/organogramma/" ><li>Οργανόγραμμα</li></a>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/diarthrosi-ypiresion-dimou-oef/" ><li>Διάρθρωση Υπηρεσίων Δήμου (ΟΕΥ)</li></a>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/kep/"><li>ΚΕΠ</li></a>
			</ul>		
	</div>
<?php }elseif(is_page(72726)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/organogramma/" ><li>Οργανόγραμμα</li></a>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/diarthrosi-ypiresion-dimou-oef/" ><li>Διάρθρωση Υπηρεσίων Δήμου (ΟΕΥ)</li></a>
				<a href="http://beta.platanias.gr/dimotikes-ypiresies/epikinonia-me-to-dimo/"><li>Επικοινωνία με το Δήμο</li></a>				
			</ul>		
	</div>
	
	
<?php } ?>
<div style="margin-bottom:30px;">
	<?php 
		$id = get_the_ID();
		$dioikisi = get_field('dioikisi', $id); 
		$address = get_field('address', $id); 
		$phone = get_field('phone_number', $id); 
		$fax = get_field('fax_number', $id); 
		?>
	<?php if ($dioikisi) {?>
		<div style="display:table;     padding: 5px;">			
				<i style="display:table-cell; padding-right: 15px; color:#3183ee;" class="fa fa-users" aria-hidden="true"></i>
				<p style="padding:0px;"><?php echo $dioikisi; ?></p>
		</div>
	<?php } ?>
	<?php if ($address) {?>
		<hr style="margin:0px;">
		<div style="display:table; padding: 5px;">
				<i style="display:table-cell;    font-size: 22px; padding-right: 15px; color:#3183ee;" class="fa fa-map-marker" aria-hidden="true"></i>
				<p style="padding:0px;"><?php echo $address; ?></p>			
		</div>
	<?php } ?>
	<?php if ($phone) {?>
		<hr style="margin:0px;">
		<div style="display:table; padding: 5px;">
			<i style="display:table-cell;     font-size: 18px; padding-right: 15px; color:#3183ee;" class="fa fa-phone aria-hidden="true"></i>
			<p style="padding:0px;"><?php echo $phone; ?></p>				
		</div>
	<?php } ?>
	<?php if ($fax) {?>
		<hr style="margin:0px;">
		<div style="display:table; padding: 5px;">
			<i style="display:table-cell;     font-size: 16px; padding-right: 15px; color:#3183ee;" class="fa fa-fax" aria-hidden="true"></i>
			<p style="padding:0px;"><?php echo $fax; ?></p>			
		</div>
	<?php } ?>
<hr>
	
	</div>
<?php
if(is_page(71801)){ ?>
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
		<ul>
			
			<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/k-e-d-p-kinofelis-epichirisi-dimou-platania/"><li>Κ.Ε.Δ.Η.Π. (Κοινωφελής Επιχείρηση Δήμου Πλατανιά)</li></a>
			<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/nomiko-prosopo-dimosiou-dikeou-dimou-platania/"><li>Νομικό Πρόσωπο Δημοσίου Δικαίου Δήμου Πλατανιά</li></a>
			<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-protovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Πρωτοβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
			<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-defterovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Δευτεροβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
			
		</ul>	
	</div>
	
	
	
	
<?php }elseif(is_page(71803)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
						
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/devava-dimotiki-epichirisi-ydrefsis-apochetefsis-voriou-axona/" ><li>ΔΕΥΑΒΑ (Δημοτική Επιχείρηση Ύδρευσης Αποχέτευσης Βορείου Άξονα)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/nomiko-prosopo-dimosiou-dikeou-dimou-platania/"><li>Νομικό Πρόσωπο Δημοσίου Δικαίου Δήμου Πλατανιά</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-protovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Πρωτοβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-defterovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Δευτεροβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
				
			</ul>	
	</div>
<?php }elseif(is_page(71805)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/devava-dimotiki-epichirisi-ydrefsis-apochetefsis-voriou-axona/" ><li>ΔΕΥΑΒΑ (Δημοτική Επιχείρηση Ύδρευσης Αποχέτευσης Βορείου Άξονα)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/k-e-d-p-kinofelis-epichirisi-dimou-platania/"><li>Κ.Ε.Δ.Η.Π. (Κοινωφελής Επιχείρηση Δήμου Πλατανιά)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-protovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Πρωτοβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-defterovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Δευτεροβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
			</ul>		
	</div>
	<?php }elseif(is_page(71807)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/devava-dimotiki-epichirisi-ydrefsis-apochetefsis-voriou-axona/" ><li>ΔΕΥΑΒΑ (Δημοτική Επιχείρηση Ύδρευσης Αποχέτευσης Βορείου Άξονα)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/k-e-d-p-kinofelis-epichirisi-dimou-platania/"><li>Κ.Ε.Δ.Η.Π. (Κοινωφελής Επιχείρηση Δήμου Πλατανιά)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/nomiko-prosopo-dimosiou-dikeou-dimou-platania/"><li>Νομικό Πρόσωπο Δημοσίου Δικαίου Δήμου Πλατανιά</li></a>			
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-defterovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Δευτεροβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
				
			</ul>	
	</div>
<?php }elseif(is_page(71809)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/devava-dimotiki-epichirisi-ydrefsis-apochetefsis-voriou-axona/" ><li>ΔΕΥΑΒΑ (Δημοτική Επιχείρηση Ύδρευσης Αποχέτευσης Βορείου Άξονα)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/k-e-d-p-kinofelis-epichirisi-dimou-platania/"><li>Κ.Ε.Δ.Η.Π. (Κοινωφελής Επιχείρηση Δήμου Πλατανιά)</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/nomiko-prosopo-dimosiou-dikeou-dimou-platania/"><li>Νομικό Πρόσωπο Δημοσίου Δικαίου Δήμου Πλατανιά</li></a>
				<a href="http://beta.platanias.gr/nomika-prosopa-epichirisis/scholiki-epitropi-protovathmias-ekpedefsis-dimou-platania/"><li>Σχολική Επιτροπή Πρωτοβάθμιας Εκπαίδευσης Δήμου Πλατανιά</li></a>
				
			</ul>		
	</div>
	
	
<?php }

if(is_page(71813)){ ?>
	<div class="more-side">				
		<h3 class="project_details_title">Δείτε επίσης</h3>
		
		<ul>
			<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/techniko-programma-ergon/"><li>Τεχνικό Πρόγραμμα Έργων</li></a>
			<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/ikonomika-stichia-opd/"><li>Οικονομικά Στοιχεία-ΟΠΔ</li></a>
			<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/apologismos-pepragmenon/"><li>Απολογισμός Πεπραγμένων</li></a>
			
		</ul>	
	</div>
<?php }elseif(is_page(71815)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/stratigika-epichirisiaka-programmata/" ><li>Στρατηγικά-Επιχειρησιακά Προγράμματα</li></a>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/ikonomika-stichia-opd/"><li>Οικονομικά Στοιχεία-ΟΠΔ</li></a>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/apologismos-pepragmenon/"><li>Απολογισμός Πεπραγμένων</li></a>
			</ul>	
	</div>
<?php }elseif(is_page(71817)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/stratigika-epichirisiaka-programmata/" ><li>Στρατηγικά-Επιχειρησιακά Προγράμματα</li></a>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/techniko-programma-ergon/"><li>Τεχνικό Πρόγραμμα Έργων</li></a>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/apologismos-pepragmenon/"><li>Απολογισμός Πεπραγμένων</li></a>
			</ul>		
	</div>
<?php }elseif(is_page(72590)){ ?>
		<div class="more-side">				
			<h3 class="project_details_title">Δείτε επίσης</h3>
			
			<ul>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/stratigika-epichirisiaka-programmata/" ><li>Στρατηγικά-Επιχειρησιακά Προγράμματα</li></a>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/techniko-programma-ergon/"><li>Τεχνικό Πρόγραμμα Έργων</li></a>
				<a href="http://beta.platanias.gr/ikonomiki-epichirisiaki-litourgia/ikonomika-stichia-opd/"><li>Οικονομικά Στοιχεία-ΟΠΔ</li></a>
			</ul>		
	</div>
	
	
<?php }







/*
$parent_id = 72383;
$id = get_the_ID();
//echo $id;	

 //$child = get_page_children($parent_id);
 
 
 $args = array(
	'post_parent' => $parent_id,
	'post_type'   => 'any', 
	'numberposts' => -1,
	'post_status' => 'pub;ish' 
);
$children = get_children( $args );

echo '<pre>';
var_dump($children);
echo '</pre>';
*/


/* Sidebar deksia me selides titlo kai emfanisi twn childs
if( is_page()) {
	$id = get_the_ID();
    $children = wp_list_pages( array(
        'depth'        => 1,     
        'child_of'     => $id,
        'sort_column' => 'menu_order',
        'echo'         => 0,
    ));
    
     echo $children;
	 
	 $post= get_post_type();
	 //echo $post;

}*/
