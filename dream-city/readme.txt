/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.1
 * 
 * Theme Information
 * Created by CMSMasters
 * 
 */

To update your theme please use the files list from the FILE LOGS below and substitute/add the listed files on your server with the same files in the Updates folder.

Important: after you have updated the theme, in your admin panel please proceed to
Theme Settings - Fonts and click "Save" in any tab,
then proceed to 
Theme Settings - Colors and click "Save" in any tab here.


Version 1.0.1: files operations:


	Theme Files edited:
	
		dream-city\functions.php
		dream-city\js\jquery.script.js
		dream-city\readme.txt
		dream-city\style.css
		dream-city\theme-framework\admin\plugin-activator.php
		dream-city\theme-framework\admin\plugins\cmsmasters-content-composer.zip
		dream-city\theme-framework\admin\plugins\LayerSlider.zip
		dream-city\theme-framework\css\less\style.less
		dream-city\theme-framework\function\template-functions-profile.php
		dream-city\theme-framework\js\jquery.isotope.mode.js
		dream-city\theme-framework\postType\profile\profile-horizontal.php
		dream-city\theme-framework\postType\profile\profile-single.php
		dream-city\theme-framework\postType\profile\profile-vertical.php
		dream-city\theme-framework\theme-functions.php
		dream-city\tribe-events\cmsmasters-framework\function\plugin-fonts.php
		dream-city\tribe-events\widgets\list-widget.php
		
		
	Theme Files removed:
	
		dream-city\framework\class\widgets.php
		dream-city\single-event-schedule.php
		
		

	Proceed to wp-content\plugins\cmsmasters-content-composer
	and update all files in this folder to version 2.0.2
	
	Proceed to wp-content\plugins\LayerSlider
	and update all files in this folder to version 6.1.6
	
	
	
--------------------------------------
Version 1.0: Release!

