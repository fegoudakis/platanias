<?php
/**
 * @package 	WordPress
 * @subpackage 	Dream City
 * @version 	1.0.0
 * 
 * Website Header Template
 * Created by CMSMasters
 * 
 */

acf_form_head();
$cmsmasters_option = dream_city_get_global_options();


?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="cmsmasters_html">
<head>
<meta charset="<?php bloginfo('charset'); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
<meta name="format-detection" content="telephone=no" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php esc_url(bloginfo('pingback_url')); ?>" />



<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!--<script type="text/javascript" src="https://crowdpolicy.atlassian.net/s/d41d8cd98f00b204e9800998ecf8427e-T/-45hbff/b/c/7ebd7d8b8f8cafb14c7b0966803e5701/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=c30b348d"></script>-->

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.0/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>



<script>

$(document).ready(function(){
	$( ".cmsmasters_prev_post" ).find( "a" ).text( "");
	$( ".cmsmasters_next_post" ).find( "a" ).text( "");
	$( ".cmsmasters_next_post" ).find( "a" ).text( "");

	/*(hide) Είδος Εγγράφου αλλαγή σε Ενημέρωση-Επίκαιρα*/
	var x = $(".cmsmasters_breadcrumbs_inner a:nth-child(3)" ).text();
	if (x == 'Είδος Εγγράφου'){
		$(".cmsmasters_breadcrumbs_inner a:nth-child(3)" ).text('Ενημέρωση-Επίκαιρα');
		//$(".cmsmasters_breadcrumbs_inner a:nth-child(3)" ).hide();
		//$(".cmsmasters_breadcrumbs_inner span:nth-child(4)" ).hide();
	}	

});

</script>	
		
<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php do_action('cmsmasters_before_body', $cmsmasters_option); ?>

<?php dream_city_get_header_search_form($cmsmasters_option); ?>

<!-- _________________________ Start Page _________________________ -->
<div id="page" class="<?php dream_city_get_page_classes($cmsmasters_option); ?>hfeed site">
<?php do_action('cmsmasters_before_page', $cmsmasters_option); ?>

<!-- _________________________ Start Main _________________________ -->
<div id="main">
	
<!-- _________________________ Start Header _________________________ -->
<header id="header">
	
	<?php 
	get_template_part('theme-framework/template/header-top');
	
	get_template_part('theme-framework/template/header-mid');
	
	get_template_part('theme-framework/template/header-bot');
	?>
</header>
<!-- _________________________ Finish Header _________________________ -->

	
<!-- _________________________ Start Middle _________________________ -->
<div id="middle"<?php echo (is_404()) ? ' class="error_page"' : ''; ?>>
<?php 
if (!is_404()) {
	dream_city_page_heading();
}


list($cmsmasters_layout, $cmsmasters_page_scheme) = dream_city_theme_page_layout_scheme();


echo '<div class="middle_inner' . (($cmsmasters_page_scheme != 'default') ? ' cmsmasters_color_scheme_' . $cmsmasters_page_scheme : '') . '">' . "\n" . 
	'<div class="content_wrap ' . $cmsmasters_layout . 
	((is_singular('project')) ? ' project_page' : '') . ((is_page(71075)) ? ' map_style' : '').
	((is_singular('profile')) ? ' profile_page' : '') . 
	((CMSMASTERS_WOOCOMMERCE && (
		is_woocommerce() || 
		is_cart() || 
		is_checkout() || 
		is_checkout_pay_page() || 
		is_account_page() || 
		is_order_received_page() || 
		is_add_payment_method_page()
	)) ? ' cmsmasters_woo' : '') . 
	'">' . "\n\n";

